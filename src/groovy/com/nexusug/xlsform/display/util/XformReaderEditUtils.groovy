package com.nexusug.xlsform.display.util

import com.nexusug.xlsform.XformQuestion
import com.nexusug.xlsform.XformSubmission
import com.nexusug.xlsform.display.model.XformMultiDisplayableType
import com.nexusug.xlsform.display.model.XformDisplayable
import com.nexusug.xlsform.display.model.XformMultiDisplayable
import com.nexusug.xlsform.exception.XformBindingNotFoundException
import com.nexusug.xlsform.exception.XformElementPropertiesNotFoundException
import org.springframework.beans.BeanUtils

/**
 * Contains methods for processing submissions for editing. This processing is limited to creating {@link XformDisplayable}
 * elements for the submissions.
 *
 * Created by ivan on 16/04/2016.
 */
class XformReaderEditUtils {

	/**
	 * returns formElements for a given <tt>xform</tt> populated with corresponding values from the provided
	 * <tt>submission</tt>. Note that this method will create {@link XformDisplayable} for each matching
	 * <tt>repeatSubmission</tt> group found in the <tt>submission</tt>.
	 * @param xform the xml content of the {@link com.nexusug.xlsform.Xform} to be processed.
	 * @param submission the submission that is being prepared for editing.
	 * @return a list of <tt>XformDisplayable</tt>s contained in the provided form in the order at which they were added to the form.
	 * <br />Could be empty but not null
	 * @throws XformBindingNotFoundException
	 * @throws XformElementPropertiesNotFoundException
	 */
	static List<XformDisplayable> getFormElements(String xform, XformSubmission submission)
			throws XformBindingNotFoundException, XformElementPropertiesNotFoundException {

		List<XformDisplayable> formElements = XformReader.getFormElements(xform)
		List<XformDisplayable> processedElements = new ArrayList<>()

		formElements.each { XformDisplayable displayable ->
			if (displayable.type == XformMultiDisplayableType.group) {
				((XformMultiDisplayable) displayable).displayables.each { groupDisplayable ->
					XformQuestion question = submission.questions.find { XformQuestion formQuestion ->
						formQuestion.question == groupDisplayable.name && !formQuestion.repeatQuestion
					}

					if (question && question.answers) {
						if (question.answers) {
							groupDisplayable.value = (question.answers.collect { it.value } - null)?.join(',')
						}
						submission.questions.remove(question)
					}
				}
				processedElements.add(displayable)
			} else if (displayable.type == XformMultiDisplayableType.repeat) {
				//when repeats become optional, this need to be looked into
				Map<Integer, List<XformQuestion>> indexQuestionsMap = submission.questions.findAll { XformQuestion formQuestion ->
					formQuestion.repeatQuestion && formQuestion.repeatName == displayable.name
				}.groupBy { it.inRepeatIndex }

				indexQuestionsMap.each { int index, List<XformQuestion> questions ->
					processedElements.addAll(processXformMultiDisplayable((XformMultiDisplayable) displayable, questions))
				}
			} else {
				XformQuestion question = submission.questions.find { XformQuestion formQuestion ->
					formQuestion.question == displayable.name && !formQuestion.repeatQuestion
				}

				if (question) {
					displayable.value = (question.answers.collect { it.value } - null)?.join(',')
					submission.questions.remove(question)
				}
				processedElements.add(displayable)
			}
		}

		return processedElements
	}

	/**
	 * Creates a list of {@link XformMultiDisplayable} instances from the supplied <tt>questions</tt>.
	 * Each of the {@link XformMultiDisplayable} <tt>displayables</tt> will contain values for the corresponding
	 * matching questions.
	 * Determining of which questions belong to the same displayable is done by checking the <tt>inRepeat</tt>
	 * index of the questions. If no questions had their <tt>repeatName</tt> matching the name of
	 * the <tt>defaultMultiDisplayable</tt> then, the <tt>defaultMultiDisplayable is returned.
	 * @param defaultMultiDisplayable the {@link XformMultiDisplayable} to use for repopulating formElements.
	 * @param questions the questions to use for determining the values of the various formElements in the
	 * <tt>defaultMultiDisplayable</tt>
	 * @return non empty list of {@link XformMultiDisplayable} instances
	 */
	static List<XformMultiDisplayable> processXformMultiDisplayable(XformMultiDisplayable defaultMultiDisplayable,
																		List<XformQuestion> questions) {
		List<XformMultiDisplayable> multiDisplayables = new ArrayList<>()

		while (questions.size() > 0) {
			List<String> addedElementNames = []
			XformMultiDisplayable multiDisplayable = new XformMultiDisplayable()
			BeanUtils.copyProperties(defaultMultiDisplayable, multiDisplayable, 'displayables')
			multiDisplayable.displayables = new ArrayList<>()

			XformQuestion seekQuestion = questions.get(0)
			List<XformQuestion> questionGroup = questions.findAll {
				it.inRepeatIndex == seekQuestion.inRepeatIndex
			}
			questionGroup?.each { XformQuestion formQuestion ->
				XformDisplayable formElement = defaultMultiDisplayable.displayables.find {
					it.name == formQuestion.question
				}

				if (formElement) {
					//do not add elements that could probably no longer exist in the multi displayable.
					XformDisplayable displayable = formElement.class.newInstance()
					BeanUtils.copyProperties(formElement, displayable)

					displayable.value = (formQuestion.answers?.collect { it.value } - null)?.join(',')

					multiDisplayable.displayables.add(displayable)
					addedElementNames << displayable.name
				}
			}
			questions.removeAll(questionGroup)

			defaultMultiDisplayable.displayables.findAll { !(it.name in addedElementNames) }.each {
				// ensures that displayables that did not exist in the past for the multi displayable are added.
				XformDisplayable displayable = it.class.newInstance()
				BeanUtils.copyProperties(it, displayable)
				multiDisplayable.displayables.add(displayable)
			}
			multiDisplayables.add(multiDisplayable)
		}

		if (!multiDisplayables) {
			//if no submission was ever provided for this multi displayable then at least return the multi displayable as is
			multiDisplayables.add(defaultMultiDisplayable)
		}
		return multiDisplayables
	}
}
