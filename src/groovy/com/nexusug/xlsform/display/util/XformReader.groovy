package com.nexusug.xlsform.display.util

import com.nexusug.xlsform.display.model.*
import com.nexusug.xlsform.exception.XformBindingNotFoundException
import com.nexusug.xlsform.exception.XformElementPropertiesNotFoundException
import groovy.xml.XmlUtil
import org.json.XML

/**
 * Utility containing methods for getting various details about a form.
 *
 * Created by ivan on 30/11/2015.
 */
class XformReader {

	static final String JAVAROSA_NAMESPACE_URI = 'http://openrosa.org/javarosa'
	/**
	 * Returns a map of all the meta data about a form. The key is the metadata names and the values are the values
	 * of the metadata entries.
	 *
	 * @param xform the xform whose meta data is to be got.
	 * @param formName the name of the form
	 * @return non nullable map.
	 */
	static Map<String, String> getMetaData(String xform) {
		def form = new XmlParser().parseText(xform)
		Map<String, String> metaData = new HashMap<>()
		form.'*'.model.instance.getAt(0).'*'.getAt(0).meta.'*'.each { Node node ->
			String name = com.nexusug.xlsform.util.XmlUtil.getNodeName(node)
			metaData.put(name, node.value()[0])
		}
		return metaData
	}

	/**
	 * Gets the name of the form. The name of the form is the tag that contains the id of the xform.
	 *
	 * @param xform
	 * @return
	 */
	static String getFormName(String xform) {
		def form = new XmlParser().parseText(xform)
		Node formNode = form.'*'.model.instance.getAt(0).'*'.getAt(0)
		return com.nexusug.xlsform.util.XmlUtil.getNodeName(formNode)
	}

	/**
	 * Get the title of the form.
	 * @param xform
	 * @return
	 */
	static String getFormTitle(String xform) {
		def form = new XmlParser().parseText(xform)
		Node formNode = form.'*'.'h:title'.getAt(0)
		return formNode.value()[0]
	}

	/**
	 * Get the id of the form.
	 * @param xform
	 * @return
	 */
	static String getFormId(String xform) {
		def form = new XmlParser().parseText(xform)
		Node formNode = form.'*'.model.instance.getAt(0).'*'.getAt(0)
		return formNode.@id
	}

	/**
	 * Serializes the model element of the <tt>xform</tt> into equivalent json.
	 *
	 * @param xform the <tt>xform</tt> whose model is to be serialized.
	 * @return the serialized model of the <tt>xform</tt> as <tt>JSON</tt>, <br />
	 * 		could return null if no <tt>model</tt> was found in the <tt>xform</tt>
	 */
	static String serializeFormModelToJSON(String xform, int textIndentation = 2) {
		def form = new XmlParser().parseText(xform)
		String jsonModel

		def model = form.'*'.model?.getAt(0)
		if (model) {
			String xmlModel = XmlUtil.serialize(model)
			jsonModel = XML.toJSONObject(xmlModel).toString(textIndentation)
		}
		return jsonModel
	}

	/**
	 * //TODO does not support constraints and relevant specifications.
	 * Extracts all the <tt>Xform</tt> elements contained within a given xform.
	 *
	 * @param xform the xml content of the <tt>Xform</tt> to be processed.
	 * @return a list of <tt>XformDisplayable</tt>s contained in the provided form in the order at which they were added to the form.
	 * 		  <br />Could be empty but not null
	 */
	static List<XformDisplayable> getFormElements(String xform) {
		String formName = getFormName(xform)
		def form = new XmlParser().parseText(xform)
		List<XformDisplayable> formElements = new ArrayList<>()

		form.'*'.model.instance.getAt(0).'*'.'*'.each { Node node ->
			String name = com.nexusug.xlsform.util.XmlUtil.getNodeName(node)
			String value = node.value()?.getAt(0)
			if (name == 'meta') {
				return
			}
			String nodeset = "/${formName}/${name}"
			try {
				if (!isXformGroupElement(xform, nodeset)) {
					XformDisplayable displayable = createDisplayable(name, value, xform, nodeset)
					if (displayable) {
						formElements.add(displayable)
					}
				} else {
					formElements.add(createXformMultiDisplayable(xform, nodeset))
				}
			} catch (XformBindingNotFoundException | XformElementPropertiesNotFoundException ex) {
				ex.printStackTrace()
			}
		}
		return formElements
	}

	/**
	 * Helper method for creating a {@link XformMultiDisplayable} representation for an xls form group or xls form group.
	 *
	 * @param xform the form containing the elements
	 * @param nodeset the nodeset of the form group.
	 * @return a {@link XformMultiDisplayable} that could be an xls form group or an xls form repeat.
	 * @throws XformBindingNotFoundException
	 * @throws XformElementPropertiesNotFoundException
	 */
	static XformMultiDisplayable createXformMultiDisplayable(String xform, String nodeset)
			throws XformElementPropertiesNotFoundException, XformBindingNotFoundException {
		XformMultiDisplayable multiDisplayable = new XformMultiDisplayable()
		Node node = findNodeInFormBody(xform, nodeset)
		boolean isRepeat = node.'repeat'.size() > 0
		multiDisplayable.type = isRepeat ? XformMultiDisplayableType.repeat : XformMultiDisplayableType.group
		multiDisplayable.displayables = new ArrayList<>()

		Map<String, String> properties = getTextBoxTypeElementProperties(xform, nodeset)
		multiDisplayable.name = nodeset.substring(nodeset.lastIndexOf('/') + 1)
		multiDisplayable.label = properties.label
		multiDisplayable.hint = properties.hint
		multiDisplayable.hidden = properties.label != null
		if (isRepeat) {
			node.'repeat'.getAt(0).'*'.each { Node subElementNode ->
				XformDisplayable subElementDisplayable = createSubElementDisplayable(subElementNode, xform)
				if (subElementDisplayable) {
					multiDisplayable.displayables.add(subElementDisplayable)
				}
			}
		} else {
			node.'*'.each { Node subElementNode ->
				XformDisplayable subElementDisplayable = createSubElementDisplayable(subElementNode, xform)
				if (subElementDisplayable) {
					multiDisplayable.displayables.add(subElementDisplayable)
				}
			}
		}

		return multiDisplayable
	}

	/**
	 * Helper method for finding an element in the xls form's body tag that has a <tt>ref</tt> that points to the supplied
	 * <tt>nodeset</tt>
	 *
	 * @param xform
	 * @param nodeset
	 * @return
	 * @throws XformElementPropertiesNotFoundException
	 */
	static Node findNodeInFormBody(String xform, String nodeset)
			throws XformElementPropertiesNotFoundException {
		def form = new XmlParser().parseText(xform)
		Node element = form.'h:body'.'**'.find { Node node ->
			node.@ref == nodeset
		}
		if (!element) {
			throw new XformElementPropertiesNotFoundException("Could not find element with reference to nodeset: $nodeset")
		}
		return element
	}

	/**
	 * Helper method for creating elements that belong inside an xls form repeat or xls form group.
	 *
	 * @param node the element node.
	 * @param xform the form in which the element is contained.
	 * @return the {@link XformDisplayable} representation of the element.
	 * @throws XformBindingNotFoundException
	 * @throws XformElementPropertiesNotFoundException
	 */
	static XformDisplayable createSubElementDisplayable(Node node, String xform)
			throws XformBindingNotFoundException, XformElementPropertiesNotFoundException {
		String nodeset = node.@ref
		// skip labels, hints and such
		if (!nodeset) {
			return null
		}
		String name = nodeset.substring(nodeset.lastIndexOf('/') + 1)

		def form = new XmlParser().parseText(xform)
		Node parentElement = form.'*'.model.instance.getAt(0).'**'.find { Node it ->
			com.nexusug.xlsform.util.XmlUtil.getNodeName(it) == name
		}
		String value = parentElement.value()?.getAt(0)
		XformDisplayable subElementDisplayable = createDisplayable(name, value, xform, nodeset)
		return subElementDisplayable
	}

	/**
	 * Helper method for creating a single element displayable.
	 * @param name the name of the element
	 * @param value the value of the element
	 * @param xform the form containing the element
	 * @param nodeset the nodeset to use to lookup the in body tag reference for the element.
	 * @return {@link XformDisplayable} if the element type is supported otherwise return null.
	 * @throws XformBindingNotFoundException
	 * @throws XformElementPropertiesNotFoundException
	 */
	static XformDisplayable createDisplayable(String name, String value, String xform, String nodeset)
			throws XformBindingNotFoundException, XformElementPropertiesNotFoundException {
		XformDisplayable displayable = null
		Map constraints = getElementConstraints(xform, nodeset)
		List<String> textBoxTypes = XformTextBoxType.values().collect { it.xlsTypeName }
		List<String> selectTypes = XformSelectElementType.values().collect { it.name() }
		switch (constraints.type) {
			case textBoxTypes:
				displayable = createXformTextBox(constraints, name, value, xform, nodeset)
				break
			case selectTypes:
				displayable = createXformSelectElement(constraints, name, value, xform, nodeset)
				break
			default:
				break
		}

		return displayable
	}

	/**
	 * checks if a given nodeset belongs to an xform group element.
	 *
	 * @param xform the form that contains the element
	 * @param nodeset the nodeset ref of the element to check for.
	 * @return true if nodeset belongs to a form otherwise false.
	 */
	static boolean isXformGroupElement(String xform, String nodeset) {
		def form = new XmlParser().parseText(xform)
		Node element = form.'h:body'.'**'.find { Node node ->
			node.@ref == nodeset
		}
		if (!element) {
			return false
		}
		String nodeName = com.nexusug.xlsform.util.XmlUtil.getNodeName(element)
		return nodeName.equals('group')
	}

	/**
	 * Helper method for creating a {@link XformSelectElement}
	 *
	 * @param constraints the various constraints of the {@link XformSelectElement} to be created.
	 * @param name the name of the {@link XformSelectElement} that will be created
	 * @param value the value of the {@link XformSelectElement} that will be created
	 * @param xform the form containing the {@link XformSelectElement}
	 * @param nodeset the nodeset of the {@link XformSelectElement}
	 * @return {@link XformSelectElement} non nullable
	 * @throws XformElementPropertiesNotFoundException
	 */
	static XformSelectElement createXformSelectElement(Map constraints, String name,
														   String value, String xform, String nodeset)
			throws XformElementPropertiesNotFoundException {
		XformSelectElement displayable = new XformSelectElement()
		displayable.readOnly = constraints.readonly?.equals('true()') ?: false
		displayable.required = constraints.required?.equals('true()') ?: false
		displayable.requiredMsg = constraints.requiredMsg

		displayable.type = XformSelectElementType."${constraints.type}"

		Map<String, String> properties = getTextBoxTypeElementProperties(xform, nodeset)
		displayable.label = properties.label
		displayable.hint = properties.hint
		displayable.hidden = (properties.label == null)
		displayable.appearance = properties.appearance

		displayable.items = getSelectTypeElementItems(xform, nodeset)
		if (!displayable.items) {
			displayable.itemset = getSelectTypeElementItemset(xform, nodeset)
		}

		displayable.name = name
		displayable.value = value
		return displayable
	}

	/**
	 * Fetches the itemset that could be found in a select element.
	 *
	 * @param xform the form containing the elements.
	 * @param nodeset the nodeset to use for getting the parent element that could contain an <tt>itemset</tt>
	 * @return the <tt>XformSelectItemset</tt> if any is found, otherwise return null
	 * @throws XformElementPropertiesNotFoundException if no element's ref matched the provided <tt>nodeset</tt>
	 */
	static XformSelectItemset getSelectTypeElementItemset(String xform, String nodeset)
			throws XformElementPropertiesNotFoundException {
		def form = new XmlParser().parseText(xform)
		Node element = form.'h:body'.'**'.find { Node node ->
			node.@ref == nodeset
		}

		if (!element) {
			throw new XformElementPropertiesNotFoundException("Could not find element referring to nodeset ref: $nodeset")
		}

		Node itemsetNode = element.itemset?.getAt(0) as Node
		if (!itemsetNode) {
			return null
		}
		XformSelectItemset itemset = new XformSelectItemset()
		itemset.instanceItemFilters = extractItemsetInstanceFilters(itemsetNode.@nodeset)
		itemset.instanceRef = extractItemsetInstanceRef(itemsetNode.@nodeset)
		itemset.valueRef = itemsetNode.value ? itemsetNode.value.getAt(0).@ref : null
		String labelRef = itemsetNode.label ? itemsetNode.label.getAt(0).@ref : null
		if (labelRef) {
			itemset.labelRef = XformTranslatedValueIndicator.generateDynamicValue(extractItextReference(labelRef))
		}

		return itemset
	}

	/**
	 * Gets a list of filters associated with an <tt>itemset</tt> <tt>nodeset</tt>.
	 * @param nodeset the nodeset from which to extract the <tt>XformSelectItemsetInstanceFilter</tt>s
	 * @return non null list of <tt>XformSelectItemsetInstanceFilter</tt>s
	 */
	static List<XformSelectItemsetInstanceFilter> extractItemsetInstanceFilters(String nodeset) {
		List<XformSelectItemsetInstanceFilter> filterList = new ArrayList<XformSelectItemsetInstanceFilter>()
		if (!nodeset?.startsWith('instance')) {
			return filterList
		}

		String[] base = nodeset.split('\\)')[1].split("\\[")
		def baseRef = base[0].trim()

		def instanceFilterString = base[1].substring(0, base[1].indexOf("]")).trim()
		instanceFilterString.split('and').each { String filter ->
			XformSelectItemsetInstanceFilter instanceFilter = new XformSelectItemsetInstanceFilter()

			String[] instanceRefValueArray = filter.split("=")
			instanceFilter.itemPropertyRef = "${baseRef}/${instanceRefValueArray[0].trim()}".trim()
			instanceFilter.instanceValueRef = instanceRefValueArray[1].trim()
			filterList.add(instanceFilter)
		}

		return filterList
	}

	/**
	 * extracts the instance name from a <tt>nodeset</tt> ref of an <tt>itemset</tt>. Typical example could be
	 * <tt>instance('counties')/root/</tt>, in that case the instance name is <tt>counties</tt>. <br />Handles null values
	 * @param nodeset the string from which to extract the instance name. supplying null for this will result into null be returned.
	 * @return the instance name or the original value if it doesn't seem to be an instance name, could return null if the supplied
	 * <tt>nodeset</tt> is null.
	 */
	static String extractItemsetInstanceRef(String nodeset) {
		if (!nodeset?.startsWith('instance')) {
			return nodeset
		}
		return nodeset.substring(nodeset.indexOf("('") + 2, nodeset.indexOf("')"))
	}

	/**
	 * gets the text being referred to in a <tt>jr:itext</tt>. Can handle null values for <tt>text</tt>
	 *
	 * @param text the text to extract the <tt>itext</tt> from.
	 * @return text contained in <tt>jr:itext</tt>, otherwise returns the entire supplied <tt>text</tt>
	 */
	static String extractItextReference(String text) {
		if (!text?.startsWith("jr:itext")) {
			return text
		}
		def itextRef = text.substring(text.indexOf('(') + 1, text.indexOf(')'))
		if (itextRef.startsWith("'") && itextRef.endsWith("'")) {
			itextRef = itextRef.substring(itextRef.indexOf("'") + 1, itextRef.lastIndexOf("'"))
		}
		return itextRef
	}

	/**
	 * returns a list of {@link XformSelectItem}s contained in a specified {@link XformSelectItem}.
	 *
	 * @param xform the form containing the items.
	 * @param nodeset the nodeset ref of the {@link XformSelectItem}
	 * @return non nullable list of {@link XformSelectItem}s
	 * @throws XformElementPropertiesNotFoundException if no element in the xform body was referencing the specified <tt>nodeset</tt>.
	 */
	static List<XformSelectItem> getSelectTypeElementItems(String xform, String nodeset)
			throws XformElementPropertiesNotFoundException {
		def form = new XmlParser().parseText(xform)
		Node element = form.'h:body'.'**'.find { Node node ->
			node.@ref == nodeset
		}
		if (!element) {
			throw new XformElementPropertiesNotFoundException("Could not find element referring to nodeset ref: $nodeset")
		}
		List<XformSelectItem> items = new ArrayList<>()
		element.item?.each { Node node ->
			XformSelectItem item = new XformSelectItem()
			item.value = node.value ? node.value[0].value()?.getAt(0) : null
			item.label = node.label ? node.label[0].value()?.getAt(0) : null
			items.add(item)
		}
		return items
	}

	/**
	 * Helper method for creating a {@link XformTextBox}
	 *
	 * @param constraints the various constraints of the {@link XformTextBox} to be created.
	 * @param name the name of the {@link XformTextBox} that will be created
	 * @param value the value of the {@link XformTextBox} that will be created
	 * @param xform the form containing the {@link XformTextBox}
	 * @param nodeset the nodeset of the {@link XformTextBox}
	 * @return {@link XformTextBox} non nullable
	 * @throws XformBindingNotFoundException
	 * @throws XformElementPropertiesNotFoundException
	 */
	static XformTextBox createXformTextBox(Map constraints, String name,
											   String value, String xform, String nodeset)
			throws XformBindingNotFoundException, XformElementPropertiesNotFoundException {
		XformTextBox displayable = new XformTextBox()
		displayable.readOnly = constraints.readonly?.equals('true()') ?: false
		displayable.required = constraints.required?.equals('true()') ?: false
		displayable.type = XformTextBoxType.findByXlsTypeName(constraints.type)
		displayable.requiredMsg = constraints.requiredMsg
		if (isConfigurationElement(xform, nodeset)) {
			displayable.hidden = true
			displayable.readOnly = true
		} else {
			Map<String, String> properties = getTextBoxTypeElementProperties(xform, nodeset)
			displayable.label = properties.label
			displayable.hint = properties.hint
			displayable.appearance = properties.appearance
			displayable.hidden = (properties.label == null)
		}
		displayable.name = name
		displayable.value = value
		return displayable
	}

	/**
	 * checks if a given element nodeset ref belongs to a configuration element.
	 *
	 * @param xform the form to check through
	 * @param nodeset nodeset to check
	 * @return true if the nodeset belongs to a configuration element, false otherwise
	 * @throws XformBindingNotFoundException if the specified nodeset matched no binding.
	 */
	static boolean isConfigurationElement(String xform, String nodeset)
			throws XformBindingNotFoundException {
		def form = new XmlParser().parseText(xform)
		Node element = form.'h:head'.model.bind.find { Node node ->
			node.@nodeset == nodeset
		}
		if (!element) {
			throw new XformBindingNotFoundException("Could not find a binding matching nodeset: ${nodeset}")
		}
		Node elementStructure = form.'h:body'.'**'.find { Node node ->
			node.@ref == nodeset
		}
		return elementStructure == null
	}

	/**
	 * Gets the properties of an Xform element of type input that isn't a configuration.
	 *
	 * @param xform
	 * @param elementNodeSet
	 * @throws XformElementPropertiesNotFoundException if no element with ref matching the specified nodeset is found.
	 * @return non nullable map of element child properties against their values.
	 */
	static Map<String, String> getTextBoxTypeElementProperties(String xform, String elementNodeSet)
			throws XformElementPropertiesNotFoundException {
		def form = new XmlParser().parseText(xform)
		Node element = form.'h:body'.'**'.find { Node node ->
			node.@ref == elementNodeSet
		}
		if (!element) {
			throw new XformElementPropertiesNotFoundException("Could not find element with reference to nodeset: $elementNodeSet")
		}
		Map<String, String> properties = new HashMap<>()
		element.'*'.each { Node node ->
			String nodeName = com.nexusug.xlsform.util.XmlUtil.getNodeName(node)
			if (!['item', 'repeat'].contains(nodeName)) {
				properties.put(nodeName, node.value()?.getAt(0))
			}
		}
		properties.put('appearance', element.@appearance)
		return properties
	}

	/**
	 * Get all the constraints of a given element. The constraints are usually the attributes of the binding
	 *
	 * @param xform the form containing the element.
	 * @param elementNodeSet the binding nodeset of the element
	 * @return Map containing all the constraints of the specified nodeset.
	 * @throws XformBindingNotFoundException if no binding matched the specified elementNodeSet.
	 */
	static Map getElementConstraints(String xform, String elementNodeSet)
			throws XformBindingNotFoundException {
		def form = new XmlParser().parseText(xform)
		Node binding = form.'h:head'.model.bind.find { Node node ->
			node.@nodeset == elementNodeSet
		}
		if (!binding) {
			throw new XformBindingNotFoundException("Could not find binding for nodeset: $elementNodeSet")
		}
		Map attributes = new HashMap()
		binding.attributes().each { key, attribute ->
			if (key instanceof groovy.xml.QName && key.namespaceURI.equals(JAVAROSA_NAMESPACE_URI)) {
				attributes[key.localPart] = attribute
			} else {
				attributes[key] = attribute
			}
		}
		return attributes
	}


	static XformDisplayable bindContraints(String xform, String formName, XformDisplayable displayable) {
		//should recursively bind elements if multi displayable element detected.
	}

	void bindMetaDataContraints() {
		//TODO think about me!
	}
}
