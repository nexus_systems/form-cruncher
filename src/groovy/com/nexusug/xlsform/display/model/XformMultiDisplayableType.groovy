package com.nexusug.xlsform.display.model

/**
 * Created by ivan on 29/11/2015.
 */
enum XformMultiDisplayableType {
	group,
	repeat
}