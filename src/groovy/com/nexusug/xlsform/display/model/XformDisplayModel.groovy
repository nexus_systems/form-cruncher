package com.nexusug.xlsform.display.model

/**
 * Front end representation of an <tt>Xform</tt>
 *
 * Created by ivan on 29/11/2015.
 */
class XformDisplayModel {
	String title
	String xlsId
	String name
	Long xformId

	List<XformDisplayable> displayables
}
