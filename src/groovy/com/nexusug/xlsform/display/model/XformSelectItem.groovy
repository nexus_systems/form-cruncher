package com.nexusug.xlsform.display.model

/**
 * Created by ivan on 29/11/2015.
 */
class XformSelectItem {
	String label
	String value

	@Override
	public String toString() {
		return "XformSelectItem{" +
				"label='" + label + '\'' +
				", value='" + value + '\'' +
				'}';
	}

	@Override
	boolean equals(Object object) {
		if (this.is(object)) return true
		if (getClass() != object.class) return false

		XformSelectItem that = (XformSelectItem) object

		if (label != that.label) return false
		if (value != that.value) return false

		return true
	}

	@Override
	int hashCode() {
		int result
		result = (label != null ? label.hashCode() : 0)
		result = 31 * result + (value != null ? value.hashCode() : 0)
		return result
	}
}
