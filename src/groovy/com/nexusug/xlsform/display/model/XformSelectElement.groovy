package com.nexusug.xlsform.display.model

/**
 * Created by ivan on 29/11/2015.
 */
class XformSelectElement extends XformDisplayable {
	XformSelectElementType type
	List<XformSelectItem> items

	// this is only useful for cascading selects
	XformSelectItemset itemset
}
