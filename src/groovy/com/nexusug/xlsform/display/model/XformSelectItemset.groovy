package com.nexusug.xlsform.display.model

/**
 * This is useful for cascading selects.
 *
 * Created by ivan on 29/11/2015.
 */
class XformSelectItemset {
	// the instance the itemset belongs to
	String instanceRef

	// filters used to determine which items are eligible for selection in the instance that the itemset belongs
	List<XformSelectItemsetInstanceFilter> instanceItemFilters

	// how to resolve the label of each item got from the ${link itemGroupSelector}
	String labelRef

	// how to resolve the value of each item got from the ${link itemGroupSelector}
	String valueRef

	@Override
	boolean equals(o) {
		if (this.is(o)) return true
		if (getClass() != o.class) return false

		XformSelectItemset that = (XformSelectItemset) o

		if (instanceItemFilters != that.instanceItemFilters) return false
		if (instanceRef != that.instanceRef) return false
		if (labelRef != that.labelRef) return false
		if (valueRef != that.valueRef) return false

		return true
	}

	@Override
	int hashCode() {
		int result
		result = (instanceRef != null ? instanceRef.hashCode() : 0)
		result = 31 * result + (instanceItemFilters != null ? instanceItemFilters.hashCode() : 0)
		result = 31 * result + (labelRef != null ? labelRef.hashCode() : 0)
		result = 31 * result + (valueRef != null ? valueRef.hashCode() : 0)
		return result
	}

	@Override
	public String toString() {
		return "XformSelectItemset{" +
				"instanceRef='" + instanceRef + '\'' +
				", instanceItemFilters=" + instanceItemFilters +
				", labelRef='" + labelRef + '\'' +
				", valueRef='" + valueRef + '\'' +
				'}';
	}
}

class XformSelectItemsetInstanceFilter {
	String itemPropertyRef
	String instanceValueRef

	@Override
	public String toString() {
		return "XformSelectItemsetInstanceFilter{" +
				"itemPropertyRef='" + itemPropertyRef + '\'' +
				", instanceValueRef='" + instanceValueRef + '\'' +
				'}';
	}

	@Override
	boolean equals(o) {
		if (this.is(o)) return true
		if (getClass() != o.class) return false

		XformSelectItemsetInstanceFilter that = (XformSelectItemsetInstanceFilter) o

		if (itemPropertyRef != that.itemPropertyRef) return false
		if (instanceValueRef != that.instanceValueRef) return false

		return true
	}

	@Override
	int hashCode() {
		int result
		result = (itemPropertyRef != null ? itemPropertyRef.hashCode() : 0)
		result = 31 * result + (instanceValueRef != null ? instanceValueRef.hashCode() : 0)
		return result
	}
}