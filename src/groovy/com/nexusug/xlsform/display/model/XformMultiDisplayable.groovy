package com.nexusug.xlsform.display.model

/**
 * This is a displayable that can contain other displayable. Typical of 'repeats' and 'form group's
 * Created by ivan on 29/11/2015.
 */
class XformMultiDisplayable extends XformDisplayable {
	XformMultiDisplayableType type
	List<XformDisplayable> displayables
}
