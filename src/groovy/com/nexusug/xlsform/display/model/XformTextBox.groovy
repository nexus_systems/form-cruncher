package com.nexusug.xlsform.display.model

/**
 * Created by ivan on 29/11/2015.
 */
class XformTextBox extends XformDisplayable {
	XformTextBoxType type

	@Override
	public String toString() {
		return "XformTextBox{" +
				"type=" + type +
				"} " + super.toString();
	}
}
