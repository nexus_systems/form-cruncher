package com.nexusug.xlsform.display.model

/**
 * Created by ivan on 29/11/2015.
 */
enum XformTextBoxType {
	integer('int'),
	decimal('decimal'),
	string('string'),
	date('date'),
	time('time'),
	dateTime('dateTime'),

	final String xlsTypeName

	XformTextBoxType(String name) {
		this.xlsTypeName = name
	}

	static XformTextBoxType findByXlsTypeName(String name) {
		values().find {
			it.xlsTypeName == name
		}
	}


	@Override
	public String toString() {
		return xlsTypeName
	}
}