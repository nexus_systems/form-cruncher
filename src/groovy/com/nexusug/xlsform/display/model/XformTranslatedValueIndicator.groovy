package com.nexusug.xlsform.display.model

/**
 * Utility method for creating strings the imply they are i18n keys rather than actual values.
 *
 * Created by ivan on 03/12/2015.
 */
class XformTranslatedValueIndicator {

	/**
	 * Returns a string prefixed with the global indicator for dynamic values that are internationalized
	 *
	 * @param value the value to prefix with the i18n signaller.
	 * @return a string prefixed with the global indicator for i18n strings
	 */
	static String generateDynamicValue(String value) {
		return "com.nexus.xlsfom.dynamic-itext:${value}"
	}

	/**
	 * Returns a string prefixed with the global indicator for fixed values that are internationalized
	 *
	 * @param value the value to prefix with the i18n signaller.
	 * @return a string prefixed with the global indicator for i18n strings
	 */
	static String generateAbsoluteValue(String value) {
		return "com.nexus.xlsfom.absolute-itext:${value}"
	}
}
