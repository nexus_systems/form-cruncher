package com.nexusug.xlsform.display.model

/**
 * Xform data type options available for meta data collection
 * Created by ivan on 27/03/2016.
 */
enum XformMetaData {
	meta,
	phonenumber,
	simserial,
	subscriberid,
	deviceid,
	today,
	end,
	start
}