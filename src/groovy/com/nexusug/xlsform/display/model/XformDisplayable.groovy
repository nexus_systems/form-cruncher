package com.nexusug.xlsform.display.model

/**
 * Created by ivan on 29/11/2015.
 */
abstract class XformDisplayable {
	String label
	String name
	String hint
	String value

	String relevant

	boolean readOnly
	boolean required
	String requiredMsg
	boolean hidden
	String appearance

	@Override
	public String toString() {
		return "XformDisplayable{" +
				"label='" + label + '\'' +
				", name='" + name + '\'' +
				", hint='" + hint + '\'' +
				", value='" + value + '\'' +
				", relevant='" + relevant + '\'' +
				", readOnly=" + readOnly +
				", required=" + required +
				", requiredMsg=" + requiredMsg +
				", hidden=" + hidden +
				", appearance=" + appearance +
				'}';
	}

	abstract def getType()
}