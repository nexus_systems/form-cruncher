package com.nexusug.xlsform.reporting

/**
 * The different ways two or more conditions can be linked to each other.
 *
 * Created by ivan on 24/12/2015.
 */
enum ConditionLinker {
	OR,
	AND
}