package com.nexusug.xlsform.reporting

import com.nexusug.xlsform.XformQuestion
import com.nexusug.xlsform.XformSubmission

/**
 * Contains methods for processing of reports whose conditions have questions that span different {@link com.nexusug.xlsform.Xform} types.
 * Created by ivan on 08/01/2016.
 */
class MultiFormReportHelper {

	/**
	 * Helper method to collect all the submissions that have a given <tt>formLiner</tt> with a given <tt>formLinkerValue</tt>
	 * @param formLinker the formLinker question
	 * @param formLinkerValue the value of the <tt>formLinker</tt>
	 * @param submissions the submissions to filter
	 * @return non null list of {@link com.nexusug.xlsform.XformSubmission}s
	 */
	static List<XformSubmission> extractRelatedSubmissionsBasedOnFormLinker(String formLinker, List<String> formLinkerValue, List<XformSubmission> submissions) {
		List<XformSubmission> relatedSubmissions = submissions.findAll {
			it.questions.any {
				it.question == formLinker && it.answers && !Collections.disjoint(it.answers*.value, formLinkerValue ?: [])
			}
		}
		return relatedSubmissions
	}

	/**
	 * Determines if a given set of related {@link XformSubmission}s together meet a given list of {@link Condition}s
	 * @param submissions list of {@link XformSubmission}s that are related in some way. Typically using a {@link Report#formLinkerQuestion}
	 * @param currentEvaluationStatus the result of evaluation of the first {@link XformSubmission} from which the related {@link XformSubmission}s were derived.
	 * @param conditions list of {@link Condition}s to use in evaluating the <tt>submissions</tt>
	 * @param conditionLinkers list of {@link ConditionLinker}s to use when linking up subsequent {@link Condition} evaluation results.
	 * @return true if validation passes, otherwise false.
	 */
	static boolean evaluateRelatedSubmissionList(List<XformSubmission> submissions, boolean currentEvaluationStatus,
												 List<Condition> conditions, List<ConditionLinker> conditionLinkers) {
		if (conditions) {
			boolean evaluationResult
			int index = 0
			ConditionLinker currentConditionLinker = conditionLinkers ? conditionLinkers.get(0) : null
			if (!(currentEvaluationStatus && currentConditionLinker == ConditionLinker.OR)) {
				conditionsLoop:
				for (Condition condition : conditions) {
					XformSubmission submission = submissions.find { XformSubmission sub ->
						sub.questions.any { it.question == condition.questionKey }
					}
					evaluationResult = submission ? ReportUtils.evaluateSubmission(condition, submission) : false

					if ((currentConditionLinker = conditionLinkers[index])) {
						switch (currentConditionLinker.name()) {
							case ConditionLinker.AND.name():
								currentEvaluationStatus = currentEvaluationStatus && evaluationResult
								break
							case ConditionLinker.OR.name():
								currentEvaluationStatus = currentEvaluationStatus || evaluationResult
								break
						}
					}
					if (currentEvaluationStatus && conditionLinkers[index + 1] == ConditionLinker.OR) {
						break conditionsLoop
					}
					++index
				}
			}
		}
		return currentEvaluationStatus
	}

	/**
	 * Filters out submissions that meet the provided set of conditions in a {@link Report} whose conditions have questions
	 * that span multiple {@link com.nexusug.xlsform.Xform} types.
	 * @param report the report
	 * @param submissionList the list of {@link XformSubmission} instances that are to be evaluated.
	 * @return non null list of {@link ReportEntry} instances from the provided <tt>submissionList</tt>
	 */
	static List<ReportEntry> generateReportEntries(Report report, List<XformSubmission> submissionList) {
		List<XformSubmission> submissions = (List<XformSubmission>) submissionList.clone()
		List<ReportEntry> reportEntries = []
		List<Condition> conditions = (List<Condition>) report.conditions?.clone()
		List<ConditionLinker> conditionLinkers = (List<ConditionLinker>) report.conditionLinkers?.clone()

		while (conditions && submissions) {
			String formLinker = report.formLinkerQuestion
			Condition condition = conditions.remove(0)

			List<XformSubmission> conditionSubmissions = submissions.findAll {
				it.questions.any { it.question == condition.questionKey }
			}
			submissions.removeAll(conditionSubmissions)

			conditionSubmissions.each { XformSubmission submission ->

				boolean evaluationPassed = ReportUtils.evaluateSubmission(condition, submission)
				List<XformAnswer> xformLinkerAnswers = submission.questions.find { XformQuestion xformQuestion ->
					xformQuestion.question == formLinker
				}?.answers

				List<String> formLinkerValue = xformLinkerAnswers ? xformLinkerAnswers*.value : []
				List<XformSubmission> relatedSubmissions = extractRelatedSubmissionsBasedOnFormLinker(formLinker, formLinkerValue, submissions)

				relatedSubmissions.add(0, submission)
				evaluationPassed = evaluateRelatedSubmissionList(relatedSubmissions, evaluationPassed, conditions, conditionLinkers)

				if (evaluationPassed) {
					reportEntries.add(new ReportEntry(submissions: relatedSubmissions))
				}
				submissions.removeAll(relatedSubmissions)
			}
			if (conditionLinkers) {
				conditionLinkers.remove(0)
			}
		}
		return reportEntries
	}
}
