package com.nexusug.xlsform.reporting

/**
 * The different supported operators for creating conditions.
 *
 * Created by ivan on 24/12/2015.
 */
enum ConditionOperator {
	TEXT_EQUAL{
		@Override
		boolean evaluate(String constant, String variable) {
			if (!constant || !variable) {
				return constant == variable
			}
			return constant.equalsIgnoreCase(variable)
		}

		@Override
		boolean canValidate(String value) {
			return true
		}

		@Override
		String generateHql(String property, String value) {
			return "UPPER($property) = UPPER($value) "
		}

		@Override
		String userFriendlyName() {
			return 'is the same as'
		}
	},

	TEXT_CONTAINS{
		@Override
		boolean evaluate(String constant, String variable) {
			if (!constant || !variable) {
				return false
			}
			return constant.toLowerCase().contains(variable.toLowerCase())
		}

		@Override
		boolean canValidate(String value) {
			return true
		}

		@Override
		String generateHql(String property, String value) {
			return "UPPER($property) LIKE UPPER($value) "
		}

		@Override
		String userFriendlyName() {
			return 'contains'
		}

		@Override
		String processOperand(String operand) {
			return "%$operand%"
		}
	},

	TEXT_BEGINS_WITH{
		@Override
		boolean evaluate(String constant, String variable) {
			if (!constant || !variable) {
				return false
			}
			return constant.toLowerCase().startsWith(variable.toLowerCase())
		}

		@Override
		boolean canValidate(String value) {
			return true
		}

		@Override
		String generateHql(String property, String value) {
			return "UPPER($property) LIKE UPPER($value) "
		}

		@Override
		String userFriendlyName() {
			return 'begins with'
		}

		@Override
		String processOperand(String operand) {
			return "$operand%"
		}
	},

	TEXT_ENDS_WITH{
		@Override
		boolean evaluate(String constant, String variable) {
			if (!constant || !variable) {
				return false
			}
			return constant.toLowerCase().endsWith(variable.toLowerCase())
		}

		@Override
		boolean canValidate(String value) {
			return true
		}

		@Override
		String generateHql(String property, String value) {
			return "UPPER($property) LIKE UPPER($value) "
		}

		@Override
		String userFriendlyName() {
			return 'ends with'
		}

		@Override
		String processOperand(String operand) {
			return "%$operand"
		}
	},

	NUMERIC_EQUAL{
		@Override
		boolean evaluate(String constant, String variable) {
			if (!constant || !variable) {
				return false
			}
			return constant.isNumber() && variable.isNumber() && ((constant as BigDecimal) == (variable as BigDecimal))
		}

		@Override
		boolean canValidate(String value) {
			return value?.isNumber()
		}

		@Override
		String generateHql(String property, String value) {
			return "regexp($property, '^([0-9]+)?(.[0-9]+)?\$') <> 0 AND $property = $value "
		}

		@Override
		String userFriendlyName() {
			return 'is equal to'
		}
	},

	GREATER_THAN{
		@Override
		boolean evaluate(String constant, String variable) {
			if (!constant || !variable) {
				return false
			}
			return constant.isNumber() && variable.isNumber() && ((constant as BigDecimal) > (variable as BigDecimal))
		}

		@Override
		boolean canValidate(String value) {
			return value?.isNumber()
		}

		@Override
		String generateHql(String property, String value) {
			return "regexp($property, '^([0-9]+)?(.[0-9]+)?\$') <> 0 AND $property > $value "
		}

		@Override
		String userFriendlyName() {
			return 'is greater than'
		}
	},

	LESS_THAN{
		@Override
		boolean evaluate(String constant, String variable) {
			if (!constant || !variable) {
				return false
			}
			return constant.isNumber() && variable.isNumber() && ((constant as BigDecimal) < (variable as BigDecimal))
		}

		@Override
		boolean canValidate(String value) {
			return value?.isNumber()
		}

		@Override
		String generateHql(String property, String value) {
			return "regexp($property, '^([0-9]+)?(.[0-9]+)?\$') <> 0 AND $property < $value "
		}

		@Override
		String userFriendlyName() {
			return 'is less than'
		}
	},

	LESS_EQUAL{
		@Override
		boolean evaluate(String constant, String variable) {
			if (!constant || !variable) {
				return false
			}
			return constant.isNumber() && variable.isNumber() && ((constant as BigDecimal) <= (variable as BigDecimal))
		}

		@Override
		boolean canValidate(String value) {
			return value?.isNumber()
		}

		@Override
		String generateHql(String property, String value) {
			return "regexp($property, '^([0-9]+)?(.[0-9]+)?\$') <> 0 AND $property <= $value "
		}

		@Override
		String userFriendlyName() {
			return 'is equal to'
		}
	},

	GREATER_EQUAL{
		@Override
		boolean evaluate(String constant, String variable) {
			if (!constant || !variable) {
				return false
			}
			return constant.isNumber() && variable.isNumber() && ((constant as BigDecimal) >= (variable as BigDecimal))
		}

		@Override
		boolean canValidate(String value) {
			return value?.isNumber()
		}

		@Override
		String generateHql(String property, String value) {
			return "regexp($property, '^([0-9]+)?(.[0-9]+)?\$') <> 0 AND $property >= $value "
		}

		@Override
		String userFriendlyName() {
			return 'is greater than'
		}
	}

	/**
	 * Compares two values based on the type of operator
	 * @param constant the known value
	 * @param variable the unknown value to be compared against
	 * @return true if value and operand are equal as evaluated by the operator.
	 */
	abstract boolean evaluate(String constant, String variable)

	/**
	 * Checks if the provided <tt>value</tt> can be evaluated by the given operator.
	 * @param value the value
	 * @return true if <tt>value</tt> can be validated, otherwise false.
	 */
	abstract boolean canValidate(String value)

	/**
	 * Generates an HQL snippet that can be added to a parent HQL query in which the operator is being used
	 * @param property the domain property
	 * @param value the value to be used for comparison. Normally this will be a named query parameter.
	 * @return the hql snippet
	 */
	abstract String generateHql(String property, String value)

	abstract String userFriendlyName()

	/**
	 * Manipulates the <tt>operand</tt> so that it makes sense when used in HQL with respect to the operator. This is
	 * especially useful for text based operators like {@link #TEXT_BEGINS_WITH}.
	 * @param operand the value to be used as an operand in the hql query.
	 * @return the manipulated value
	 */
	String processOperand(String operand) {
		return operand
	}
}
