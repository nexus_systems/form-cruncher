package com.nexusug.xlsform.reporting

import com.nexusug.xlsform.XformQuestion
import com.nexusug.xlsform.XformSubmission

/**
 * Contains methods that help in processing of a given {@link Report}
 *
 * Created by ivan on 08/01/2016.
 */
class ReportUtils {

	/**
	 * Evaluates a form against a condition.
	 *
	 * @param condition
	 * @param submission
	 * @return
	 */
	static boolean evaluateSubmission(Condition condition, XformSubmission submission) {
		boolean result = submission.questions.any { XformQuestion formQuestion ->
			formQuestion.question == condition.questionKey && formQuestion.answers.any {
				condition.operator.evaluate(it.value, condition.operand)
			}
		}
		return result
	}
}
