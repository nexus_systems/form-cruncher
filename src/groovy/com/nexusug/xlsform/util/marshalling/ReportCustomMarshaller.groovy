package com.nexusug.xlsform.util.marshalling

import com.nexusug.xlsform.reporting.Report
import grails.converters.JSON

/**
 * Created by ivan on 12/03/2016.
 */
class ReportCustomMarshaller implements CustomMarshaller {

	void register() {
		JSON.registerObjectMarshaller(Report) { Report report ->
			Map properties = new HashMap()
			properties['id'] = report.id
			properties['name'] = report.name
			properties['description'] = report.description
			properties['conditions'] = report.conditions?.collect { condition ->
				['id': condition.id, 'questionKey': condition.questionKey, 'operator': condition.operator.name(), 'operand': condition.operand, 'formXlsId': condition.formXlsId]
			}
			properties['conditionLinkers'] = report.conditionLinkers.collect {
				it.name()
			}

			properties['displayColumns'] = report.displayColumns?.collect { column ->
				['id': column.id, 'question': column.question, 'displayName': column.displayName]
			}
			properties['formLinkerQuestion'] = report.formLinkerQuestion

			return properties
		}
	}
}
