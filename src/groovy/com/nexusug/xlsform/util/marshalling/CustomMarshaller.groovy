package com.nexusug.xlsform.util.marshalling

/**
 * To be implemented by all Custom marshallers in the app.
 *
 * Created by ivan on 31/01/2016.
 */
interface CustomMarshaller {
	void register()
}