package com.nexusug.xlsform.util.marshalling

import javax.annotation.PostConstruct

/**
 * Registers all Custom marshallers in the app.
 *
 * Created by ivan on 31/01/2016.
 */
class CustomMarshallerRegistrar {

	static final List<CustomMarshaller> marshallers = [
			new ValidationErrorsMarshaller(),
			new XformSubmissionMarshaller(),
			new ReportCustomMarshaller()
	]

	@PostConstruct
	void registerMarshallers() {
		marshallers.each {
			it.register()
		}
	}
}
