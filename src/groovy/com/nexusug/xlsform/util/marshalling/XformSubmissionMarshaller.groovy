package com.nexusug.xlsform.util.marshalling

import com.nexusug.xlsform.XformSubmission
import grails.converters.JSON

/**
 * Marshals {@link com.nexusug.xlsform.XformSubmission} instances to return the;
 * <ul>
 *     <li><tt>dateCreated</tt></li>
 *     <li><tt>lastUpdated</tt></li>
 *     <li><tt>metaData</tt> map</li>
 *     <li>all questions as top level elements with their respective answers being</li>
 * </ul>
 *
 * Created by ivan on 28/02/2016.
 */
class XformSubmissionMarshaller implements CustomMarshaller {
	void register() {
		JSON.registerObjectMarshaller(XformSubmission) { XformSubmission submission ->
			Map properties = new HashMap()
			properties['dateCreated'] = submission.dateCreated
			properties['lastUpdated'] = submission.lastUpdated
			properties['metaData'] = submission.metaData
			submission.questions.each { question ->
				properties[question.question] = question.answers*.value
			}
			return properties
		}
	}
}
