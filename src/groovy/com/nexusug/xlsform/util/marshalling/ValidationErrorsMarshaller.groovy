package com.nexusug.xlsform.util.marshalling

import grails.converters.JSON
import grails.util.Holders
import grails.validation.ValidationErrors
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.validation.FieldError

/**
 * Custom marshaller for {@link ValidationErrors}, only returns the {@link FieldError}s contained in the {@link ValidationErrors}.
 *
 * Created by ivan on 31/01/2016.
 */
class ValidationErrorsMarshaller implements CustomMarshaller{

	void register() {
		JSON.registerObjectMarshaller(ValidationErrors) {
			return it.getAllErrors().collect {
				if (it instanceof FieldError) {
					Locale locale = LocaleContextHolder.getLocale();
					def msg = Holders.grailsApplication.mainContext.getBean('messageSource').getMessage(it, locale)
					return [field: it.getField(), message: msg]
				}
			}
		}
	}
}
