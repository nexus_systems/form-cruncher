package com.nexusug.xlsform.db.dialect

import org.hibernate.dialect.function.SQLFunctionTemplate
import org.hibernate.type.StandardBasicTypes

/**
 * Implementation of <tt>H2Dialect</tt> to cater for addition of new functions that are needed but are not
 * included in hibernate by default. <br />
 * So far this is adding the <tt>regexp</tt> function.
 *
 * Created by ivan on 28/12/2015.
 */
class SwaliH2Dialect extends org.hibernate.dialect.H2Dialect {
	public SwaliH2Dialect() {
		super()
		this.registerFunction("regexp", new SQLFunctionTemplate(StandardBasicTypes.BOOLEAN, "?1 regexp ?2"));
	}
}
