package com.nexusug.xlsform.exception

/**
 * Thrown when a specified binding could not be found.
 *
 * Created by ivan on 30/11/2015.
 */
class XformElementPropertiesNotFoundException extends Exception {
	public XformElementPropertiesNotFoundException() {
		super()
	}

	public XformElementPropertiesNotFoundException(String message) {
		super(message);
	}

	public XformElementPropertiesNotFoundException(Throwable cause) {
		super(cause);
	}

	public XformElementPropertiesNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public XformElementPropertiesNotFoundException(String message, Throwable cause,
										   boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
