package com.nexusug.xlsform.exception

import com.nexusug.xlsform.XformSubmission

/**
 * Thrown when a given {@link XformSubmission} does not contain a required property.
 * Created by ivan on 26/03/2016.
 */
class XlsFormConversionException extends Exception {
	public XlsFormConversionException() {
		super()
	}

	public XlsFormConversionException(String message) {
		super(message);
	}

	public XlsFormConversionException(Throwable cause) {
		super(cause);
	}

	public XlsFormConversionException(String message, Throwable cause) {
		super(message, cause);
	}

	public XlsFormConversionException(String message, Throwable cause,
							 boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
