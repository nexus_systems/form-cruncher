package com.nexusug.xlsform.exception

import com.nexusug.xlsform.XformSubmission

/**
 * Thrown when a given {@link XformSubmission} does not contain a required property.
 * Created by ivan on 26/03/2016.
 */
class InvalidSubmission extends Exception {
	public InvalidSubmission() {
		super()
	}

	public InvalidSubmission(String message) {
		super(message);
	}

	public InvalidSubmission(Throwable cause) {
		super(cause);
	}

	public InvalidSubmission(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidSubmission(String message, Throwable cause,
							 boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
