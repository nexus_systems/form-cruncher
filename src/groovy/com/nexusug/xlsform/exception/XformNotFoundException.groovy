package com.nexusug.xlsform.exception

/**
 * Thrown when a specified <tt>Xform</tt> could not be found.
 *
 * Created by ivan on 30/11/2015.
 */
class XformNotFoundException extends Exception {
	public XformNotFoundException() {
		super()
	}

	public XformNotFoundException(String message) {
		super(message);
	}

	public XformNotFoundException(Throwable cause) {
		super(cause);
	}

	public XformNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public XformNotFoundException(String message, Throwable cause,
										   boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
