package com.nexusug.xlsform.exception
/**
 * Thrown when a specified {@link com.nexusug.xlsform.XformSubmission} could not be found.
 *
 * Created by ivan on 30/11/2015.
 */
class XformSubmissionNotFoundException extends Exception {
	public XformSubmissionNotFoundException() {
		super()
	}

	public XformSubmissionNotFoundException(String message) {
		super(message);
	}

	public XformSubmissionNotFoundException(Throwable cause) {
		super(cause);
	}

	public XformSubmissionNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public XformSubmissionNotFoundException(String message, Throwable cause,
										   boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
