package com.nexusug.xlsform.exception

/**
 * Thrown for errors resulting from poor configuration of the App
 * Created by ivan on 02/05/2017.
 */
class MisConfigurationException extends Exception{
	public MisConfigurationException() {
		super()
	}

	public MisConfigurationException(String message) {
		super(message);
	}

	public MisConfigurationException(Throwable cause) {
		super(cause);
	}

	public MisConfigurationException(String message, Throwable cause) {
		super(message, cause);
	}

	public MisConfigurationException(String message, Throwable cause,
									 boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
