package com.nexusug.xlsform.exception

/**
 * Thrown when a specified binding could not be found.
 *
 * Created by ivan on 30/11/2015.
 */
class XformBindingNotFoundException extends Exception {
	public XformBindingNotFoundException() {
		super()
	}

	public XformBindingNotFoundException(String message) {
		super(message);
	}

	public XformBindingNotFoundException(Throwable cause) {
		super(cause);
	}

	public XformBindingNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public XformBindingNotFoundException(String message, Throwable cause,
										   boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
