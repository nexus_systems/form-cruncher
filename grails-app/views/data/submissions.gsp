<g:applyLayout name="one-column-top-nav">
	<head>
		<title>forms</title>
		<asset:javascript src="data-manifest"/>
		<asset:stylesheet src="data-manifest"/>
	</head>
	<content tag="main">
		<ol class="breadcrumb">
			<li><g:link controller="data">Data</g:link></li>
			<li><g:link controller="form" action="view" id="${xform.id}">${xform.xlsId}</g:link></li>
			<li class="active">submissions</li>
		</ol>


		<g:render template="submissions_table" model="${[submissions: submissions]}"/>
	</content>
</g:applyLayout>