<table data-toggle="table" data-striped="true">
	<thead>
	<tr>
		<th>form id</th>
		<th>form title</th>
		<th>upload date</th>
		<th></th>
	</tr>
	</thead>
	<tbody>
	<g:each in="${submissions}" var="submission">
		<tr>
			<td>${submission.xform.xlsId}</td>
			<td>${submission.xform.title}</td>
			<td><g:formatDate date="${submission.dateCreated}"/></td>
			<td>
				<g:link class="btn btn-default btn-sm btn-danger" controller="data"
						action="delete" id="${submission.id}" title="delete submission"
						data-toggle="confirm-delete-submission">
					<i class="fa fa-trash"></i>
				</g:link>
				<g:link class="btn btn-default btn-sm" controller="data" action="edit" id="${submission.id}"
						title="edit submission">
					<i class="fa fa-edit"></i>
				</g:link>
			</td>
		</tr>
	</g:each>
	</tbody>
</table>