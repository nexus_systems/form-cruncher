<g:applyLayout name="one-column-top-nav">
	<head>
		<title>forms</title>
	</head>
	<content tag="top-nav">
		<asset:javascript src="data-manifest"/>
		<asset:javascript src="data/submissions-management"/>
		<asset:stylesheet src="data-manifest"/>
	</content>
	<content tag="main">
		<div class="panel panel-default">
			<div class="panel-body">
				<g:form controller="data">
					<div class="form-group">
						<select class="form-control" name="id" id="form-xlsId">
							<g:each in="${formList}" var="form">
								<option value="${form.xlsId}">${form.title}</option>
							</g:each>
						</select>
					</div>

					<div class="form-group">
						<g:actionSubmit class="btn btn-default btn-sm" value="view submissions" action="viewFormSubmissions" />
						<g:actionSubmit class="btn btn-default btn-sm" value="capture data" action="captureFormData" />
					</div>
				</g:form>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading text-capitalize">recently captured data</div>

			<div class="panel-body">
				<g:set var="submissions" value="${mostRecentSubmissions}"/>
				<g:render template="submissions_table" model="${submissions}"/>
			</div>
		</div>
	</content>
</g:applyLayout>