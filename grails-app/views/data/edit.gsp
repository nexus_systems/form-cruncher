<g:applyLayout name="one-column-top-nav">
	<head>
		<title>Data edit</title>
		<g:if test="${xformDisplayModel}">
			<asset:javascript src="xls-form/form-visualizer"></asset:javascript>
			<asset:javascript src="xls-form/form-data-capture-helper"></asset:javascript>
			<asset:javascript src="data/data-edit"></asset:javascript>
			<g:render template="/handlebars/xlsform-elements"/>
		</g:if>
	</head>
	<content tag="main">
		<ol class="breadcrumb">
			<li><g:link controller="data">Data</g:link></li>
			<li><g:link controller="data" action="submissions" id="${xformDisplayModel.xlsId}">${xformDisplayModel.xlsId}</g:link></li>
			<li class="active ">edit submission</li>
		</ol>
		<input type="hidden" id="edit-form-xls-id" value="${xformDisplayModel.xlsId}"/>
		<g:form action="update" name="data-capture-form">
			<input type="hidden" name="submissionId" value="${submission.id}">
			<g:render template="/formVisualizer/form_visualization"/>
		</g:form>
	</content>
</g:applyLayout>