<g:applyLayout name="one-column-top-nav">
	<head>
		<title>forms</title>
		<asset:javascript src="xls-form/form-visualizer"></asset:javascript>
		<asset:javascript src="xls-form/form-data-capture-helper"></asset:javascript>
		<asset:javascript src="data/data-capture"></asset:javascript>
		<g:render template="/handlebars/xlsform-elements"/>
	</head>
	<content tag="main">
		<ol class="breadcrumb">
			<li><g:link controller="data">Data</g:link></li>
			<li><g:link controller="form" action="view"
						id="${xformDisplayModel.xformId}">${xformDisplayModel.xlsId}</g:link></li>
			<li class="active">capture data</li>
		</ol>
		<g:form action="save" name="data-capture-form">
			<g:render template="/formVisualizer/form_visualization"
					  model="${[
							  xformDisplayModel    : xformDisplayModel,
							  xformDisplayModelJson: xformDisplayModelJson,
							  xformModelJson       : xformModelJson]}"/>
		</g:form>
	</content>
</g:applyLayout>