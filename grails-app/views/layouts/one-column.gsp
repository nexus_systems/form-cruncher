<html>
<head>
	<meta name="layout" content="main" />
	<title><g:layoutTitle /></title>
	<g:layoutHead />
</head>
<body>
	<div class="container-fluid">
		<div class="col-md-12">
			<g:pageProperty name="page.main" />
		</div>
	</div>
</body>
</html>