<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title><g:layoutTitle default="swali"/></title>

		<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
		<link rel="icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">

		<asset:stylesheet src="application.css"/>
		<asset:javascript src="application.js"/>
		<g:render template="/handlebars/notifications" />
		<g:layoutHead/>
	</head>
	<body>
	<div class="container-fluid">
			<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed"
							data-toggle="collapse" data-target="#shopper-navbar-top">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
						<span class="navbar-brand logo text-uppercase">swali</span>
					</div>

					<div class="collapse navbar-collapse" id="shopper-navbar-top">
						<ul class="nav navbar-nav">
							<li class="text-capitalize ${controllerName == 'form' ? 'active' : ''}"><g:link controller="form">Forms</g:link></li>
							<li class="text-capitalize ${controllerName == 'data' ? 'active' : ''}"><g:link controller="data">Data</g:link></li>
							<li class="text-capitalize ${controllerName == 'report' ? 'active' : ''}"><g:link controller="report">Reports</g:link></li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown">Ivan Orone<span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">account</a></li>
									<li class="divider"></li>
									<li><a href="#">logout</a></li>
								</ul></li>
						</ul>
					</div>
				</div>
			</nav>
	</div>
	<g:layoutBody/>
	</body>
</html>
