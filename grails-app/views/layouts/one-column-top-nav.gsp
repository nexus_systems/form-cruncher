<html>
<head>
	<meta name="layout" content="main"/>
	<title><g:layoutTitle/></title>
	<g:layoutHead/>
</head>

<body>
<div class="container-fluid">
	<div class="col-sm-12 context-nav">
		<g:pageProperty name="page.top-nav"/>
	</div>

	<div class="col-sm-12">
		<g:render template="/flash-messages"></g:render>
		<g:pageProperty name="page.main"/>
	</div>
</div>
</body>
</html>