<script type="text/x-handlebars-template" data-name="xform-row-detail-view" data-partial="true">
	<div class="col-sm-12">
		<a href="${g.createLink(controller: 'form', action: 'deleteXform', id: '{{id}}')}" class="btn btn-danger btn-sm"
		   data-toggle='confirm-delete-xform-{{id}}'><i class="fa fa-trash"></i></a>

		<a href="${g.createLink(controller: 'form', action: 'view', id: '{{id}}')}"
		   class="btn btn-primary btn-sm" title="edit xform"><i class="fa fa-edit"></i></a>

		<a href="${g.createLink(controller: 'form', action: 'downloadXform', id: '{{id}}')}"
		   class="btn btn-default btn-sm" title="download xform"><i class="fa fa-download"></i></a>
	</div>
</script>
<script type="text/x-handlebars-template" data-name="xls-form-row-detail-view" data-partial="true">
	<div class="col-sm-12">

		{{#switch uploadStatus}}
			{{#case "SUCCESSFUL"}}
				{{#if xformTitle}}
					The xform <b>{{xformTitle}}</b> was created
				{{else}}
					<div class="alert alert-warning">Creation of the xform failed</div>
				{{/if}}
			{{/case}}
			{{#case "FAILED"}}
				<div class="alert alert-danger">
					{{#if conversionReport}}
						{{conversionReport}}
					{{else}}
						An unknown error occurred.
					{{/if}}
				</div>
			{{/case}}
		{{/switch}}
	</div>

	<div class="col-sm-12">
		<a href="${g.createLink(controller: 'form', action: 'deleteXlsFormUpload', id: '{{id}}')}"
		   class="btn btn-danger btn-sm" title="Delete upload"
		   data-toggle='confirm-delete-xls-upload-{{id}}'><i class="fa fa-trash"></i></a>

		<a href="${g.createLink(controller: 'form', action: 'downloadXlsForm', id: '{{id}}')}"
		   class="btn btn-default btn-sm" title="download original upload"><i class="fa fa-download"></i></a>
	</div>
</script>
