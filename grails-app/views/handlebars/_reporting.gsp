%{-- determines whether or not to set an option as the selected one on init of the select --}%
<script type="text/x-handlebars-template" data-name="is-default-option-partial" data-partial="true">
	{{#if_equals option defaultOption }}
	selected
	{{/if_equals}}
</script>

%{-- partial for condition linkers --}%
<script id="report-condition-linker" type="text/x-handlebars-template" data-name="report-condition-linker"
		data-namespace="report">
	<div class="form-group report-condition-linker">
		<div class="col-sm-1">
			<select class="form-control selectpicker show-tick" id="conditionLinkers[{{index}}]"
					name="conditionLinkers[{{index}}]">
				<g:each in="${com.nexusug.xlsform.reporting.ConditionLinker.values()}" var="conditionLinker">
					<option value="${conditionLinker}"
							{{>is-default-option-partial option='${conditionLinker}' defaultOption=value}}>
					${conditionLinker.name()}
					</option>
				</g:each>
			</select>
		</div>

		<div class="clearfix"></div>
	</div>
</script>

%{-- partial containing detailed view of a report when viewed in the view reports section --}%
<script type="text/x-handlebars-template" data-name="report-detailed-view-partial" data-partial="true">
	<div class="col-sm-12">
		<p>{{description}}</p>
	</div>

	<div class="col-sm-12">
		<a href="delete/{{id}}" class="btn btn-danger btn-sm" title="Delete report"
		   data-toggle='confirm-delete-report-{{id}}'><i class="fa fa-trash"></i></a>
		<a href="edit/{{id}}" class="btn btn-primary btn-sm" title="edit"><i class="fa fa-edit"></i></a>
		<a href="view/{{id}}" class="btn btn-default btn-sm">view data</a>
	</div>
</script>

%{-- template for a single condition in a report --}%
<script id="report-condition" type="text/x-handlebars-template" data-name="report-condition" data-namespace="report">

	<div class="panel panel-default report-condition">
		<div class="panel-body">
			<div class="report-element-validation-errors"></div>
			<span class="report-element">
				<a href="#" id="conditions_{{index}}_formXlsId_label" data-type="select" data-emptytext="Select a form"
				   data-showbuttons="false"></a>
				<input type="hidden" id="conditions[{{index}}].formXlsId" name="conditions[{{index}}].formXlsId"
					   data-condition-index="{{index}}" value="{{formXlsId}}" data-msg-required="Please select a form"
					   required>
			</span>
			<span class="report-element">
				<a href="#" id="conditions_{{index}}_questionKey_label" data-type="select"
				   data-emptytext="Select a question" data-showbuttons="false"></a>
				<input type="hidden" id="conditions[{{index}}].questionKey" name="conditions[{{index}}].questionKey"
					   data-condition-index="{{index}}" value="{{questionKey}}"
					   data-msg-required="Please select a question" required>
			</span>
			<span class="report-element">
				<a href="#" id="conditions_{{index}}_operator_label" data-type="select"
				   data-emptytext="select a condition" data-showbuttons="false"></a>
				<input type="hidden" id="conditions[{{index}}].operator" name="conditions[{{index}}].operator"
					   data-condition-index="{{index}}" value="{{operator}}"
					   data-msg-required="Please select a condition" required>
			</span>
			<span class="report-element">
				<a href="#" id="conditions_{{index}}_operand_label" data-emptytext="value to compare against"></a>
				<input type="hidden" id="conditions[{{index}}].operand" name="conditions[{{index}}].operand"
					   data-condition-index="{{index}}" value="{{operand}}"
					   data-msg-required="Please provide a value to compare against" required>
			</span>
		</div>

		<div class="panel-footer">
			<button class="btn-xs btn-primary add-condition-trigger" type="button" title="add new condition"><i
					class="fa fa-plus-circle"></i></button>
			<button class="btn-xs btn-danger remove-condition" type="button" title="delete this condition"><i
					class="fa fa-minus-circle"></i></button>
		</div>
	</div>
</script>

%{-- template for container through which a new display column can be added to the report --}%
<script id="report-column-creator" type="text/x-handlebars-template" data-name="report-column-creator"
		data-namespace="report">
	<div class="panel panel-default">
		<div class="panel-heading">
			Add columns
		</div>

		<div class="panel-body">
			<div class="form">
				<div class="form-group">
					<label for="col-form">Form</label>

					<div class="clearfix"></div>
					<a href="#" id="display-col-forms" data-emptytext="Select a form" data-type="select"
					   data-showbuttons="false"></a>
					<input type="hidden" id="col-form" name="col-form" readonly>
				</div>

				<div class="divider10"></div>

				<div class="form-group">
					<label for="col-question">Question</label>

					<div class="clearfix"></div>
					<a href="#" id="display-col-questions" data-emptytext="Select a question" data-type="select"
					   data-showbuttons="false"></a>
					<input type="hidden" id="col-question" name="col-question" readonly>
				</div>

				<div class="divider10"></div>

				<div class="form-group">
					<label for="col-displayName">Display name</label>

					<div class="clearfix"></div>
					<a href="#" id="display-col-display-name" data-emptytext="Provide the column display name"></a>
					<input type="hidden" id="col-displayName" name="col-displayName" readonly>
				</div>

				<div class="divider10"></div>
				<button type="button" class="btn btn-default btn-sm" id="add-col-button">Add column</button>
			</div>
		</div>
	</div>
</script>

%{-- partial for a display column shown on the list of available report display columns --}%
<script type="text/x-handlebars-template" data-name="report-display-col-partial" data-partial="true">
	<div class="col-sm-12 report-display-col">
		<a href="#"><i class="pull-right fa fa-close remove-col-indicator"></i></a>
		<span class="h5">{{displayName}} <br/> <i class="small">{{question}}</i></span>
		<input type="hidden" name="displayColumns[{{index}}].question" value="{{question}}">
		<input type="hidden" name="displayColumns[{{index}}].displayName" value="{{displayName}}">
	</div>
</script>