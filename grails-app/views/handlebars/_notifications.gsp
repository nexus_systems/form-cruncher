<script type="text/x-handlebars-template" data-name="notification-partial" data-partial="true">
	{{#if error}}
		<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
					aria-hidden="true">&times;</span></button>
			{{error}}
		</div>
	{{/if}}

	{{#if errors}}
		{{#each errors}}
			<div class="alert alert-danger alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				{{this}}
			</div>
		{{/each}}
	{{/if}}



	{{#if warn}}
		<div class="alert alert-warning alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
					aria-hidden="true">&times;</span></button>
			{{warn}}
		</div>
	{{/if}}

	{{#if message}}
		<div class="alert alert-info alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
					aria-hidden="true">&times;</span></button>
			{{message}}
		</div>
	{{/if}}
</script>