<script id="string" type="text/x-handlebars-template" data-name="string" data-namespace="elements">
   {{>string-partial}}
</script>

<script id="select1" type="text/x-handlebars-template"  data-name="select1" data-namespace="elements">
    {{>select1-partial}}
</script>

<script id="select" type="text/x-handlebars-template"  data-name="select" data-namespace="elements">
    {{>select-partial}}
</script>

<script id="decimal" type="text/x-handlebars-template"  data-name="decimal" data-namespace="elements">
    {{>decimal-partial}}
</script>

<script id="integer" type="text/x-handlebars-template"  data-name="integer" data-namespace="elements">
    {{>integer-partial}}
</script>

<script id="dateTime" type="text/x-handlebars-template"  data-name="dateTime" data-namespace="elements">
    {{>dateTime-partial}}
</script>

<script id="time" type="text/x-handlebars-template"  data-name="time" data-namespace="elements">
    {{>time-partial}}
</script>

<script id="date" type="text/x-handlebars-template"  data-name="date" data-namespace="elements">
    {{>date-partial}}
</script>


<script id="group" type="text/x-handlebars-template"  data-name="group" data-namespace="elements">
    <div class="panel panel-default">
        <div class="panel-heading">{{label}}</div>
        <div class="panel-body">
            {{>group-partial}}
        </div>
    </div>
</script>

<script id="repeat" type="text/x-handlebars-template"  data-name="repeat" data-namespace="elements">
    <div class="panel panel-default">
        <div class="panel-heading">{{label}}</div>
        <div class="panel-body repeat-container">
            {{>repeat-partial}}
        </div>
    </div>
</script>

%{-- PARTIALS --}%
<script type="text/x-handlebars-template"  data-name="repeat-partial" data-partial="true">
    <div class="panel panel-default repeat-instance repeat-instance-{{name}}">
        <div class="panel-body">
            {{>group-partial isRepeat=true parent=this inRepeatIndex=(getNextDistinctIdForItem this.name)}}
        </div>
        <div class="panel-footer">
            <input class="btn btn-primary btn-sm add-repeat-button" type="button" value="New entry" data-name="{{name}}" name="{{name}}">
            <input class="btn btn-default btn-sm remove-repeat-button" type="button" value="Delete" name="{{name}}">
        </div>
    </div>
</script>

<script type="text/x-handlebars-template"  data-name="group-partial" data-partial="true">
    {{#if isRepeat}}
        {{#each displayables}}
            {{>(determine_xls_form_element_partial type) isRepeat=true parent=.. inRepeatIndex=../inRepeatIndex}}
        {{/each}}
    {{else}}
        {{#each displayables}}
            {{>(determine_xls_form_element_partial type)}}
        {{/each}}
    {{/if}}
</script>

<script type="text/x-handlebars-template"  data-name="select1-partial" data-partial="true">
    <div>
        <label>{{>input-required-indicator-partial}}{{label}}</label>
        {{#if hint}}
        <p class="small"><i>{{hint}}</i></p>
        {{/if}}
    </div>
    {{#if isRepeat}}
        {{#if_equals appearance 'minimal' }}
            <div class="form-group" >
                <select name="{{parent.name}}[{{inRepeatIndex}}][{{name}}]"  data-name="{{name}}-{{inRepeatIndex}}" class="{{name}}-{{inRepeatIndex}} form-control"
                        value="{{value}}" {{>input-validation-attributes-partial}}>
                    <option value="">select</option>
                        {{>select1-items-partial isRepeat=true inRepeatIndex=this.inRepeatIndex parent=this.parent}}
                </select>
            </div>
        {{else}}
            <div class="form-group" data-name="{{name}}-{{inRepeatIndex}}">
                {{>select1-items-partial isRepeat=true inRepeatIndex=this.inRepeatIndex parent=this.parent}}
            </div>
        {{/if_equals}}
    {{else}}
        {{#if_equals appearance 'minimal' }}
            <div class="form-group">
                <select class="form-control {{name}}" name="{{name}}" data-name="{{name}}" value="{{value}}" {{>input-validation-attributes-partial}}>
                    <option value=""></option>
                    {{>select1-items-partial}}
                </select>
            </div>
        {{else}}
            <div class="form-group" data-name="{{name}}">
                {{>select1-items-partial}}
            </div>
        {{/if_equals}}
    {{/if}}
</script>

<script type="text/x-handlebars-template"  data-name="select1-items-partial" data-partial="true">
    {{#if isRepeat}}
        {{#if_equals appearance 'minimal' }}
                {{#each items}}
                    <option value="{{value}}" {{>add-selected-if-valid optionValue=value itemValue=../value}}>{{label}}</option>
                {{/each}}
        {{else}}
            {{#each items}}
                <div class="radio">
                    <label>
                        <input type="radio" name="{{../parent.name}}[{{../inRepeatIndex}}][{{../name}}]"
                               class="{{../name}}-{{../inRepeatIndex}}"
                               value="{{value}}" {{>input-validation-attributes-partial ..}}
                               {{>add-checked-if-valid optionValue=value itemValue=../value}}>
                        {{label}}
                    </label>
                </div>
            {{/each}}
        {{/if_equals}}
        {{#if required}}
            <label id="{{parent.name}}[{{inRepeatIndex}}][{{name}}]-error" class="error" for="{{parent.name}}[{{inRepeatIndex}}][{{name}}]"></label>
        {{/if}}
    {{else}}
        {{#if_equals appearance 'minimal' }}
            {{#each items}}
                <option value="{{value}}" {{>add-selected-if-valid optionValue=value itemValue=../value}}>{{label}}</option>
            {{/each}}
        {{else}}
            {{#each items}}
                <div class="radio">
                    <label>
                        <input type="radio" name="{{../name}}" class="{{../name}}"
                               value="{{value}}" {{>add-checked-if-valid optionValue=value itemValue=../value}}
                               {{>input-validation-attributes-partial ..}}>{{label}}
                    </label>
                </div>
            {{/each}}
        {{/if_equals}}
        {{#if required}}
            <label id="{{name}}-error" class="error" for="{{name}}"></label>
        {{/if}}
    {{/if}}
</script>

%{-- partial to aid in adding the checked flag to a checkbox or radio button if it's value matches the default value --}%
<script type="text/x-handlebars-template"  data-name="add-checked-if-valid" data-partial="true">
    {{#if_in optionValue itemValue}}
        checked="checked"
    {{/if_in}}
</script>

%{-- partial to aid in deciding whether or not an option in a select should marked as selected --}%
<script type="text/x-handlebars-template"  data-name="add-selected-if-valid" data-partial="true">
    {{#if_equals optionValue itemValue}}
        selected="selected"
    {{/if_equals}}
</script>

<script type="text/x-handlebars-template"  data-name="select-partial" data-partial="true">
    <div class="form-group">
        <label for="{{name}}">{{>input-required-indicator-partial}}{{label}}</label>
        {{#if hint}}
            <p class="small"><i>{{hint}}</i></p>
        {{/if}}
        {{#if isRepeat}}
            {{#each items}}
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="{{../parent.name}}[{{../inRepeatIndex}}][{{../name}}][]"
                               class="{{../name}}-{{../inRepeatIndex}}"
                               value="{{value}}"
                               {{>input-validation-attributes-partial ..}}
                               {{>add-checked-if-valid optionValue=value itemValue=../value}}>
                        {{label}}
                        </label>
                    </div>
                {{/each}}
            {{#if required}}
                <label id="{{parent.name}}[{{inRepeatIndex}}][{{name}}]-error" class="error" for="{{parent.name}}[{{inRepeatIndex}}][{{name}}]"></label>
            {{/if}}
        {{else}}
            {{#if_equals appearance 'minimal' }}
                <select multiple class="form-control {{name}}" name="{{name}}[]" value="{{value}}" {{>input-validation-attributes-partial}}>
                    {{#each items}}
                        <option value="{{value}}">{{label}}</option>
                    {{/each}}
                </select>
            {{else}}
                {{#each items}}
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="{{../name}}[]" value="{{value}}" class="{{../name}}"
                                   {{>input-validation-attributes-partial ..}}> {{label}}
                                   {{>add-checked-if-valid optionValue=value itemValue=../value}}>
                        </label>
                    </div>
                {{/each}}
            {{/if_equals}}
            {{#if required}}
                <label id="{{name}}-error" class="error" for="{{name}}"></label>
            {{/if}}
        {{/if}}
    </div>
</script>

%{-- this partial inserts the required and requireMsg properties to the target if conditions are valid --}%
<script type="text/x-handlebars-template"  data-name="input-validation-attributes-partial" data-partial="true">
    {{#if required}} required {{/if}} {{#if requiredMsg}} data-msg="{{requiredMsg}}" {{/if}}
</script>

%{--inserts an asterisk if input is required--}%
<script type="text/x-handlebars-template"  data-name="input-required-indicator-partial" data-partial="true">
    {{#if required}}<span class="required">*</span>{{/if}}
</script>

<script type="text/x-handlebars-template"  data-name="string-partial" data-partial="true">
    <label for="{{name}}">{{>input-required-indicator-partial}}{{label}}</label>
    {{#if value}}
        <p class="small"><i>{{hint}}</i></p>
    {{/if}}
    <div class="form-group {{#if hidden}}hidden{{/if}}">
        {{#if isRepeat}}
            {{#if_equals appearance 'multiline' }}
                <textarea class="form-control" id="{{name}}-{{inRepeatIndex}}" name="{{parent.name}}[{{inRepeatIndex}}][{{name}}]"
                          placeholder="{{hint}}" {{>input-validation-attributes-partial}}>{{value}}</textarea>
            {{else}}
                <input type="text" class="form-control" {{#if readOnly}} readonly {{/if}}
                       id="{{name}}-{{inRepeatIndex}}" name="{{parent.name}}[{{inRepeatIndex}}][{{name}}]"
                       placeholder="{{hint}}" value="{{value}}" {{>input-validation-attributes-partial}}>
            {{/if_equals}}
        {{else}}
            {{#if_equals appearance 'multiline' }}
                <textarea class="form-control"
                          id="{{name}}" name="{{name}}" placeholder="{{hint}}"
                          {{>input-validation-attributes-partial}}>{{value}}</textarea>
            {{else}}
                <input type="text" class="form-control"
                       {{#if readOnly}} readonly {{/if}}
                       id="{{name}}" name="{{name}}" placeholder="{{hint}}"
                       value="{{value}}" {{>input-validation-attributes-partial}}>
            {{/if_equals}}
        {{/if}}
    </div>
</script>


<script type="text/x-handlebars-template"  data-name="decimal-partial" data-partial="true">
    <div class="form-group {{#if hidden}}hidden{{/if}}">
        {{#if isRepeat}}
            <label for="{{name}}-{{inRepeatIndex}}">{{>input-required-indicator-partial}}{{label}}</label>
            {{#if value}}
                <p class="small"><i>{{hint}}</i></p>
            {{/if}}
            <div class="row">
                <div class='col-sm-6'>
                    <input type="number" class="form-control"
                           {{#if readOnly}} readonly {{/if}}
                           id="{{name}}-{{inRepeatIndex}}" name="{{parent.name}}[{{inRepeatIndex}}][{{name}}]"
                           placeholder="{{hint}}" value="{{value}}" step="0.01" {{>input-validation-attributes-partial}}>
                </div>
            </div>
        {{else}}
            <label for="{{name}}">{{>input-required-indicator-partial}}{{label}}</label>
            {{#if value}}
                <p class="small"><i>{{hint}}</i></p>
            {{/if}}
            <div class="row">
                <div class='col-sm-6'>
                    <input type="number" class="form-control"
                           {{#if readOnly}} readonly {{/if}}
                           id="{{name}}" name="{{name}}" placeholder="{{hint}}"
                           value="{{value}}" step="0.01" {{>input-validation-attributes-partial}}>
                </div>
            </div>
        {{/if}}
    </div>
</script>

<script type="text/x-handlebars-template"  data-name="integer-partial" data-partial="true">
    <div class="form-group {{#if hidden}}hidden{{/if}}">
        {{#if isRepeat}}
            <label for="{{name}}-{{inRepeatIndex}}">{{>input-required-indicator-partial}}{{label}}</label>
            {{#if value}}
                <p class="small"><i>{{hint}}</i></p>
            {{/if}}
            <div class="row">
                <div class='col-sm-6'>
                    <input type="number" class="form-control"
                           {{#if readOnly}} readonly {{/if}}
                           id="{{name}}-{{inRepeatIndex}}" name="{{parent.name}}[{{inRepeatIndex}}][{{name}}]"
                           placeholder="{{hint}}" value="{{value}}" step="1" {{>input-validation-attributes-partial}}>
                </div>
            </div>
        {{else}}
            <label for="{{name}}">{{>input-required-indicator-partial}}{{label}}</label>
            {{#if value}}
                <p class="small"><i>{{hint}}</i></p>
            {{/if}}
            <div class="row">
                <div class='col-sm-6'>
                    <input type="number" class="form-control"
                           {{#if readOnly}} readonly {{/if}}
                           id="{{name}}" name="{{name}}" placeholder="{{hint}}"
                           value="{{value}}" step="1" {{>input-validation-attributes-partial}}>
                </div>
            </div>
        {{/if}}
    </div>
</script>

<script type="text/x-handlebars-template"  data-name="dateTime-partial" data-partial="true">
    <div class="form-group {{#if hidden}}hidden{{/if}}">
        {{#if isRepeat}}
            <label for="{{name}}-{{inRepeatIndex}}">{{>input-required-indicator-partial}}{{label}}</label>
            {{#if value}}
                <p class="small"><i>{{hint}}</i></p>
            {{/if}}
            <div class="row">
                <div class='col-sm-6'>
                    <input type="text" class="form-control date-time-picker"
                           {{#if readOnly}} readonly {{/if}}
                           id="{{name}}-{{inRepeatIndex}}" name="{{parent.name}}[{{inRepeatIndex}}][{{name}}]"
                           placeholder="{{hint}}" value="{{value}}" {{>input-validation-attributes-partial}}>
                </div>
            </div>
        {{else}}
            <label for="{{name}}">{{>input-required-indicator-partial}}{{label}}</label>
            {{#if value}}
                <p class="small"><i>{{hint}}</i></p>
            {{/if}}
            <div class="row">
                <div class='col-sm-6'>
                    <input type="text" class="form-control date-time-picker"
                           {{#if readOnly}} readonly {{/if}}
                           id="{{name}}" name="{{name}}" placeholder="{{hint}}"
                           value="{{value}}" {{>input-validation-attributes-partial}}>
                </div>
            </div>
        {{/if}}
    </div>
</script>

<script type="text/x-handlebars-template"  data-name="time-partial" data-partial="true">
    <div class="form-group {{#if hidden}}hidden{{/if}}">
        {{#if isRepeat}}
            <label for="{{name}}-{{inRepeatIndex}}">{{>input-required-indicator-partial}}{{label}}</label>
            {{#if value}}
                <p class="small"><i>{{hint}}</i></p>
            {{/if}}
            <div class="row">
                <div class='col-sm-6'>
                    <input type="text" class="form-control time-picker"
                           {{#if readOnly}} readonly {{/if}}
                           id="{{name}}-{{inRepeatIndex}}" name="{{parent.name}}[{{inRepeatIndex}}][{{name}}]"
                           placeholder="{{hint}}" value="{{value}}" {{>input-validation-attributes-partial}}>
                </div>
            </div>
        {{else}}
            <label for="{{name}}">{{>input-required-indicator-partial}}{{label}}</label>
            {{#if value}}
                <p class="small"><i>{{hint}}</i></p>
            {{/if}}
            <div class="row">
                <div class='col-sm-6'>
                    <input type="text" class="form-control time-picker" {{#if readOnly}} readonly {{/if}}
							id="{{name}}" name="{{name}}" placeholder="{{hint}}"
                           	value="{{value}}" {{>input-validation-attributes-partial}}>
                </div>
            </div>
        {{/if}}
    </div>
</script>

<script type="text/x-handlebars-template"  data-name="date-partial" data-partial="true">
    <div class="form-group {{#if hidden}}hidden{{/if}}">
        {{#if isRepeat}}
            <label for="{{name}}-{{inRepeatIndex}}">{{>input-required-indicator-partial}}{{label}}</label>
            {{#if value}}
                <p class="small"><i>{{hint}}</i></p>
            {{/if}}
            <div class="row">
                <div class='col-sm-6'>
                    <input type="text" class="form-control date-picker" {{#if readOnly}} readonly {{/if}}
							id="{{name}}-{{inRepeatIndex}}"
							name="{{parent.name}}[{{inRepeatIndex}}][{{name}}]"
                           	placeholder="{{hint}}" value="{{value}}" {{>input-validation-attributes-partial}}>
                </div>
            </div>
        {{else}}
            <label for="{{name}}">{{>input-required-indicator-partial}}{{label}}</label>
            {{#if value}}
                <p class="small"><i>{{hint}}</i></p>
            {{/if}}
            <div class="row">
                <div class='col-sm-6'>
                    <input type="text" class="form-control date-picker"
                           {{#if readOnly}} readonly {{/if}} id="{{name}}" name="{{name}}" placeholder="{{hint}}"
                           value="{{value}}" {{>input-validation-attributes-partial}}>
                </div>
            </div>
        {{/if}}
    </div>
</script>

