<ul class="nav nav-tabs nav-justified report-context-nav">
	<li role="presentation" class="${actionName in ['index', 'view', 'edit'] ? 'active' : ''}">
		<g:link controller="report" action="index">View reports</g:link>
	</li>
	<li role="presentation" class="${params.action == 'create' ? 'active' : ''}">
		<g:link controller="report" action="create">Create report</g:link>
	</li>
</ul>