<g:applyLayout name="one-column-top-nav">
	<head>
		<title>reports</title>
		<asset:javascript src="reports-manifest.js"/>
		<asset:stylesheet src="reports-manifest.css"/>
		<asset:javascript src="reports/report-builder.js"/>
		<g:render template="/handlebars/reporting" />
	</head>
	<content tag="top-nav">
		<g:render template="side-nav"/>
	</content>
	<content tag="main">
		<g:form action="saveOrUpdate">
			<g:render template="report_form_content"/>
			<button type="submit" class="btn btn-default btn-sm" id="submit" value="create">Submit</button>
		</g:form>
	</content>
</g:applyLayout>