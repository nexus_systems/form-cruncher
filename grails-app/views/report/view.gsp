<g:applyLayout name="one-column-top-nav">
	<head>
		<title>forms</title>
		<asset:javascript src="reports-manifest.js"/>
		<asset:stylesheet src="reports-manifest.css"/>
		<g:render template="/handlebars/reporting"/>
		<asset:javascript src="/reports/report-submissions-fetcher.js"/>
	</head>
	<content tag="top-nav">
		<g:render template="side-nav"/>
	</content>
	<content tag="main">
		<ol class="breadcrumb">
			<li><g:link controller="report">Report</g:link></li>
			<li class="active ">${reportInstance.name}</li>
		</ol>
		<table id="submissions-table" data-report-id="${reportInstance.id}">
			<thead>
			<tr>
				<g:each in="${reportInstance.displayColumns}" var="column">
					<th data-field="${column.question}" data-sortable="true">${column.displayName}</th>
				</g:each>
			</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</content>
</g:applyLayout>