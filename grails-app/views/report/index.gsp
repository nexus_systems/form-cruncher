<g:applyLayout name="one-column-top-nav">
	<head>
		<title>forms</title>
		<asset:javascript src="reports-manifest.js"/>
		<asset:stylesheet src="reports-manifest.css"/>
		<g:render template="/handlebars/reporting"/>
		<asset:javascript src="/reports/report-viewer.js"/>
	</head>
	<content tag="top-nav">
		<g:render template="side-nav"/>
	</content>
	<content tag="main">
		<table id="reports-list"
			   data-toggle="table"
			   data-url="${g.createLink(controller: 'report', action: 'list')}.json"
			   data-striped="true"
			   data-detail-view="true"
			   data-detail-formatter="reportViewer.reportDetailsFormatter">
			<thead>
			<tr>
				<th data-field="name">Created reports</th>
			</tr>
			</thead>
		</table>
	</content>
</g:applyLayout>