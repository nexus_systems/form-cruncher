<g:applyLayout name="one-column-top-nav">
	<head>
		<title>reports</title>
		<asset:javascript src="reports-manifest.js"/>
		<asset:stylesheet src="reports-manifest.css"/>
		<asset:javascript src="reports/report-builder.js"/>
		<g:render template="/handlebars/reporting"/>
	</head>
	<content tag="top-nav">
		<g:render template="side-nav"/>
	</content>
	<content tag="main">
		<ol class="breadcrumb">
			<li><g:link controller="report">Reports</g:link></li>
			<li><g:link controller="report" action="edit" id="${reportInstance?.id}">Edit</g:link></li>
			<li class="active ">${reportInstance?.name}</li>
		</ol>
		<g:form action="saveOrUpdate">
			<g:render template="report_form_content"/>
			<button type="submit" class="btn btn-default btn-sm" id="submit" value="update">Submit</button>
		</g:form>
	</content>
</g:applyLayout>