<input type="hidden" data-value="${formDisplayModelsJson}" id="report-creation-config" name="report-creation-config" readonly />
<input type="hidden" data-value="${allFormsXlsIdModelMap}" id="allFormsXlsIdModelMap" name="allFormsXlsIdModelMap" readonly />
<input type="hidden" data-value="${reportJsonModel}" id="saveReportDetails" name="saveReportDetails" readonly />
<input type="hidden" name="id" value="${reportInstance?.id}" />
<input type="hidden" id="conditionOperators" name="conditionOperators" data-value="${conditionOperators}" readonly />
<input type="hidden" id="conditionLinkers" name="conditionLinkers" data-value="${conditionLinkers}" readonly />
<div class="form-horizontal">
	<div class="form-group">
		<label for="name" class="col-sm-2 control-label">Name</label>

		<div class="col-sm-10">
			<input type="text" required class="form-control" id="name" name="name" value="${reportInstance?.name}" placeholder="name of the report">
		</div>
	</div>

	<div class="form-group">
		<label for="description" class="col-sm-2 control-label">Description</label>

		<div class="col-sm-10">
			<input type="text" class="form-control" id="description" name="description" value="${reportInstance?.description}"
				   placeholder="description og the report">
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">Conditions</div>
	<div class="panel-body conditions-holder" id="conditions-holder"></div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">Fields to display</div>
	<div class="panel-body">
		<div class="col-md-6" id="report-column-creator-holder"></div>
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					Columns to be displayed
				</div>
				<div class="panel-body" id="report-display-cols"></div>
			</div>
		</div>
	</div>
</div>