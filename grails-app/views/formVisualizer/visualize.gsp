<g:applyLayout name="one-column-top-nav">
	<head>
		<title>visualize</title>
		<asset:javascript src="xls-form/form-visualizer"></asset:javascript>
		<g:render template="/handlebars/xlsform-elements"/>
	</head>
	<content tag="top-nav">
		<ul class="nav nav-tabs nav-justified">
			<li role="presentation" class="active"><a href="#">View existing</a></li>
			<li role="presentation"><a href="#">Add new</a></li>
		</ul>
	</content>
	<content tag="main">
		<ol class="breadcrumb">
			<li><a href="#">Form</a></li>
			<li class="active ">visualize</li>
		</ol>
		<g:form>
			<g:render template="form_visualization"/>
		</g:form>
	</content>
</g:applyLayout>