<div class="elements-config-container">
	<div class="page-header">
		<h4 class="text-capitalize">${xformDisplayModel.title}</h4>
	</div>
	<input type="hidden" data-value="${xformDisplayModelJson}" name="xform" id="xform" disabled />
	<input type="hidden" value="${xformDisplayModel.xlsId}" name="xlsId" id="xlsId" />
	<input type="hidden" data-value="${xformModelJson}" name="xformModel" id="xformModel" disabled />
</div>
<div class="elements-container"></div>
<input type="submit" value="submit" class="btn btn-primary">