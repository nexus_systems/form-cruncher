<g:applyLayout name="one-column-top-nav">
	<head>
		<title>forms</title>
		<asset:javascript src="forms-manifest"/>
		<asset:stylesheet src="forms-manifest"/>
		<g:render template="/handlebars/forms"/>
	</head>
	<content tag="main">
		<ol class="breadcrumb">
			<li><g:link controller="form">Forms</g:link></li>
			<li class="active">${xform.name}</li>
		</ol>

		<div class="page-header">
			<h4 class="text-capitalize">${xform.title}</h4>
		</div>

		<div class="panel panel-default">
			<div class="panel-body">
				<p class="lead">Browse and import an XLSForm to update xform</p>
				<g:uploadForm name="xlsForm" action="update">
					<div class="form-group">
						<input type="file" class="file-loading" name="uploadFile" id="uploadFile"
							   data-allowed-file-extensions='["xls", "xlsx"]'/>
					</div>
					<input type="hidden" name="xformId" id="xformId" value="${xform.id}">
				</g:uploadForm>
			</div>
		</div>

		<div>
			<a href="${g.createLink(controller: 'data', action: 'submissions', id: xform.xlsId)}"
			   class="btn btn-default">view submissions</a>
			<a href="${g.createLink(controller: 'data', action: 'capture', id: xform.xlsId)}"
			   class="btn btn-default">capture data</a>
		</div>
	</content>
</g:applyLayout>