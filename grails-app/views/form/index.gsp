<g:applyLayout name="one-column-top-nav">
	<head>
		<title>forms</title>
		<asset:javascript src="forms-manifest"/>
		<asset:stylesheet src="forms-manifest"/>
		<g:render template="/handlebars/forms"/>
	</head>
	<content tag="main">
		<div class="panel panel-default">
			<div class="panel-body">
				<span class="lead">
					Browse and import an XLSForm</span>
				<a href="#" data-toggle="popover" data-trigger="hover|click" data-html="true"
				   data-content="<p><em>XLSForm is a form standard created to help simplify the authoring of forms
					   in Excel.</em></p>
					   <a class='btn btn-default btn-xs btn-block' target='_blank'
					   href='http://xlsform.org/'>creating XLSForms</a>

					   <a class='btn btn-default btn-xs btn-block' target='_blank'
					   href='http://bit.ly/2pWr2Od'>sample XLSForm</a>">
					<i class="fa fa-question-circle small"></i></a>
				<div class="divider15"></div>

				<g:uploadForm name="xlsForm" action="upload">
					<div class="form-group">
						<input type="file" class="file-loading" name="uploadFile" id="uploadFile"
							   data-allowed-file-extensions='["xls", "xlsx"]'/>
					</div>
				</g:uploadForm>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">
				Available xforms
			</div>
			<div class="panel-body">
				<table id="xform-list" data-striped="true" data-detail-view="true" data-data="${xformList}">
					<thead>
					<tr>
						<th data-field="title"><g:message code="form.title"/></th>
						<th data-field="name"><g:message code="form.name"/></th>
						<th data-field="xlsId" data-field="formId"><g:message code="form.xlsId"/></th>
						<th data-field="lastUpdated"><g:message code="form.last_update"/></th>
					</tr>
					</thead>
				</table>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading text-capitalize">recently imported XLSForms</div>

			<div class="panel-body">
				<table id="xls-form-list" data-striped="true" data-detail-view="true" data-data="${xlsFormList}">
					<thead>
					<tr>
						<th data-field="fileName">File name</th>
						<th data-field="dateCreated">Date of import</th>
						<th data-field="uploadStatus">Status</th>
					</tr>
					</thead>
				</table>
			</div>
		</div>
	</content>
</g:applyLayout>