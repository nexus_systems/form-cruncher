<div class="notifications-holder">
	<g:if test="${flash.error}">
		<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
					aria-hidden="true">&times;</span></button>
			${flash.error}
		</div>
	</g:if>
	<g:if test="${flash.errors}">
		<g:each in="${flash.errors}" var="error">
			<div class="alert alert-danger alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				${error}
			</div>
		</g:each>
	</g:if>

	<g:if test="${flash.warn}">
		<div class="alert alert-warning alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
					aria-hidden="true">&times;</span></button>
			${flash.warn}
		</div>
	</g:if>
	<g:if test="${flash.message}">
		<div class="alert alert-info alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
					aria-hidden="true">&times;</span></button>
			${flash.message}
		</div>
	</g:if>
</div>