package com.nexusug.xlsform.util
/**
 * Created by ivan on 02/05/2017.
 */
class ConfigUtil {
	static String XLS_FORM_UPLOAD_DIR = 'swali.xlsForm.upload.dir'
	static String PYXFORM_INSTALL_LOCATION = 'swali.pyxform.install.location'

	static String getProperty(String key, String defaultValue) {
		[System.properties[key], System.getenv(key.toUpperCase().replace('.', '_')), defaultValue].find { it != null }
	}
}
