package com.nexusug.xlsform.util

import com.nexusug.Download

import javax.servlet.http.HttpServletResponse

/**
 * Created by ivan on 05/05/2017.
 */
class DownloadUtil {

	static void download(Download download, HttpServletResponse response) {
		response.setContentType("APPLICATION/OCTET-STREAM")
		response.setHeader("Content-Disposition", "Attachment;Filename=\"${download.fileName}\"")
		response.setContentType(download.contentType)
		def outputStream = response.getOutputStream()
		def fileInputStream = null
		if (download.file) {
			fileInputStream = new FileInputStream(download.file)
			byte[] buffer = new byte[4096];
			int len;
			while ((len = fileInputStream.read(buffer)) > 0) {
				outputStream.write(buffer, 0, len);
			}
			outputStream.flush()
			outputStream.close()
			fileInputStream.close()
		} else {
			outputStream << download.content
		}
		outputStream.flush()
		outputStream.close()
		fileInputStream?.close()
	}
}
