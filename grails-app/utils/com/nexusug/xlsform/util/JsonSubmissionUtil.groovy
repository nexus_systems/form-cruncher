package com.nexusug.xlsform.util

import com.nexusug.xlsform.XformSubmission
import com.nexusug.xlsform.exception.InvalidSubmission

/**
 * Utility class for manipulating JSON payloads for {@link com.nexusug.xlsform.XformSubmission} instances.
 * Created by ivan on 26/03/2016.
 */
class JsonSubmissionUtil {

	/**
	 * Verifies if the given <tt>payload</tt> contains all the properties expected of a valid {@link XformSubmission}.
	 * @param payload a map representation of the json payload to be validated.
	 * @return true if the <tt>payload</tt> is valid
	 * @throws InvalidSubmission when it encounters the first missing property that makes the <tt>payload</tt> invalid.
	 */
	static boolean verifyPayload(Map payload) throws InvalidSubmission {

		def formXlsId = payload['xlsId']
		if (!formXlsId || formXlsId instanceof List) {
			throw new InvalidSubmission("Required property 'xlsId' is missing")
		}
		return true
	}

	/**
	 * Checks if a given submission is for a repeated set of questions.
	 * @param submission the submission to check
	 * @return true if the submission is for a repeated set of questions, otherwise false.
	 */
	static boolean isRepeatSubmission(def submission) {

		boolean repeatSubmission
		if (submission instanceof List && submission.size() > 0) {
			repeatSubmission = (submission[0] instanceof Map)
		}
		return repeatSubmission
	}
}
