package com.nexusug.xlsform.util

/**
 * Contains utilities for manipulating common operations for <tt>XML</tt> manipulation.
 *
 * Created by ivan on 27/03/2016.
 */
class XmlUtil {

	/**
	 * Extracts the name of an XML node element.
	 * @param node the node to extract the name from.
	 * @return the name of the node.
	 */
	static String getNodeName(Node node) {
		String name
		if (node.name() instanceof groovy.xml.QName || node.name() instanceof javax.xml.namespace.QName) {
			name = node.name().localPart
		} else {
			name = node.name()
		}
		return name
	}
}
