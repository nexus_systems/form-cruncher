package com.nexusug.xlsform.util;
/**
 * A bunch of small utilities for managing strings.
 *
 * Created by ivan on 11/03/2016.
 */
class StringUtils {

	/**
	 * Removes all extra whitespace and converts invisible characters into spaces. For example <tt>" i   am   ivan "</tt> <br/>
	 * will result into a string <tt>" i am ivan "</tt>, <tt>"check   this"</tt> will result into <tt>"check this"</tt>,<br/>
	 * <tt>"ivan \n orone         "</tt> will result into <tt>"ivan orone "<tt> Note that this method doesn't trim the string.
	 * @param target the string from which to remove the extra white space.
	 * @return
	 */
	static String sanitizeExtraWhiteSpace(String target) {
		return target.replaceAll("\\s+", " ");
	}
}
