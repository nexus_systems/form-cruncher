package com.nexusug.xlsform.util
/**
 * Created by ivan on 02/05/2017.
 */
class ShellUtil {

	static int executeOnShell(List<String> command, StringBuilder outputHolder, File workingDir) {
		def process = new ProcessBuilder(command)
				.directory(workingDir)
				.redirectErrorStream(true)
				.start()
		process.inputStream.eachLine { outputHolder.append(it) }
		process.waitFor()
		return process.exitValue()
	}
}
