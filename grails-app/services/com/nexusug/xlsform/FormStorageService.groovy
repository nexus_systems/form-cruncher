package com.nexusug.xlsform

import com.nexusug.xlsform.display.util.XformReader
import com.nexusug.xlsform.exception.MisConfigurationException
import com.nexusug.xlsform.exception.XlsFormConversionException
import com.nexusug.xlsform.util.ConfigUtil
import com.nexusug.xlsform.util.ShellUtil
import grails.transaction.Transactional
import org.codehaus.groovy.runtime.DateGroovyMethods

@Transactional
class FormStorageService {

	/**
	 * Creates an {@link Xform} with all the required properties.
	 * @param xform the xml content from which to create the {@link Xform}.
	 *
	 * @return the created {@link Xform}
	 */
	Xform createXform(String xform) {
		Xform form = populateXform(xform, new Xform())
		form.save(failOnError: true)
		return form
	}

	boolean convertXlsFormToXform(String formLocation, String xFormDestination, StringBuilder conversionReportHolder) {
		List<String> command = []
		command << 'python'
		command << 'pyxform/xls2xform.py'
		command << formLocation
		command << xFormDestination

		return ShellUtil.executeOnShell(command, conversionReportHolder, new File(getPyxformLocation())) == 0
	}

	Xform createOrUpdateXform(XlsFormUploadCommand formUploadCommand, Xform xform = null) {
		XlsFormUpload formUpload = processXlsFormUploadToXform(formUploadCommand, '1')
		// create an xform from the upload
		if (formUpload.uploadStatus == XlsFormConversionStatus.SUCCESSFUL) {
			String xFormContent = new File(formUpload.xformContentLocation).text
			xform = populateXform(xFormContent, xform ?: new Xform())
			if (xform.validate()) {
				xform.save(failOnError: true)
				formUpload.xform = xform
				formUpload.save(failOnError: true)
			}
		} else {
			String fileName = formUpload.fileName
			String conversionTime = DateGroovyMethods.format(formUpload.dateCreated, 'EEE, d MMM yyyy HH:mm')

			throw new XlsFormConversionException("Error converting XLSForm to Xform, " +
					"see xlsform ${fileName} created at ${conversionTime} for details")
		}
		return xform
	}

	XlsFormUpload processXlsFormUploadToXform(XlsFormUploadCommand formUploadCommand, String destination) {
		File destinationDir = new File(getUploadDir(), destination)
		if (!destinationDir.exists()) {
			if (!destinationDir.mkdirs()) {
				String errorMessage = "Could not create file upload directory, " +
						"ensure that the app has permissions to create files in the specified location:" +
						" ${destinationDir.getAbsolutePath()}"
				throw new MisConfigurationException(errorMessage)
			}
		}

		// store the xls form being uploaded
		String escapedOriginalFileName = sanitizeFileName(formUploadCommand.uploadFile.originalFilename)
		File destinationFile = new File("${destinationDir.getAbsolutePath()}/${escapedOriginalFileName}")
		formUploadCommand.uploadFile.transferTo(destinationFile)

		// convert the xls form to an xform
		String xformLocation = "${destinationDir.getAbsolutePath()}" +
				"/${escapedOriginalFileName.substring(0, escapedOriginalFileName.lastIndexOf('.'))}.xml"
		StringBuilder conversionReportHolder = new StringBuilder()
		boolean conversionResult = convertXlsFormToXform(destinationFile.absolutePath, xformLocation, conversionReportHolder)

		// save the results of the xls form conversion.
		XlsFormUpload formUpload = new XlsFormUpload()
		formUpload.fileName = formUploadCommand.uploadFile.originalFilename
		formUpload.uploadLocation = destinationFile.absolutePath
		formUpload.conversionReport = conversionReportHolder.toString()
		formUpload.xformContentLocation = xformLocation
		formUpload.uploadStatus = conversionResult ? XlsFormConversionStatus.SUCCESSFUL : XlsFormConversionStatus.FAILED
		formUpload.save(failOnError: true)
		return formUpload
	}

	private static String sanitizeFileName(String fileName) {
		return fileName.replaceAll('[\\)\\(\\[\\]]', "_").replaceAll('\\s', '').replaceAll('_+', '_')
	}

	private static String getUploadDir() {
		return new File(ConfigUtil.getProperty(ConfigUtil.XLS_FORM_UPLOAD_DIR, 'xls_form_uploads'))
	}

	private static String getPyxformLocation() {
		return new File(ConfigUtil.getProperty(ConfigUtil.PYXFORM_INSTALL_LOCATION,
				"${System.getProperty("user.home")}/pyxform"))
	}

	private static Xform populateXform(String xform, Xform form) {
		form.name = XformReader.getFormName(xform)
		form.title = XformReader.getFormTitle(xform)
		form.xlsId = XformReader.getFormId(xform)
		form.formXml = xform
		return form
	}
}
