package com.nexusug.xlsform

import com.nexusug.xlsform.display.model.XformMetaData
import com.nexusug.xlsform.exception.InvalidSubmission
import com.nexusug.xlsform.exception.XformNotFoundException
import com.nexusug.xlsform.exception.XformSubmissionNotFoundException
import com.nexusug.xlsform.util.JsonSubmissionUtil
import com.nexusug.xlsform.util.StringUtils
import com.nexusug.xlsform.util.XmlUtil
import grails.transaction.Transactional
import groovy.json.JsonSlurper

@Transactional
class DataService {

	/**
	 * Gets all submissions that have not been marked as deleted by the user.
	 *
	 * @param queryProperties optional map containing properties such as the offset, page size (max) etc.
	 * @return list of non user deleted submissions based on the supplied <tt>queryProperties</tt>
	 */
	@Transactional(readOnly = true)
	List<XformSubmission> getSubmissions(Map queryProperties = null) {
		List<XformSubmission> submissions
		String query = "SELECT submission FROM XformSubmission submission WHERE archived = 'false' ORDER BY dateCreated DESC"
		if (queryProperties) {
			submissions = XformSubmission.executeQuery(query, queryProperties)
		} else {
			submissions = XformSubmission.executeQuery(query)
		}
		return submissions
	}

	/**
	 * Looks up all {@link XformSubmission} instances matching the supplied <tt>formXlsId</tt>
	 * @param formXlsId the xls id of the form whose submissions are to be looked up.
	 * @return non null list containing the submissions.
	 */
	@Transactional(readOnly = true)
	List<XformSubmission> getSubmissions(String formXlsId) {
		StringBuilder query = new StringBuilder()
		query.append("SELECT submission FROM XformSubmission submission WHERE ")
		query.append("submission.archived = 'false' AND submission.xform.xlsId = :formXlsId ")
		query.append("ORDER BY dateCreated DESC ")
		List<XformSubmission> submissions = XformSubmission.executeQuery(query.toString(), [formXlsId: formXlsId])
		return submissions
	}

	/**
	 * Updates a given submission with content of the provided <tt>jsonPayload</tt>
	 * @param jsonPayload the payload containing the new properties of the submission and it's id in the db currently
	 * @return
	 * @throws XformSubmissionNotFoundException
	 * @throws InvalidSubmission
	 */
	XformSubmission updateSubmission(Map jsonPayload) throws XformSubmissionNotFoundException, InvalidSubmission {
		String submissionId = jsonPayload.submissionId?.toString()
		if (!submissionId?.isNumber()) {
			throw new InvalidSubmission("invalid submission id: $submissionId")
		}
		XformSubmission oldSubmission = XformSubmission.findByIdAndArchived(Long.parseLong(submissionId), false)
		if (!oldSubmission) {
			throw new XformSubmissionNotFoundException("could not find find submission matching id:$submissionId")
		}

		XformSubmission submission = captureJsonSubmission(jsonPayload)

		oldSubmission.archived = true
		oldSubmission.save(failOnError: true)

		return submission
	}

	/**
	 * Creates a {@link XformSubmission} instance for a given <tt>jsonPayload</tt>
	 * @param jsonPayload Map containing the JSON to create the {@link XformSubmission} from.
	 * @return the created {@link XformSubmission}
	 * @throws XformNotFoundException
	 * @throws InvalidSubmission
	 */
	XformSubmission captureJsonSubmission(Map jsonPayload) throws XformNotFoundException, InvalidSubmission {

		JsonSubmissionUtil.verifyPayload(jsonPayload)

		String formXlsId = jsonPayload.remove('xlsId')
		Xform form = Xform.findByXlsId(formXlsId)
		if (!form) {
			throw new XformNotFoundException("no form matched name: $formXlsId")
		}

		XformSubmission submission = new XformSubmission()
		submission.metaData = [:]
		submission.xform = form

		//holds the current in repeat index for submissions.
		Map<String, Integer> repeatSubmissionCountMap = [:]

		jsonPayload.each { String question, answer ->

			if (JsonSubmissionUtil.isRepeatSubmission(answer)) {
				if (question.equals(XformMetaData.meta.name())) {
					answer.each { metaData ->
						metaData.each { String metaDataKey, metaDataValue ->
							submission.metaData.put(metaDataKey, metaDataValue.toString())
						}
					}
				} else {
					answer.each { repeatSubmission ->
						int inRepeatIndex = repeatSubmissionCountMap[question] ?: 0
						repeatSubmission.each { String submissionQuestion, submissionValue ->
							submission.addToQuestions(createRepeatXformQuestion(submissionQuestion, submissionValue,
									question, inRepeatIndex))
						}
						repeatSubmissionCountMap[question] = ++inRepeatIndex
					}
				}
			} else {
				if (question in XformMetaData.values()*.name()) {
					submission.metaData.put(question, answer.toString())
				} else {
					submission.addToQuestions(createXformQuestion(question, answer))
				}
			}
		}
		submission.save(failOnError: true)

		return submission
	}

	/**
	 * Creates a {@link XformSubmission} instance for a given <tt>jsonPayload</tt>
	 * @param jsonPayload string containing the JSON to create the {@link XformSubmission} from.
	 * @return the created {@link XformSubmission}
	 * @throws XformNotFoundException
	 * @throws InvalidSubmission
	 */
	XformSubmission captureJsonSubmission(String jsonPayload) throws XformNotFoundException, InvalidSubmission {
		Map formModel = new JsonSlurper().parseText(jsonPayload)
		captureJsonSubmission(formModel)
	}

	/**
	 * Creates a {@link XformSubmission} instance for a given <tt>xmlPayload</tt>
	 * @param xmlPayload {@link Node} containing the <tt>XML</tt> to create the {@link XformSubmission} from.
	 * @return the created {@link XformSubmission}
	 * @throws InvalidSubmission
	 * @throws XformNotFoundException
	 */
	XformSubmission captureXMLSubmission(Node xmlPayload) throws InvalidSubmission, XformNotFoundException {
		String formXlsId = xmlPayload.@id
		if (!formXlsId) {
			throw new InvalidSubmission("Invalid form id for the submission is missing.")
		}

		Xform form = Xform.findByXlsId(formXlsId)
		if (!form) {
			throw new XformNotFoundException("no form matched name: $formXlsId")
		}
		XformSubmission submission = new XformSubmission()
		submission.metaData = [:]
		submission.xform = form

		//holds the current in repeat index for submissions.
		Map<String, Integer> repeatSubmissionCountMap = [:]

		xmlPayload.children().each { Node node ->
			String question = XmlUtil.getNodeName(node)
			if (node.'*'.'*'.size() > 0) {
				if (question.equals(XformMetaData.meta.name())) {
					node.children().each { Node metaData ->

						String metaDataName = XmlUtil.getNodeName(metaData)
						submission.metaData.put(metaDataName, metaData.value()?.getAt(0)?.toString())
					}
				} else {
					int inRepeatIndex = 0
					node.children().each { Node repeatElement ->
						String repeatQuestion = XmlUtil.getNodeName(repeatElement)
						inRepeatIndex = repeatSubmissionCountMap[question] ?: 0
						submission.addToQuestions(
								createRepeatXformQuestion(repeatQuestion, repeatElement.value()?.getAt(0),
										question, inRepeatIndex))
					}
					repeatSubmissionCountMap[question] = ++inRepeatIndex
				}
			} else {
				if (question in XformMetaData.values()*.name()) {
					submission.metaData.put(question, (String)node.value()?.getAt(0))
				} else {
					submission.addToQuestions(createXformQuestion(question, node.value()?.getAt(0)))
				}
			}
		}
		submission.save(failOnError: true)

		return submission
	}

	/**
	 * Creates a {@link XformSubmission} instance for a given <tt>xmlPayload</tt>
	 * @param xmlPayload string containing the <tt>XML</tt> to create the {@link XformSubmission} from.
	 * @return the created {@link XformSubmission}
	 * @throws InvalidSubmission
	 * @throws XformNotFoundException
	 */
	XformSubmission captureXMLSubmission(String xmlPayload) throws InvalidSubmission, XformNotFoundException {
		captureXMLSubmission(new XmlParser().parseText(xmlPayload))
	}

	/**
	 * Helper method for creating an {@link XformQuestion} that belongs to an <tt>Xform</tt> <tt>Repeat</tt>
	 * @param question the question key
	 * @param answer the answer, could be a set of just a string.
	 * @param repeatName the name of the <tt>Repeat</tt>
	 * @param inRepeatIndex the index of the <tt>Repeat</tt> in the submission
	 * @return the created {@link XformQuestion}
	 */
	private static XformQuestion createRepeatXformQuestion(String question,
														def answer, String repeatName, int inRepeatIndex) {
		XformQuestion formQuestion = createXformQuestion(question, answer)
		formQuestion.repeatQuestion = true
		formQuestion.repeatName = repeatName
		formQuestion.inRepeatIndex = inRepeatIndex
		return formQuestion
	}

	/**
	 * Helper method for creating an {@link XformQuestion}.
	 * @param question the question key
	 * @param answer the answer to the question, can be a <tt>string</tt> or a <tt>string</tt> <tt>List</tt>
	 * @return the created {@link XformQuestion}
	 */
	private static XformQuestion createXformQuestion(String question, def answer) {
		XformQuestion formQuestion = new XformQuestion(question: question)

		if (answer instanceof List) {
			answer.each {
				formQuestion.addToAnswers([value: (it ? StringUtils.sanitizeExtraWhiteSpace(it?.toString()) : it)])
			}
		} else {
			formQuestion.addToAnswers([value: (answer ? StringUtils.sanitizeExtraWhiteSpace(answer?.toString()) : answer)])
		}
		return formQuestion
	}
}
