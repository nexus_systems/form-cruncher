package com.nexusug.xlsform

import com.nexusug.xlsform.reporting.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ReportManagementService {

	/**
	 * Updates a report and all the properties associated with it.
	 * //todo need to write up tests for this.
	 * @param report the report to update.
	 * @return the updated report.
	 */
	Report updateReport(Report report) {

		//delete all currently existing conditions
		Condition.withNewSession {
			String query = "SELECT condition FROM Condition condition WHERE condition.report.id = ${report.id}"
			List<Condition> existingConditions = Condition.executeQuery(query)
			existingConditions.each {
				it.delete(flush: true)
			}
		}

		//delete all currently existing display columns
		DisplayColumn.withNewSession {
			String query = "SELECT displayColumn FROM DisplayColumn displayColumn WHERE displayColumn.report.id = ${report.id}"
			List<DisplayColumn> existingDisplayColumns = DisplayColumn.executeQuery(query)
			existingDisplayColumns.each {
				it.delete(flush: true)
			}
		}

		report.save(failOnError: true, flush: true)
		return report
	}

	List<ReportEntry> generateReport(Report report) {
		boolean isMultiFormReport = report.formLinkerQuestion ? true : false
		List<XformSubmission> matchedSubmissions = getSubmissionsMatchingReport(report,isMultiFormReport)
		if (isMultiFormReport) {
			return MultiFormReportHelper.generateReportEntries(report, matchedSubmissions)
		} else {
			return matchedSubmissions.collect { new ReportEntry(submissions: [it])}
		}
	}

	/**
	 * Get all {@link XformSubmission}s whose questions match any one of the {@link Report} conditions.
	 *
	 * @param report
	 * @param isMultiFormReport whether or not the report encompasses multiple form types.
	 * @return
	 */
	private static List<XformSubmission> getSubmissionsMatchingReport(Report report, boolean isMultiFormReport) {

		Map<String, Object> namedParameters = new HashMap<>()

		StringBuilder query = new StringBuilder()
		query.append("SELECT DISTINCT submission from XformSubmission submission ")

		Integer conditionLinkerSize = report.conditionLinkers?.size() ?: 0
		report.conditions.eachWithIndex { Condition condition, Integer index ->
			String questionNamedParameter = 'question_' + index
			String answerNamedParameter = 'answer_' + index
			String xlsIdNamedParameter = 'xlsId_' + index
			boolean addConditionGrouper = false
			if (index == 0) {
				query.append(" WHERE ")
				conditionLinkerSize.times {
					query.append("(")
				}
			} else {
				addConditionGrouper = true
				if (isMultiFormReport) {
					query.append(" OR ")
				} else {
					query.append(report.conditionLinkers[index - 1].name())
				}
			}

			query.append(" submission.id IN ( ")
			query.append(" SELECT formQuestion.submission.id from XformQuestion formQuestion ")
			query.append(" INNER JOIN formQuestion.answers as answer where question = :$questionNamedParameter ")
			query.append(" AND formQuestion.submission.xform.xlsId = :$xlsIdNamedParameter ")
			query.append(" AND ")
			query.append(condition.operator.generateHql('answer.value', ':' + answerNamedParameter))
			query.append(") ")

			if (addConditionGrouper) {
				// enclose conditions so that evaluation is always between two conditions and validated from left to right
				query.append(") ")
			}

			namedParameters.put(questionNamedParameter, condition.questionKey)
			namedParameters.put(xlsIdNamedParameter, condition.formXlsId)
			namedParameters.put(answerNamedParameter, condition.operator.processOperand(condition.operand))
		}
		return XformSubmission.executeQuery(query.toString(), namedParameters)
	}
}
