package com.nexusug.xlsform

import com.nexusug.Download
import com.nexusug.xlsform.exception.XlsFormConversionException
import com.nexusug.xlsform.util.DownloadUtil
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional(readOnly = true)
class FormController {

	FormStorageService formStorageService

	def index() {
		def xformList = Xform.findAllByDeleted(false, [max: 50, sort: 'lastUpdated', order: 'desc']).collect {
			[
					id         : it.id,
					name       : it.name,
					title      : it.title,
					xlsId      : it.xlsId,
					lastUpdated: g.formatDate(date: it.lastUpdated.getTime()),
					dateCreated: g.formatDate(date: it.dateCreated)
			]

		}
		def xlsFormList = XlsFormUpload.findAllByDeleted(false, [max: 50, sort: 'dateCreated', order: 'desc']).collect {
			[
					id              : it.id,
					xformTitle      : it.xform ? (it.xform.isDeleted() ? it.xform?.title + ' (deleted)' : it.xform?.title) : null,
					fileName        : it.fileName,
					dateCreated     : g.formatDate(date: it.dateCreated.getTime()),
					uploadStatus    : it.uploadStatus.name(),
					conversionReport: it.conversionReport
			]
		}

		return [xlsFormList: xlsFormList as JSON, xformList: xformList as JSON]
	}

	def view(Xform xform) {
		if (xform) {
			return [xform: xform]
		}
	}

	def downloadXlsForm(XlsFormUpload formUpload) {
		if (formUpload) {
			File xlsFile = new File(formUpload.uploadLocation)
			String fileName = formUpload.fileName
			String contentType = fileName.endsWith('xlsx') ?
					'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' : 'application/vnd.ms-excel'
			Download download = new Download(file: xlsFile, fileName: fileName, contentType: contentType)
			DownloadUtil.download(download, response)
		} else {
			flash.error = "XLSForm upload could not be found"
			redirect action: 'index'
		}
	}

	@Transactional
	def deleteXlsFormUpload(XlsFormUpload formUpload) {
		if (formUpload) {
			formUpload.deleted = true
			formUpload.save(failOnError: true)
			flash.message = "XLSForm upload successfully deleted"
		} else {
			flash.error = "XLSForm upload could not be found"
		}
		redirect action: 'index'
	}

	def downloadXform(Xform xform) {
		if (xform) {
			String fileName = "${xform.xlsId.toLowerCase()}.xml"
			Download download = new Download(content: xform.formXml, fileName: fileName, contentType: 'application/xml')
			DownloadUtil.download(download, response)
		} else {
			flash.error = "XForm could not be found"
			redirect action: 'index'
		}
	}

	@Transactional
	def deleteXform(Xform xform) {
		if (xform) {
			String originalXlsId = xform.xlsId
			xform.xlsId = xform.xlsId + '_deleted_' + System.currentTimeMillis()
			xform.deleted = true
			xform.save(failOnError: true)
			flash.message = "Xform ${originalXlsId} was successfully deleted"
		} else {
			flash.error = "specified Xform could not be found"
		}
		redirect action: 'index'
	}

	@Transactional
	def update(XlsFormUploadCommand formUpload) {
		Xform xform = Xform.get(formUpload.xformId)
		if (formUpload.validate()) {
			try {
				xform = formStorageService.createOrUpdateXform(formUpload, xform)
				if (xform.hasErrors()) {
					flash.errors = xform.errors.collect { g.message(error: it).toString() }
				} else {
					flash.message = "xform successfully updated"
				}
			} catch (XlsFormConversionException exception) {
				flash.error = exception.message
			}
			redirect action: 'view', id: xform.id
		} else {
			List<String> errors = formUpload.errors.collect { g.message(error: it).toString() }
			flash.errors = errors
			redirect action: 'view', id: xform.id
		}
	}

	@Transactional
	def upload(XlsFormUploadCommand formUpload) {
		if (formUpload.validate()) {
			try {
				Xform xform = formStorageService.createOrUpdateXform(formUpload)
				if (xform.hasErrors()) {
					flash.errors = xform.errors.collect { g.message(error: it).toString() }
				} else {
					flash.message = "xform successfully created"
				}
			} catch (XlsFormConversionException exception) {
				flash.error = exception.message
			}
			redirect action: 'index'
		} else {
			List<String> errors = formUpload.errors.collect { g.message(error: it).toString() }
			flash.errors = errors
			redirect action: 'index'
		}
	}
}
