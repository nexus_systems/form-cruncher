package com.nexusug.xlsform

import com.nexusug.xlsform.display.model.XformDisplayModel
import com.nexusug.xlsform.display.util.XformReader
import com.nexusug.xlsform.reporting.ConditionLinker
import com.nexusug.xlsform.reporting.ConditionOperator
import com.nexusug.xlsform.reporting.Report
import com.nexusug.xlsform.reporting.ReportEntry
import grails.converters.JSON
import grails.transaction.Transactional
import groovy.json.JsonBuilder

import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY
import static org.springframework.http.HttpStatus.NOT_FOUND

@Transactional(readOnly = true)
class ReportController {
	ReportManagementService reportManagementService

	def index() {

	}

	def list() {
		withFormat {
			'*' {
				respond Report.findAllWhere(archived: false)
			}
		}
	}

	def view(Report report) {
		if (report == null) {
			notFound()
			return
		}
		respond report
	}

	def edit(Report report) {
		if (report == null) {
			notFound()
			return
		}
		Map model = reportCreationModel()

		model.reportJsonModel = report as JSON
		println model.reportJsonModel
		respond report, model: model
	}

	@Transactional
	def delete(Report report) {
		if (report == null) {
			notFound()
			return
		}
		report.archived = true
		report.save(failOnError: true, flush: true)
		flash.message = g.message(code: 'report.delete.success.message', args: [report.name])
		redirect action: 'index'
	}

	def submissions(Report report) {
		if (report == null) {
			notFound()
			return
		}
		List<ReportEntry> reportEntries = reportManagementService.generateReport(report)
		respond reportEntries
	}

	def create() {
		respond new Report(params), model: reportCreationModel()
	}

	/**
	 * Helper method for returns properties needed typically when creating a new report.
	 * @return
	 */
	private static Map reportCreationModel() {
		List<XformDisplayModel> formDisplayModels = []
		Map<String, String> allFormsXlsIdModelMap = [:]

		Xform.all?.each { Xform form ->
			XformDisplayModel formDisplayModel = new XformDisplayModel()
			formDisplayModel.name = form.name
			formDisplayModel.title = form.title
			formDisplayModel.xlsId = form.xlsId
			formDisplayModel.displayables = XformReader.getFormElements(form.formXml)

			formDisplayModels.add(formDisplayModel)
			allFormsXlsIdModelMap.put(form.xlsId, XformReader.serializeFormModelToJSON(form.formXml, 0))
		}
		def conditionOperators = [:]
		conditionOperators['decimal'] = ConditionOperator.values().findAll { it.canValidate('1.0') && !it.name().startsWith('TEXT') }.collect {
			[text: it.userFriendlyName(), value: it.name()]
		}
		conditionOperators['integer'] = ConditionOperator.values().findAll { it.canValidate('1') && !it.name().startsWith('TEXT')}.collect {
			[text: it.userFriendlyName(), value: it.name()]
		}
		conditionOperators['text'] = ConditionOperator.values().findAll { it.canValidate('text') }.collect {
			[text: it.userFriendlyName(), value: it.name()]
		}

		Map model = [
				formDisplayModelsJson: new JsonBuilder(formDisplayModels).toString(),
				allFormsXlsIdModelMap: new JsonBuilder(allFormsXlsIdModelMap).toString(),
				conditionLinkers     : ConditionLinker.values()*.name() as JSON,
				conditionOperators   : conditionOperators as JSON
		]
		return model
	}

	@Transactional
	def saveOrUpdate(Report report) {
		if (report.hasErrors()) {
			withFormat {
				html {
					render report.errors, view: 'create'
				}
				'*' {
					response.status = UNPROCESSABLE_ENTITY.value()
					respond report
				}
			}
		} else {
			handleBelongsToAssociationGotcha(report)
			if (report.id) {
				reportManagementService.updateReport(report)
			} else {
				report.save(flush: true, failOnError: true)
			}
			withFormat {
				html {
					flash.message = message(code: 'default.created.message', args: [message(code: 'report.label', default: 'report'), report.id])
					redirect report
				}
				'*' { render status: CREATED }
			}
		}
	}

	/**
	 * This method exists due to a bug that can vaguely be explained by
	 * <a href="https://github.com/robfletcher/grails-gson#cascading-saves">cascading-saves gotcha in gson lib</a>
	 * @param report the report whose children with belongsTo annotation are to be assigned the report manually.
	 */
	private void handleBelongsToAssociationGotcha(Report report) {
		report.conditions?.each {
			it.report = report
		}
		report.displayColumns?.each {
			it.report = report
		}
	}

	protected void notFound() {
		//todo this doesn't seem to work, needs to be fixed
		withFormat {
			html {
				flash.message = message(code: 'default.not.found.message', args: [
						message(code: 'report.label', default: 'report'),
						params.id
				])
				redirect action: "index"
			}
			'*' { render status: NOT_FOUND }
		}
	}
}
