package com.nexusug.xlsform

import com.nexusug.xlsform.display.model.XformDisplayModel
import com.nexusug.xlsform.display.util.XformReaderEditUtils
import com.nexusug.xlsform.display.util.XformReader
import com.nexusug.xlsform.exception.InvalidSubmission
import com.nexusug.xlsform.exception.XformNotFoundException
import grails.transaction.Transactional
import groovy.json.JsonBuilder
import org.codehaus.groovy.grails.plugins.web.api.MimeTypesApiSupport
import org.springframework.http.HttpStatus

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class DataController {
	static allowedMethods = [save: 'POST']

	private MimeTypesApiSupport apiSupport = new MimeTypesApiSupport()

	DataService dataService

	def index() {
		List<XformSubmission> submissions = dataService.getSubmissions([max: 20])
		List<Xform> formList = Xform.all
		[mostRecentSubmissions: submissions, formList: formList]
	}

	def viewFormSubmissions(String id) {
		redirect action: 'submissions', id: id
	}

	def submissions(String id) {
		if (!id) {
			flash.warn = "No form provided"
			redirect action: 'index'
			return
		}
		Xform form = Xform.findByXlsId(id)
		if (!form) {
			flash.error = "Could not find form with xlsId: ${id}"
			redirect action: 'index'
			return
		}
		List<XformSubmission> submissions = dataService.getSubmissions(id)

		[submissions: submissions, xform: form]
	}

	def captureFormData(String id) {
		redirect action: 'capture', id: id
	}

	def capture() {
		String formId = params.id
		if (!formId) {
			redirect action: 'index'
			return
		}

		Xform form = Xform.findByXlsId(formId)
		if (!form) {
			flash.error = "Could not find find form with id: ${formId}"
			redirect action: 'index'
			return
		}

		XformDisplayModel xformDisplayModel = new XformDisplayModel()
		xformDisplayModel.name = form.name
		xformDisplayModel.title = form.title
		xformDisplayModel.xlsId = form.xlsId
		xformDisplayModel.xformId = form.id
		xformDisplayModel.displayables = XformReader.getFormElements(form.formXml)
		String xformModelJson = XformReader.serializeFormModelToJSON(form.formXml)

		[
				xformModelJson       : xformModelJson,
				xformDisplayModel    : xformDisplayModel,
				xformDisplayModelJson: new JsonBuilder(xformDisplayModel).toPrettyString()
		]
	}

	def edit(long id) {
		XformSubmission submission = XformSubmission.get(id)
		Xform xform = submission.xform

		XformDisplayModel xformDisplayModel = new XformDisplayModel()
		xformDisplayModel.name = xform.name
		xformDisplayModel.title = xform.title
		xformDisplayModel.xlsId = xform.xlsId
		xformDisplayModel.displayables = XformReaderEditUtils.getFormElements(xform.formXml, submission)
		String xformModelJson = XformReader.serializeFormModelToJSON(xform.formXml)

		[
				submission           : submission,
				xformModelJson       : xformModelJson,
				xformDisplayModel    : xformDisplayModel,
				xformDisplayModelJson: new JsonBuilder(xformDisplayModel).toPrettyString()
		]
	}

	@Transactional
	def update() {
		apiSupport.withFormat(request) {
			'json' {
				Map payload = request.getJSON()
				dataService.updateSubmission(payload)
				render status: ACCEPTED
			}
			'*' {
				render status: UNSUPPORTED_MEDIA_TYPE
			}
		}
	}

	@Transactional
	def save() {
		apiSupport.withFormat(request) {
			'json' {
				Map payload = request.getJSON()
				dataService.captureJsonSubmission(payload)
				render status: CREATED
			}
			'xml' {
				String payload = request.reader.text
				dataService.captureXMLSubmission(payload)
				render status: CREATED
			}
			'*' {
				render status: UNSUPPORTED_MEDIA_TYPE
			}
		}
	}

	@Transactional
	def delete(XformSubmission submission) {
		if (submission == null) {
			notFound()
			return
		}
		submission.archived = true
		submission.save(failOnError: true, flush: true)
		withFormat {
			html {
				flash.message = g.message(code: 'submission.deleted.message')
				redirect action: 'index'
			}
			'*' {
				render status: OK
			}
		}
	}

	def handleXformNotFoundException(XformNotFoundException exception) {
		handleError(exception.message, BAD_REQUEST)
	}

	def handleInvalidSubmission(InvalidSubmission exception) {
		handleError(exception.message, BAD_REQUEST)
	}

	private void handleError(String message, HttpStatus status) {
		withFormat {
			'json' {
				render text: message, status: status, contentType: 'application/json'
			}
			'xml' {
				render text: message, status: status, contentType: 'application/xml'
			}
			'*' {
				render text: message, status: status
			}
		}
	}

	protected void notFound() {
		//todo this doesn't seem to work, needs to be fixed
		withFormat {
			html {
				flash.message = message(code: 'default.not.found.message', args: [
						message(code: 'submission.label', default: 'submission'),
						params.id
				])
				redirect action: "index"
			}
			'*' { render status: NOT_FOUND }
		}
	}
}
