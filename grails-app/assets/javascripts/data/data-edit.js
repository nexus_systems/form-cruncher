/**
 * Created by ivan on 26/03/2016.
 */
var DataEditUtil = function () {
    var
        formXlsId,

        onEditSuccessHandler = function () {
            location.href = '/{context}/data/submissions/{formXlsId}'.format({
                context: appContext,
                formXlsId: formXlsId
            });
        },

        init = function () {
            formDataCaptureHelper.init({successHandler: onEditSuccessHandler});
            formXlsId = $('#edit-form-xls-id').val();
        };

    init();
};

var dataCaptureHelper;
$(function () {
    dataCaptureHelper = new DataEditUtil();
});