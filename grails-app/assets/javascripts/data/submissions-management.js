/**
 * Created by ivan on 15/05/2016.
 */

var SubmissionsManagementUtil = function () {
    var
        attachOnViewSubmissionsFormSubmit = function () {
            $('#view-submissions').on('submit', function (event) {
                event.preventDefault();
                location.href = '/{context}/data/submissions/{formXlsId}'.format({
                    context: appContext,
                    formXlsId: $('#form-xlsId').val()
                });
            });
        },

        /**
         * Initializes the popover that is shown when the delete submission button is clicked.
         */
        initPopConfirm = function () {
            $("[data-toggle='confirm-delete-submission']").popConfirm({
                title: "confirm deletion",
                content: "Are you sure you want to delete this submission?",
                placement: "left"
            });
        },

        init = function () {
            attachOnViewSubmissionsFormSubmit();
            initPopConfirm();
        };

    init()
};

var submissionsManagementUtil;
$(function () {
    submissionsManagementUtil = new SubmissionsManagementUtil()
});