/**
 * Created by ivan on 26/03/2016.
 */
var DataCaptureUtil = function () {
    var
        init = function () {
            formDataCaptureHelper.init();
        };

    init();
};

var dataCaptureHelper;
$(function () {
    dataCaptureHelper = new DataCaptureUtil();
});