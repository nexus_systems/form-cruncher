/**
 * Created by ivan on 12/12/2015.
 */
var FormVisualizer = function () {
    var model = {
            xformJson: $('#xform').attr('data-value'),
            xformModelJson: $('#xformModel').attr('data-value')
        },

        defaultSubmissionHandler = function (form) {
            form.submit();
        },

        init = function (opts) {
            var defaultOptions = {
                submitHandler: defaultSubmissionHandler, form: 'form',
                elementsContainer: '.elements-container', resetValidator: false
            };

            var options = $.extend(defaultOptions, opts);

            if (options.resetValidator) {
                var validator = $(options.form).validate();
                validator.destroy();
            }

            $(options.elementsContainer).buildXform(model);
            $(options.form).validate({debug: true, submitHandler: options.submissionHandler});
        };

    return {
        init: init
    }
};

var formVisualizer;
$(function () {
    formVisualizer = new FormVisualizer();
});