/**
 * Created by ivan on 14/05/2016.
 */
var FormDataCaptureHelper = function () {
    var
    // container where notifications can be placed on the page.
    // Should typically be located at the top of the page, right below the context menu
        notificationHolder,
    // the page context menu, the default error and success handlers scroll to the page context menu
        pageContextMenu,
    // the container where xls form elements can be placed.
        pageElementsContainer,
    // function to be called when the data is successfully captured in the backend.
        successHandler,
    // function to be called when the data has not been captured successfully in the backend
        errorHandler,

        /**
         * Helper method for handling submission of form data.
         * @param form the form that is being saved.
         * @returns {boolean} false if the form supplied has an id equal to <tt>data-capture-form</tt>
         */
        submissionHandler = function (form) {
            var model = $(form).serializeJSON({useIntKeysAsArrayIndex: true}),
                saveUrl = $(form).attr('action');
            $.ajax({
                type: 'post',
                url: saveUrl,
                data: JSON.stringify(model),
                contentType: "application/json",
                headers: {
                    Accept: "application/json;charset=utf-8"
                },
                success: function () {
                    successHandler();
                },
                statusCode: {
                    400: function (args) {
                        errorHandler(args.responseText)
                    },
                    415: function () {
                        errorHandler('unsupported content type')
                    }
                }
            });
            return false;
        },

        /**
         * resets the xform data submission form.
         */
        resetCaptureForm = function () {
            $(pageElementsContainer).empty();
            init({resetValidator: true});
        },

        /**
         * Creates a notification on the ui.
         * @param options {Object} typically contains a message, error or warn property whose value is what will be shown
         * on to the user. error, warn, message are used to signify the severity of the message to be shown.
         */
        createNotification = function (options) {
            var handlbarTemplate = Handlebars.partials['notification-partial'],
                notificationsHolder = $(notificationHolder);
            notificationsHolder.html(handlbarTemplate(options));
        },

        /**
         * Helper method for handling errors encountered in submission of form data.
         * @param message the message to be shown.
         */
        handleSubmissionError = function (message) {
            createNotification({error: message});
            page_utils.scrollToLocation($(pageContextMenu));
        },

        /**
         * Helper method for handling successful submission of form data.
         */
        handleSubmissionCreated = function () {
            resetCaptureForm();
            createNotification({'message': 'Data successfully captured'});
            page_utils.scrollToLocation($(pageContextMenu));
        },

        /**
         * initializes the form for data capture to begin.
         * @param options {Object} the various options to pass to the <tt>formVisualizer</tt> when initialising it.
         */
        init = function (options) {
            var defaultOptions = {
                    form: '#data-capture-form',
                    submissionHandler: submissionHandler,
                    successHandler: handleSubmissionCreated,
                    errorHandler: handleSubmissionError,
                    notificationHolder: '.notifications-holder',
                    pageContextMenu: '.submissions-context-nav',
                    pageElementsContainer: '.elements-container'
                },
                opts = $.extend(defaultOptions, options);

            successHandler = opts.successHandler;
            errorHandler = opts.errorHandler;
            notificationHolder = opts.notificationHolder;
            pageContextMenu = opts.pageContextMenu;
            pageElementsContainer = opts.pageElementsContainer;
            formVisualizer.init(opts);
        };
    return {
        init: init
    }
};

var formDataCaptureHelper;
$(function () {
    formDataCaptureHelper = new FormDataCaptureHelper();
});