/**
 * Used for populating a form with it's elements.
 *
 * Created by ivan on 12/12/2015.
 */

(function ($) {
    "use strict";

    $.fn.buildXform = function (options) {
        var parent = this,
            settings = $.extend({
                xformJson: '',
                xformModelJson: '',
                handlebarTemplates: handlebarsUtils.templates
            }, options),

            supportedTypes = $.fn.buildXform.supportedTypes,
            formJson = $.parseJSON(settings.xformJson),
            formDisplayables = formJson.displayables,
            xformModelJson = $.parseJSON(settings.xformModelJson),

            cascadingSelectHelper = $.fn.buildXform.cascadingSelectHelper,

            repeatHelper = {
                addRepeat: function (addRepeatButtonObject, displayable) {
                    var repeatName = addRepeatButtonObject.attr('data-name'),
                        repeat = displayable ? displayable : repeatHelper.findDisplayableByName(repeatName),
                        handlbarTemplate = Handlebars.partials['repeat-partial'],
                        repeatContainer = addRepeatButtonObject.closest('.repeat-container');

                    if (!repeat) {
                        console.warn("could not find repeat element with name " + name);
                        return;
                    }
                    $(handlbarTemplate(repeat)).hide().appendTo(repeatContainer).show("slow");
                    repeatHelper.attachCascadingSelectListeners(repeat);
                },
                findDisplayableByName: function (repeatName) {
                    var element = $.grep(formDisplayables, function (displayable) {
                        return displayable.name === repeatName;
                    });

                    if (element && element.length > 0) {
                        element = element[0];
                    }
                    return element;
                },
                attachButtonListeners: function () {
                    $(parent).on('click', '.add-repeat-button', function () {
                        repeatHelper.addRepeat($(this));
                    });

                    $(parent).on('click', '.remove-repeat-button', function (event) {
                        var repeatName = $(this).attr('name');
                        var repeatInstancesSelector = '.repeat-instance-' + repeatName;
                        if ($(repeatInstancesSelector).length > 1) {
                            var repeatInstance = $(this).closest(repeatInstancesSelector);
                            repeatInstance.slideUp(800, function () {
                                repeatInstance.remove();
                            });
                        }
                    });
                },
                attachCascadingSelectListeners: function (repeat) {
                    $.each(repeat.displayables, function (index, displayable) {
                        if (cascadingSelectHelper.isCascadingSelect(displayable)) {
                            var isRepeat = true;
                            var inRepeatId = handlebarsUtils.readLastGeneratedDistinctIdFor(repeat.name);
                            cascadingSelectHelper.initializeCascadingSelect(parent, xformModelJson, displayable, isRepeat, inRepeatId, repeat.name)
                        }
                    });
                }
            },

            initializeDatePickers = function () {
                $(parent).on("focus", "input.date-time-picker:not(.initialized)", function () {
                    $(this).datetimepicker({format: 'DD/MM/YY hh:mm A'}).addClass('initialized');
                });

                $(parent).on("focus", "input.time-picker:not(.initialized)", function () {
                    $(this).datetimepicker({format: 'hh:mm A'}).addClass('initialized');
                });

                $(parent).on("focus", "input.date-picker:not(.initialized)", function () {
                    $(this).datetimepicker({format: 'DD/MM/YY'}).addClass('initialized');
                });
            },

            bindEventListeners = function () {
                $.each(formDisplayables, function (index, element) {
                    bindCascadingSelectListenerForFormElement(element);
                });
                repeatHelper.attachButtonListeners()
            },

            bindCascadingSelectListenerForFormElement = function (element) {
                if (cascadingSelectHelper.isCascadingSelect(element)) {
                    cascadingSelectHelper.initializeCascadingSelect(parent, xformModelJson, element)
                } else if (element.type === 'group') {
                    $.each(element.displayables, function (index, inGroupDisplayable) {
                        bindCascadingSelectListenerForFormElement(inGroupDisplayable);
                    });
                }
            },

            createXSLFormElements = function () {
                if (formDisplayables) {
                    $.each(formDisplayables, function (index, element) {
                        if ($.inArray(element.type, supportedTypes) || element.type === 'string') {
                            var elementPath = 'elements/' + element.type;
                            var template = handlebarsUtils.templates[elementPath];
                            if (template) {
                                var existingRepeat = parent.find('.repeat-instance-{0}'.format(element.name));
                                if (element.type === 'repeat' && existingRepeat.size() > 0) {
                                    repeatHelper.addRepeat(existingRepeat.last().find('.add-repeat-button'), element);
                                } else {
                                    parent.append(template(element));
                                }
                                if (element.type === 'repeat') {
                                    repeatHelper.attachCascadingSelectListeners(element);
                                }
                            } else {
                                console.warn("could not locate handlebar template for element: " + elementPath);
                            }
                        } else {
                            console.warn("element type: " + element.type + " not supported");
                        }
                    });
                }
            },

            init = function () {
                createXSLFormElements();
                initializeDatePickers();
                bindEventListeners();
            };

        init();
        return parent;
    };
    $.fn.buildXform.supportedTypes = ['string', 'group', 'repeat', 'integer', 'decimal', 'dateTime', 'time', 'date', 'select', 'select1'];
    $.fn.buildXform.cascadingSelectHelper = {
        initializeCascadingSelect: function (parent, xformModelJson, element, isRepeat, inRepeatId, repeatName) {
            var instanceItemFilters = element.itemset.instanceItemFilters;
            $.each(instanceItemFilters, function (filterIndex, filter) {
                var valueRef = filter.instanceValueRef,
                    onchangeSelector,
                    valueRefSelector;

                if (isRepeat) {
                    onchangeSelector = valueRef.substr(valueRef.lastIndexOf('/') + 1);
                    valueRefSelector = "." + onchangeSelector + '-' + inRepeatId;
                    $(parent).on('change', valueRefSelector, function () {
                        $.fn.buildXform.cascadingSelectHelper.evaluateItemset(xformModelJson, element, inRepeatId, repeatName);
                    });
                } else {
                    onchangeSelector = valueRef.substr(valueRef.lastIndexOf('/') + 1);
                    valueRefSelector = "." + onchangeSelector;

                    $(parent).on('change', valueRefSelector, function () {
                        $.fn.buildXform.cascadingSelectHelper.evaluateItemset(xformModelJson, element);
                    });
                }
                $(valueRefSelector).trigger('change');
            });
        },

        evaluateItemset: function (xformModelJson, element, inRepeatId, repeatName) {
            var items,
                rawItems,
                appearance = element.appearance,
                elementName = element.name,
                instanceRef = element.itemset.instanceRef,
                labelRef = element.itemset.labelRef,
                valueRef = element.itemset.valueRef,
                instances = xformModelJson.model.instance,
                updateTargetSelector,

                instance = $.grep(instances, function (entry) {
                    return entry.id === instanceRef;
                });

            if (inRepeatId === null) {
                inRepeatId = undefined
            }

            if (inRepeatId >= 0) {
                updateTargetSelector = '[data-name="' + elementName + '-' + inRepeatId + '"]';

            } else {
                updateTargetSelector = '[data-name="' + elementName + '"]';
            }
            if (instance && instance.length > 0) {
                instance = instance[0];
            }

            rawItems = $.fn.buildXform.cascadingSelectHelper.filterOutItemsFromItemset(element, instance, inRepeatId);
            items = $.fn.buildXform.cascadingSelectHelper.createSelectItemsFromItemset(rawItems, labelRef, valueRef, xformModelJson);
            // append the selected elements to the item list for the selector.
            var handlbarTemplate = Handlebars.partials['select1-items-partial'];
            if (inRepeatId >= 0) {
                var model = {
                    items: items,
                    name: elementName,
                    isRepeat: true,
                    inRepeatIndex: inRepeatId,
                    parent: {name: repeatName},
                    appearance: appearance,
                    value: element.value
                };
                $(updateTargetSelector).html(handlbarTemplate(model));
            } else {
                $(updateTargetSelector).html(handlbarTemplate({
                    items: items,
                    name: elementName,
                    appearance: appearance,
                    value: element.value
                }));
            }
        },

        filterOutItemsFromItemset: function (element, instance, inRepeatId) {
            var instanceItemFilters = $.fn.buildXform.cascadingSelectHelper.createProcessedItemsetInstanceFilters(element, inRepeatId);
            var instances = instance.root.item.filter(function (item) {
                return instanceItemFilters.every(function (filter) {
                    if (!filter.value) {
                        return false
                    }
                    return item[filter.propertyName] === filter.value;
                });
            });
            return instances;
        },

        createProcessedItemsetInstanceFilters: function (element, inRepeatId) {
            var unProcessedInstanceFilters = element.itemset.instanceItemFilters;
            var instanceFilters = [];
            $.each(unProcessedInstanceFilters, function (filterIndex, filter) {
                var instanceValueRef = filter.instanceValueRef;
                var itemPropertyRef = filter.itemPropertyRef;

                // TODO some level of flexibility for specifying the itemPropertyRef has been lost, needs to be put in.
                var selector = itemPropertyRef.substr(itemPropertyRef.lastIndexOf('/') + 1);
                var valueRefElementName = instanceValueRef.substr(instanceValueRef.lastIndexOf('/') + 1);
                var valueRefElementSelector;

                if (inRepeatId >= 0) {
                    valueRefElementSelector = '.' + valueRefElementName + '-' + inRepeatId;
                } else {
                    valueRefElementSelector = '.' + valueRefElementName;
                }

                var elements = $(valueRefElementSelector);
                var filterValue;
                if (element.appearance === 'minimal') {
                    filterValue = elements.val();
                } else {
                    // loop to get the checked radio.
                    for (var elementIndex in elements) {
                        if (elements[elementIndex].checked) {
                            filterValue = elements[elementIndex].value;
                            break;
                        }
                    }
                }
                instanceFilters.push({propertyName: selector, value: filterValue});
            });
            return instanceFilters;
        },

        createSelectItemsFromItemset: function (itemset, labelRef, valueRef, xformModelJson) {
            var isDynamicLabel = (labelRef.indexOf('com.nexus.xlsfom.dynamic-itext') != -1);
            return $.map(itemset, function (item) {
                var finalItem = {label: '', value: ''};
                if (isDynamicLabel) {
                    finalItem.label = $.fn.buildXform.cascadingSelectHelper.lookupItext(item[labelRef.substr(labelRef.indexOf(':') + 1)], xformModelJson);
                } else {
                    finalItem.label = item[labelRef];
                }
                finalItem.value = item[valueRef];
                return finalItem;
            });
        },

        lookupItext: function (itextId, xformModelJson) {
            var translations = xformModelJson.model.itext.translation.text;
            var translation = $.grep(translations, function (entry) {
                return entry.id === itextId;
            });

            if (translation && translation.length > 0) {
                translation = translation[0].value;
            }
            return translation;
        },

        isCascadingSelect: function (element) {
            return element.type.match(/select.*/) && element.itemset;
        }
    };
}(jQuery));
