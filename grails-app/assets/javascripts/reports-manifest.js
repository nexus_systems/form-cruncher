//= require plugins/bootstrap-table.min
//= require plugins/bootstrap-select.min
//= require plugins/jquery-ui-sortable.min
//= require bootstrap3-editable/js/bootstrap-editable
//= require_self
$.fn.editable.defaults.mode = 'inline';