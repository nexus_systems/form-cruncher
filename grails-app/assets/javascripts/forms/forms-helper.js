/**
 * Created by ivan on 02/05/2017.
 */
var UploadHelper = function () {
    var
        initializeFileInput = function () {
            $("#uploadFile").fileinput({showCaption: false});
        },

        xformListUtil = {
            tableRef: '#xform-list',
            initTable: function () {
                $(xformListUtil.tableRef).bootstrapTable({
                    detailFormatter: xformListUtil.rowDetailsFormatter
                });

                //initialize popConfirm on expansion to view details of a given row.
                $(xformListUtil.tableRef).on('expand-row.bs.table', function (index, row, detail) {
                    xformListUtil.initPopConfirm(detail.id);
                });
            },
            rowDetailsFormatter: function (index, row) {
                var hbsTemplate = Handlebars.partials['xform-row-detail-view'];
                return hbsTemplate(row);
            },
            initPopConfirm: function (xformId) {
                $("[data-toggle='confirm-delete-xform-" + xformId + "']").popConfirm({
                    title: "confirm deletion",
                    content: "\<div class='alert alert-warning'>Deleting this xform might affect some of your reports.</div>" +
                    " Are you sure you want to delete this Xform?",
                    placement: "right"
                });
            }
        },

        xlsFormListUtil = {
            tableRef: '#xls-form-list',
            initTable: function () {
                $(xlsFormListUtil.tableRef).bootstrapTable({
                    data: xlsFormListUtil.generateTableData(),
                    striped: true,
                    detailView: true,
                    detailFormatter: xlsFormListUtil.rowDetailsFormatter
                });

                //initialize popConfirm on expansion to view details of a given row.
                $(xlsFormListUtil.tableRef).on('expand-row.bs.table', function (index, row, detail) {
                    xlsFormListUtil.initPopConfirm(detail.id);
                });
            },
            rowDetailsFormatter: function (index, row) {
                var hbsTemplate = Handlebars.partials['xls-form-row-detail-view'];
                return hbsTemplate(row);
            },
            generateTableData: function () {
                return $(xlsFormListUtil.tableRef).data('formList')
            },
            initPopConfirm: function (uploadId) {
                $("[data-toggle='confirm-delete-xls-upload-" + uploadId + "']").popConfirm({
                    title: "confirm deletion",
                    content: "Are you sure you want to delete this upload?",
                    placement: "right"
                });
            }
        },

        initAll = function () {
            initializeFileInput();
            xlsFormListUtil.initTable();
            xformListUtil.initTable()
        };

    return {
        initAll: initAll,
        recentUploadsUtil: xlsFormListUtil,
        initializeFileInput: initializeFileInput,
        xformListUtil: xformListUtil
    }
};

