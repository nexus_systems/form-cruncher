//= require plugins/bootstrap-table.min
//= require bootstrap-fileinput/js/plugins/purify
//= require bootstrap-fileinput/js/fileinput
//= require bootstrap-fileinput/themes/fa/theme
//= require forms/forms-helper
//= require_self

$(function () {
    new UploadHelper().initAll();
});
