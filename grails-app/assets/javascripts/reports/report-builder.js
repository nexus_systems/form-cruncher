/**
 * Created by ivan on 10/01/2016.
 */
var ReportBuilder = function () {
    var
        reportJsonModel = {},
        allFormsJsonModel = {},
        forms = {},
        allFormsXmlAsJson = {},
        displayablesMap = {},
        conditionsHolder = $("#conditions-holder"),
        addConditionSelector = ".add-condition-trigger",
        removeConditionSelector = ".remove-condition",
        conditionOperators = $("#conditionOperators").data('value'),
        conditionLinkers = $("#conditionLinkers").data('value'),
        /**
         * Initializes variables holding json models used when creating/editing a report.
         */
        initVariables = function () {
            allFormsJsonModel = jsonUtils.parseJson($('#report-creation-config').attr('data-value'));
            $.each(allFormsJsonModel, function (index, value) {
                value.value = value.xlsId;
                value.text = value.title;
            });
            var rawAllFormsXmlAsJson = jsonUtils.parseJson($('#allFormsXlsIdModelMap').attr('data-value'));
            for (var formXlsId in rawAllFormsXmlAsJson) {
                allFormsXmlAsJson[formXlsId] = jsonUtils.parseJson(rawAllFormsXmlAsJson[formXlsId])
            }
            reportJsonModel = jsonUtils.parseJson($("#saveReportDetails").attr('data-value'));
            populateForms();
        },

        /**
         * Creates an array mapping form <tt>xlsId</tt> to the json associated with the them. i.e the forms json model.
         */
        populateForms = function () {
            $.each(allFormsJsonModel, function (index, form) {
                forms[form.xlsId] = form;
            });
        },

        ConditionUtil = {
            conditionPropertyValueInput: function (propertyName, conditionIndex) {
                var selector;
                switch (propertyName) {
                    case 'formXlsId':
                        selector = '#conditions\\\[' + conditionIndex + '\\\]\\\.formXlsId';
                        break;
                    case 'questionKey':
                        selector = '#conditions\\\[' + conditionIndex + '\\\]\\\.questionKey';
                        break;
                    case 'operator':
                        selector = '#conditions\\\[' + conditionIndex + '\\\]\\\.operator';
                        break;
                    case 'operand':
                        selector = '#conditions\\\[' + conditionIndex + '\\\]\\\.operand';
                        break;
                }
                return $(selector)
            },

            formQuestions: function (formXlsId) {
                var form = forms[formXlsId], questions = [];

                $.each(form.displayables, function (index, displayable) {
                    createQuestionFromDisplayable(displayable, questions, formXlsId);
                });
                return questions;
            },

            resetEditable: function (editable, valueInput) {
                editable.editable('destroy');
                editable.text(editable.data('emptytext'));
                valueInput.val('');
                valueInput.valid();
            },

            setSelectedValues: function (valueInput, editable) {
                var selectedValue = valueInput.val();
                if (selectedValue) {
                    editable.editable('setValue', selectedValue, true);
                    editable.editable('submit', {selectedValue: selectedValue});
                }
            },

            createConditionElement: function (opts) {
                var options = $.extend({condition: null, upperElement: null, conditionLinker: null}, opts),
                    template = handlebarsUtils.templates['report/report-condition'],
                    conditionLinkerTemplate = handlebarsUtils.templates['report/report-condition-linker'],
                    conditionIndex = Handlebars.helpers.getNextDistinctIdForItem('report-condition'),
                    model = $.extend({index: conditionIndex, forms: allFormsJsonModel}, options.condition);

                // showing a condition in an already existing report
                if (options.condition) {
                    if (options.conditionLinker) {
                        conditionLinkerIndex = Handlebars.helpers.getNextDistinctIdForItem('report-condition-linker');
                        conditionLinkerTemplateMode = {index: conditionLinkerIndex, value: options.conditionLinker};
                        ConditionUtil.attachNewConditionAndLinkerBlocksAfterCondition(null, template(model),
                            conditionLinkerTemplate(conditionLinkerTemplateMode))
                    } else {
                        // it is the first condition
                        conditionsHolder.append(template(model));
                    }
                }
                else {
                    if (options.upperElement) {
                        var conditionLinkerIndex = Handlebars.helpers.getNextDistinctIdForItem('report-condition-linker'),
                            conditionLinkerTemplateMode = {index: conditionLinkerIndex};
                        ConditionUtil.attachNewConditionAndLinkerBlocksAfterCondition(options.upperElement, template(model),
                            conditionLinkerTemplate(conditionLinkerTemplateMode))
                    } else {
                        conditionsHolder.append(template(model));
                    }
                }
                ConditionUtil.initializeFormSelector({conditionIndex: conditionIndex});
                $('.selectpicker').selectpicker();
            },

            attachNewConditionAndLinkerBlocksAfterCondition: function (element, newCondition, conditionLinker) {
                var linker = $(conditionLinker);
                if (element) {
                    linker.hide().insertAfter(element).show("slow");
                    $(newCondition).hide().insertAfter(linker).show("slow");
                } else {
                    conditionsHolder.append(newCondition).append(linker);
                }
            },

            initializeFormSelector: function (options) {
                var editable = $('#conditions_{conditionIndex}_formXlsId_label'.format({conditionIndex: options.conditionIndex})),
                    valueInput = ConditionUtil.conditionPropertyValueInput("formXlsId", options.conditionIndex);

                editable.editable({
                    source: allFormsJsonModel,
                    success: function (response, newValue) {
                        var formXlsd, loadingForEdit = false;

                        if (typeof newValue === 'string' || newValue instanceof String) {
                            formXlsd = newValue;
                        } else {
                            formXlsd = newValue.selectedValue;
                            loadingForEdit = true;
                        }

                        valueInput.val(formXlsd);
                        valueInput.valid();

                        ConditionUtil.initializeQuestionSelector({
                            conditionIndex: options.conditionIndex,
                            formXlsId: formXlsd,
                            loadingForEdit: loadingForEdit
                        })
                    }
                });

                // if it's edit mode
                ConditionUtil.setSelectedValues(valueInput, editable);
            },
            initializeQuestionSelector: function (options) {
                var questionList = ConditionUtil.formQuestions(options.formXlsId),
                    editable = $('#conditions_{conditionIndex}_questionKey_label'.format({conditionIndex: options.conditionIndex})),
                    valueInput = ConditionUtil.conditionPropertyValueInput("questionKey", options.conditionIndex);

                if (!options.loadingForEdit) {
                    ConditionUtil.resetEditable(editable, valueInput);
                }

                editable.editable({
                    source: questionList,
                    success: function (response, newValue) {
                        var questionKey, loadingForEdit = false;

                        if (typeof newValue === 'string' || newValue instanceof String) {
                            questionKey = newValue;
                        } else {
                            questionKey = newValue.selectedValue;
                            loadingForEdit = true;
                        }

                        valueInput.val(questionKey);
                        valueInput.valid();

                        ConditionUtil.initializeConditionSelector({
                            conditionIndex: options.conditionIndex,
                            selectedQuestionKey: questionKey,
                            formXlsId: options.formXlsId,
                            loadingForEdit: loadingForEdit,
                            questionList: questionList
                        })
                    }
                });

                // if it's edit mode
                ConditionUtil.setSelectedValues(valueInput, editable);
            },
            initializeConditionSelector: function (options) {
                var selectedQuestion = null,
                    editable = $('#conditions_{conditionIndex}_operator_label'.format({conditionIndex: options.conditionIndex})),
                    valueInput = ConditionUtil.conditionPropertyValueInput("operator", options.conditionIndex);

                if (!options.loadingForEdit) {
                    ConditionUtil.resetEditable(editable, valueInput);
                }

                $.each(options.questionList, function (index, question) {
                    if (question.value === options.selectedQuestionKey) {
                        selectedQuestion = question
                    }
                });
                editable.editable({
                    source: ConditionUtil.applicableOperators(selectedQuestion),
                    success: function (response, newValue) {
                        var condition, loadingForEdit;
                        if (typeof newValue === 'string' || newValue instanceof String) {
                            condition = newValue;
                        } else {
                            condition = newValue.selectedValue;
                            loadingForEdit = true;
                        }

                        valueInput.val(condition);
                        valueInput.valid();

                        ConditionUtil.intializeValueInput({
                            formXlsId: options.formXlsId,
                            conditionIndex: options.conditionIndex,
                            loadingForEdit: loadingForEdit,
                            selectedQuestion: selectedQuestion
                        })
                    }
                });

                ConditionUtil.setSelectedValues(valueInput, editable);
            },
            intializeValueInput: function (options) {
                var items, type,
                    displayable = displayablesMap[options.formXlsId + '_' + options.selectedQuestion.value],
                    editable = $('#conditions_{conditionIndex}_operand_label'.format({conditionIndex: options.conditionIndex})),
                    valueInput = ConditionUtil.conditionPropertyValueInput("operand", options.conditionIndex);

                if ($.fn.buildXform.cascadingSelectHelper.isCascadingSelect(displayable)) {
                    items = getAllInstanceOptionsForCascadingSelect(displayable, options.formXlsId, allFormsXmlAsJson);
                } else if (displayable.type.match(/select.*/) && displayable.items) {
                    items = [];
                    $.each(displayable.items, function (index, item) {
                        items.push({text: item.label, value: item.value});
                    });
                }

                if (items && items.length > 0) {
                    type = 'select';
                    if (!options.loadingForEdit) {
                        ConditionUtil.resetEditable(editable, valueInput);
                    }
                } else {
                    type = 'text';
                }

                editable.editable({
                    source: items,
                    type: type,
                    success: function (response, newValue) {
                        var valueToSet = (typeof newValue === 'string' || newValue instanceof String)
                            ? newValue : newValue.selectedValue;

                        valueInput.val(valueToSet);
                        valueInput.valid();
                    }
                });

                ConditionUtil.setSelectedValues(valueInput, editable);
            },
            applicableOperators: function (question) {
                var applicableOptions;
                switch (question.type) {
                    case 'decimal':
                        applicableOptions = conditionOperators.decimal;
                        break;
                    case 'integer':
                        applicableOptions = conditionOperators.integer;
                        break;
                    default:
                        applicableOptions = conditionOperators.text;
                }
                return applicableOptions;
            }
        },

        /**
         *
         */
        attachListeners = function () {
            attachAddConditionListener();
            attachRemoveConditionListener();
            attachOnSubmitListener();
        },

        /**
         * Submission handler or creating/updating a report.
         */
        attachOnSubmitListener = function () {
            $('form').validate({
                debug: true,
                submitHandler: submissionHandler, ignore: [],
                errorPlacement: function (errorMessage, element) {
                    if (element.parent().hasClass('report-element')) {
                        $(element.parent()).parent().find('.report-element-validation-errors').append(errorMessage)
                    } else {
                        errorMessage.insertAfter(element);
                    }
                }
            });
        },

        submissionHandler = function () {
            var model = formSubmissionUtil.createModelObject('form'),
                saveUrl = $('form').attr('action'),
                action = $('#submit').val();
            $.ajax({
                type: 'post',
                url: saveUrl,
                data: JSON.stringify(model),
                contentType: "application/json",
                headers: {
                    Accept: "application/json;charset=utf-8"
                },
                success: function (data) {
                    var successMessage = 'report successfully {0}'.format(action === 'update' ? 'updated' : 'created');
                    handleFormSubmissionSucceeded(successMessage, action !== 'update');
                },
                statusCode: {
                    422: function (args) {
                        handleFormSubmissionErrors(args.responseJSON);
                    }
                }
            });
        },

        /**
         * Handles successful submission of the report to the server. It will show a notification and reset the form.
         * @param message the sucess message to show.
         */
        handleFormSubmissionSucceeded = function (message, reset) {
            var handlbarTemplate = Handlebars.partials['notification-partial'],
                notificationsHolder = $('.notifications-holder');

            notificationsHolder.html(handlbarTemplate({'message': message}));
            if (reset) {
                $('form').trigger('reset');
            }
            page_utils.scrollToLocation($(".report-context-nav"));
        },

        /**
         * Handler for showing validation errors resulting from attempts to create a report.
         * @param errors json object containing an array of errors to show. Typically, the objects will have the property
         * <tt>message</tt>
         */
        handleFormSubmissionErrors = function (errors) {
            var handlbarTemplate = Handlebars.partials['notification-partial'],
                notificationsHolder = $('.notifications-holder');

            notificationsHolder.html('');
            $.each(errors, function (index, error) {
                notificationsHolder.append(handlbarTemplate({'error': error.message}));
            });
            page_utils.scrollToLocation($(".report-context-nav"));
        },

        /**
         *
         */
        attachAddConditionListener = function () {
            $(conditionsHolder).on('click', addConditionSelector, function (event) {
                var upperElement = $(event.target).closest('.report-condition');
                ConditionUtil.createConditionElement({upperElement: upperElement});
            });
        },

        /**
         *
         */
        attachRemoveConditionListener = function () {
            $(conditionsHolder).on('click', removeConditionSelector, function (event) {
                var condition = $(event.target).closest('.report-condition');
                removeCondition(condition);
            });
        },

        /**
         *
         * @param conditionBlock
         */
        removeCondition = function (conditionBlock) {
            if ($('.report-condition').length > 1) {
                var conditionLinkerBlock = conditionBlock.prev('div.report-condition-linker');
                if (!conditionLinkerBlock.length > 0) {
                    conditionLinkerBlock = conditionBlock.next('div.report-condition-linker');
                }
                conditionLinkerBlock.slideUp('slow', function () {
                    $(this).remove();
                });
                conditionBlock.slideUp('slow', function () {
                    $(this).remove();
                });
            }
        },

        /**
         *
         * @param displayables
         * @param formXlsId
         * @returns {Array}
         */
        generateQuestionsFromDisplayables = function (displayables, formXlsId) {
            var questions = [];
            $.each(displayables, function (index, displayable) {
                createQuestionFromDisplayable(displayable, questions, formXlsId);
            });
            return questions;
        },

        /**
         *
         * @param displayable
         * @param questionsArray
         * @param formXlsId
         */
        createQuestionFromDisplayable = function (displayable, questionsArray, formXlsId) {
            if (displayable.displayables) {
                $.each(displayable.displayables, function (index, childDisplayable) {
                    createQuestionFromDisplayable(childDisplayable, questionsArray, formXlsId);
                });
            } else {
                displayablesMap[formXlsId + '_' + displayable.name] = displayable;
                displayable.text = displayable.label ? displayable.label : displayable.name;
                displayable.value = displayable.name;
                questionsArray.push(displayable);
            }
        },

        /**
         *
         * @param displayable
         * @param formXlsId
         * @param allFormsXmlAsJson
         * @returns {*}
         */
        getAllInstanceOptionsForCascadingSelect = function (displayable, formXlsId, allFormsXmlAsJson) {
            var labelRef = displayable.itemset.labelRef,
                valueRef = displayable.itemset.valueRef,
                instanceRef = displayable.itemset.instanceRef,
                xformModelJson = allFormsXmlAsJson[formXlsId],
                instances = xformModelJson.model.instance,
                instance = $.grep(instances, function (entry) {
                    return entry.id === instanceRef;
                });

            if (instance && instance.length > 0) {
                instance = instance[0];
            }
            return $.fn.buildXform.cascadingSelectHelper.createSelectItemsFromItemset(instance.root.item, labelRef, valueRef, xformModelJson);
        },

        formSubmissionUtil = {
            collectionItemIdentifier: '__itemIndex__',

            /**
             * Create an object model of the entire form components that can be submitted to the server.
             * @param formSelector the selector of the form whose components are to be got.
             * @returns {{Object}} representation of the form elements.
             */
            createModelObject: function (formSelector) {
                var model = {};
                $.each($(formSelector).serializeArray(), function (index, element) {
                    var name = element.name,
                        value = element.value,
                        matches = name.match(/(.*)\[(\d*)]\.(.*)/);
                    if (matches) {
                        var collectionName = matches[1],
                            itemIndex = matches[2],
                            propertyName = matches[3],
                            item = formSubmissionUtil.extractItem(itemIndex, collectionName, model);
                        item[propertyName] = value;
                        item[formSubmissionUtil.collectionItemIdentifier] = itemIndex;
                        model[collectionName].push(item);
                    } else if ((matches = name.match(/(.*)\[(\d*)]/))) {
                        collectionName = matches[1];
                        itemIndex = matches[2];
                        item = formSubmissionUtil.extractItem(itemIndex, collectionName, model);
                        if (item[formSubmissionUtil.collectionItemIdentifier]) {
                            model[collectionName].push(item);
                            console.error("can not overwrite already stored model item " + JSON.stringify(item));
                        } else {
                            model[collectionName].push(value)
                        }
                    }
                    else {
                        model[name] = value;
                    }
                });
                return model;
            },

            /**
             * Extracts an <tt>item</tt> from the given <tt>model</tt>.
             * @param itemIndex id of the item
             * @param collectionName the name of the collection containing the item
             * @param model the model containing the entire collection.
             * @returns {Object} item if found otherwise return an empty object.
             */
            extractItem: function (itemIndex, collectionName, model) {
                var item = {};
                var items = model[collectionName];
                if (!items) {
                    model[collectionName] = [];
                } else {
                    for (var index = 0; index < items.length; index++) {
                        if (items[index][formSubmissionUtil.collectionItemIdentifier] === itemIndex) {
                            item = items.splice(index, 1)[0];
                            break;
                        }
                    }
                }
                return item;
            }
        },

        /**
         * Contains utility methods for managing columns that will be displayed when the viewing the report.
         *
         * @type {{Object}}
         */
        DisplayColumnsUtil = {
            columnCreatorHolderSelector: '#report-column-creator-holder',
            addedColumnsHolderSelector: '#report-display-cols',

            initializeFormSelector: function () {
                $('#display-col-forms').editable({
                    source: allFormsJsonModel,
                    success: function (response, newValue) {
                        var formXlsd = (typeof newValue === 'string' || newValue instanceof String)
                                ? newValue : newValue.selectedValue,
                            valueInput = $("#col-form");

                        valueInput.val(formXlsd);

                        DisplayColumnsUtil.initializeQuestionSelector({formXlsId: formXlsd})
                    }
                });
            },

            initializeQuestionSelector: function (options) {
                var questionList = ConditionUtil.formQuestions(options.formXlsId);

                $('#display-col-questions').editable({
                    source: questionList,
                    success: function (response, newValue) {
                        var question = (typeof newValue === 'string' || newValue instanceof String)
                                ? newValue : newValue.selectedValue,
                            valueInput = $("#col-question"),
                            displayName = $("#col-displayName");

                        valueInput.val(question);

                        DisplayColumnsUtil.intializeDisplayNameInput(DisplayColumnsUtil.generateDisplayNameSuggestion(question));
                    }
                });
            },

            intializeDisplayNameInput: function (defaultValue) {
                $('#display-col-display-name').editable({
                    defaultValue: defaultValue,
                    success: function (response, newValue) {
                        var question = (typeof newValue === 'string' || newValue instanceof String)
                                ? newValue : newValue.selectedValue,
                            displayName = $("#col-displayName");


                        displayName.val(DisplayColumnsUtil.generateDisplayNameSuggestion(question));
                    }
                });
            },

            /**
             * Initializes all the functionality need for adding of columns to take place.
             * These include, adding and removing of report columns.
             */
            initAddColumnContainer: function () {
                var hbsTemplate = handlebarsUtils.templates['report/report-column-creator'],
                    columnsHolder = $(this.columnCreatorHolderSelector),
                    model = {forms: allFormsJsonModel};

                columnsHolder.html(hbsTemplate(model));
                this.attachOnFormChangeLister();
                this.attachAddColumnListener();
                this.attachRemoveColumnLister();
                this.attachColumnQuestionChangeLister();
                $('#col-form').trigger('change');
                $('#col-question').trigger('change');
            },

            /**
             * Attaches handler for resetting the questions when the form changes. This happens
             * in the processing of adding new columns to be displayed.
             */
            attachOnFormChangeLister: function () {
                $(this.columnCreatorHolderSelector).on('change', '#col-form', function () {
                    DisplayColumnsUtil.resetQuestions(this.value);
                });
            },

            /**
             * Attaches handler for enabling addition of a new display column to the list of added display columns for
             * the report.
             */
            attachAddColumnListener: function () {
                var displayColumnName = $("#col-displayName");
                $(this.columnCreatorHolderSelector).on('click', '#add-col-button', function () {
                    if (displayColumnName.val().length === 0) {
                        DisplayColumnsUtil.toggleDisplayNameValidationError(true);
                        displayColumnName.focus();
                    } else {
                        DisplayColumnsUtil.addColumn({});
                        $("#col-displayName-error").remove();
                    }
                });
            },

            /**
             * Helper method for showing and hiding validation errors when the display name for the column to be added is
             * empty.
             * @param show boolean flag to indicate whether to show the validation error. true for yes, false for no.
             */
            toggleDisplayNameValidationError: function (show) {
                var error = $("#col-displayName-error");
                if (show) {
                    if (error.length === 0) {
                        $("#col-displayName").after('<label id="col-displayName-error" class="error" for="col-displayName">' +
                            'The display name for the column is required.</label>');
                    }
                } else {
                    error.remove();
                }
            },

            /**
             * Attaches a handler for suggesting a column display name every time the selected question changes.
             */
            attachColumnQuestionChangeLister: function () {
                $(this.columnCreatorHolderSelector).on('change', '#col-question', function () {
                    var selectColumn = this.value;
                    if (selectColumn) {
                        selectColumn = DisplayColumnsUtil.generateDisplayNameSuggestion(selectColumn);
                        $('#col-displayName').val(selectColumn);
                        DisplayColumnsUtil.toggleDisplayNameValidationError(false);
                    }
                });
            },

            /**
             * Capitalizes the first character and replaces all underscores with spaces in the given column display name.
             * @param text the text of the column display name.
             * @returns {string} the formatted column display name.
             */
            generateDisplayNameSuggestion: function (text) {
                var capitalizedText = text.toLowerCase().replace(/\b./g, function (a) {
                    return a.toUpperCase();
                });
                return capitalizedText.replace(/_/g, ' ');
            },

            /**
             * Attaches the handler for removing a column from the list of display columns for the report.
             */
            attachRemoveColumnLister: function () {
                $(this.addedColumnsHolderSelector).on('click', '.remove-col-indicator', function () {
                    $(this).closest('.report-display-col').remove();
                    return false;
                });
            },

            /**
             * Resets the select containing the possible questions to be displayed in the report.
             * @param formXlsId the <tt>xlsId</tt> of the form whose questions are to be shown in the available
             * questions options.
             */
            resetQuestions: function (formXlsId) {
                var questionsDropDown = $("#col-question"),
                    hbsTemplate = Handlebars.partials['question-items-partial'],
                    form = forms[formXlsId];
                if (form) {
                    var model = {questions: generateQuestionsFromDisplayables(form.displayables, formXlsId)};
                    questionsDropDown.html(hbsTemplate(model));
                }
            },

            /**
             * Adds a display column to the list of available display columns for a given report. Can be used for adding
             * both a brand new display column and also just displaying an already available display column to the <tt>DOM</tt>.
             * @param {Object} displayColumn optional object representing an already existing display column. Usually
             * available if adding to the <tt>DOM</tt> an already existing <tt>displayColumn</tt>.
             */
            addColumn: function (displayColumn) {
                var question = $("#col-question"),
                    displayName = $("#col-displayName"),
                    hbsTemplate = Handlebars.partials['report-display-col-partial'],
                    addedColumnsHolder = $(this.addedColumnsHolderSelector),
                    columnIndex = Handlebars.helpers.getNextDistinctIdForItem('report-display-column'),
                    model = {question: question.val(), displayName: displayName.val(), index: columnIndex};

                $.extend(model, displayColumn);
                addedColumnsHolder.append(hbsTemplate(model));
                question.prop('selectedIndex', 0);
                question.trigger('change');
            },

            /**
             * Initializes jquery-ui sortable on the container of the added display columns to make them re-arrangeable.
             */
            initColumnSorter: function () {
                $(this.addedColumnsHolderSelector).sortable({cursor: "move"}).disableSelection();
            },

            /**
             * Adds already existing display columns to the <tt>DOM</tt>
             */
            initExistingReportColumns: function () {
                if (reportJsonModel.displayColumns) {
                    $.each(reportJsonModel.displayColumns, function (index, displayColumn) {
                        DisplayColumnsUtil.addColumn(displayColumn);
                    });
                }
            },

            /**
             * Initializes add column functionality in general.
             */
            init: function () {
                this.initColumnSorter();
                this.initAddColumnContainer();
                this.initializeFormSelector();
                this.initExistingReportColumns();
            }
        },

        init = function () {
            initVariables();
            if (reportJsonModel.conditions && reportJsonModel.conditions.length > 0) {
                $.each(reportJsonModel.conditions, function (index, condition) {
                    ConditionUtil.createConditionElement({
                        condition: condition,
                        conditionLinker: reportJsonModel.conditionLinkers[index]
                    });
                });
            } else {
                ConditionUtil.createConditionElement({})
            }
            attachListeners();
            DisplayColumnsUtil.init();
        };

    return {
        init: init
    }
};

var reportBuilder;
$(function () {
    reportBuilder = new ReportBuilder();
    reportBuilder.init();
    //TODO move these to correct place after.
});