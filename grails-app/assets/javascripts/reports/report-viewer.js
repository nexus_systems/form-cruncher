/**
 * Created by ivan on 20/02/2016.
 */
var ReportViewer = function () {
    var
        reportsTable = $('#reports-list'),

        /**
         * Used in tables listing reports to format the details of a given report.
         * Basically returns the detailed view for a given report column.
         * @param index the in table index of the report.
         * @param row the details of the report to be formatted.
         */
        reportDetailsFormatter = function (index, row) {
            var hbsTemplate = Handlebars.partials['report-detailed-view-partial'];
            return hbsTemplate(row);
        },

        /**
         * Initializes the popover that is shown when the delete report button is clicked.
         */
        initPopConfirm = function (reportId) {
            $("[data-toggle='confirm-delete-report-"+reportId+"']").popConfirm({
                title: "confirm deletion",
                content: "Are you sure you want to delete this report?",
                placement: "right"
            });
        },

        /**
         * Initializes handlers for the various components in the detail view of the report.
         * One such thing is initializing popConfirm on the delete report buttons.
         */
        intializeReportTableComponentHandlers = function () {
            //initialize popConfirm on expansion to view details of a given report.
            reportsTable.on('expand-row.bs.table', function (index, row, detail) {
                initPopConfirm(detail.id);
            });
        },

        init = function () {
            intializeReportTableComponentHandlers();
        };

    init();
    return {
        reportDetailsFormatter: reportDetailsFormatter
    }
};

var reportViewer;
$(function () {
    reportViewer = new ReportViewer();
});