/**
 * Created by ivan on 20/02/2016.
 */
var ReportSubmissionsUtil = function () {

    var submissionsTable = $("#submissions-table"),
        intializeSubmissions = function () {
            submissionsTable.bootstrapTable({
                sidePagination: 'server',
                queryParams: function (params) {
                },
                ajaxOptions: {
                    url: '/swali/report/submissions/' + submissionsTable.attr('data-report-id') + '.json',
                    method: 'get',
                    contentType: 'application/json'
                },
                ajax: function (request) {
                    $.ajax(request);
                },
                responseHandler: function (reportEntries) {
                    var rows = [];
                    $.each(reportEntries, function (index, entry) {
                        var row = {};
                        $.each(entry.submissions, function (index, submission) {
                            $.extend(row, submission)
                        });
                        rows.push(row)
                    });
                    return {
                        rows: rows
                    };
                }
            });
        },
        init = function () {
            intializeSubmissions();
        };

    init();
};

var reportSubmissionsUtil;
$(function () {
    reportSubmissionsUtil = new ReportSubmissionsUtil();
});