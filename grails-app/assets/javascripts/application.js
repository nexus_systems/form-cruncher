//= require jquery
//= require bootstrap
//= require handlebars-v4.0.5
//= require plugins/moment-with-locales
//= require plugins/bootstrap-datetimepicker
//= require plugins/jquery.validate
//= require plugins/jquery.serializejson.min
//= require plugins/jquery.popconfirm
//= require utils/handlebars-utils
//= require utils/string_utils
//= require utils/json_utils
//= require utils/page_utils
//= require xls-form/xls-form-builder
//= require_self
appContext = 'swali';

$(function () {
    $('[data-toggle="popover"]').popover();
    $('[data-toggle="tooltip"]').tooltip();
});