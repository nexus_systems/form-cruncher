/**
 * Created by ivan on 13/12/2015.
 */
var HandlebarsUtils = function () {
    "use strict";
    var templates = {},
        generatedDistinctIds = {},
        generateHandlebarsTemplates = function () {
            var template, path, namespace, name, elementObject;
            $('script[type="text/x-handlebars-template"]').each(function (index, element) {
                elementObject = $(element);
                template = Handlebars.compile(elementObject.html());
                name = elementObject.attr('data-name');
                namespace = elementObject.attr('data-namespace');
                path = namespace + "/" + name;
                if (elementObject.attr('data-partial') === "true") {
                    Handlebars.registerPartial(name, template);
                }
                templates[path] = template;
            });
        },

        registerCustomHelpers = function () {
            Handlebars.registerHelper("switch", function (value, options) {
                this._switch_value_ = value;
                var html = options.fn(this); // Process the body of the switch block
                delete this._switch_value_;
                return html;
            });

            Handlebars.registerHelper("case", function (value, options) {
                if (value == this._switch_value_) {
                    return options.fn(this);
                }
            });

            Handlebars.registerHelper('if_equals', function (a, b, opts) {
                return a === b ? opts.fn(this) : opts.inverse(this);
            });

            /**
             * Checks if a given <tt>item</tt> is contained in a given list of <tt>items</tt>
             * if the <tt>items</tt> argument is not an array, it will be assumed to be a string and split using commas
             */
            Handlebars.registerHelper('if_in', function (item, items, opts) {
                var result = false;
                if (item && items) {
                    result = $.inArray(item, $.isArray(items) ? items : items.split(',')) !== -1
                }
                return result ? opts.fn(this) : opts.inverse(this);
            });
            Handlebars.registerHelper('determine_xls_form_element_partial', function (type, opts) {
                return type + '-partial';
            });
            Handlebars.registerHelper('getNextDistinctIdForItem', function (item, opts) {
                var nextValidId = 0;
                if (generatedDistinctIds[item]) {
                    nextValidId = generatedDistinctIds[item];
                    generatedDistinctIds[item]++;
                } else {
                    generatedDistinctIds[item] = 1
                }
                return nextValidId;
            });
        },

        readLastGeneratedDistinctIdFor = function (key) {
            var nextId = generatedDistinctIds[key];
            return nextId ? (nextId - 1) : 0;
        },

        init = function () {
            registerCustomHelpers();
            generateHandlebarsTemplates();
        };

    return {
        init: init,
        templates: templates,
        readLastGeneratedDistinctIdFor: readLastGeneratedDistinctIdFor
    }
};
var handlebarsUtils;
$(function () {
    handlebarsUtils = new HandlebarsUtils();
    handlebarsUtils.init()
});


