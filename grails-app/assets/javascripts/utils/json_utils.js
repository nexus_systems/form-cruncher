/**
 * Created by ivan on 13/03/2016.
 */
var jsonUtils = (function () {
    var
        parseJson = function (jsonString) {
            try {
                var jsonObject = JSON.parse(jsonString);

                // Handle non-exception-throwing cases:
                // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
                // but... JSON.parse(null) returns 'null', and typeof null === "object",
                // so we must check for that, too.
                if (jsonObject && typeof jsonObject === "object" && jsonObject !== null) {
                    return jsonObject;
                }
            }
            catch (exception) {
                console.warn("could not pass string: {0}, to json. Returning an empty object instead".format(jsonString, 'orone'))
            }
            return {};
        };

    return {
        parseJson: parseJson
    }
})();