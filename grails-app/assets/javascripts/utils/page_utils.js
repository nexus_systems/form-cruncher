/**
 * Created by ivan on 09/04/2016.
 */
var page_utils = (function () {

    var
        /**
         * Helper method for scrolling to specific location in the page.
         * @param location the jquery object specifying the location of the object to scroll to.
         */
        scrollToLocation = function (location) {
            $('html, body').animate({
                scrollTop: (location.offset().top - 70) //the 70 is to cater for the padding on the body in the css
            }, 500);
        };
    return {
        scrollToLocation: scrollToLocation
    }
})();