package com.nexusug.xlsform

import com.nexusug.xlsform.reporting.XformAnswer

class XformQuestion {
	boolean repeatQuestion = false
	String repeatName

	String question
	List<XformAnswer> answers
	int inRepeatIndex = 0

	// it is a list to cater for multi option questions.
	static hasMany = [answers : XformAnswer]
	static belongsTo = [submission :XformSubmission]
	static constraints = {
		repeatName nullable: true
	}

	@Override
	public String toString() {
		return "XformQuestion{" +
				"id=" + id +
				", repeatQuestion=" + repeatQuestion +
				", repeatName='" + repeatName + '\'' +
				", question='" + question + '\'' +
				", answers=" + answers +
				", version=" + version +
				'}';
	}
}
