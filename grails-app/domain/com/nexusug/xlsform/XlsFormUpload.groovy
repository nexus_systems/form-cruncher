package com.nexusug.xlsform

import grails.validation.Validateable
import org.springframework.web.multipart.MultipartFile

/**
 * Created by ivan on 02/05/2017.
 */
class XlsFormUpload {

	XlsFormConversionStatus uploadStatus
	Date dateCreated

	String fileName
	String uploadLocation
	String xformContentLocation
	String conversionReport
	boolean deleted

	Xform xform

	static constraints = {
		conversionReport nullable: true
		xformContentLocation nullable: true
		xform nullable: true
	}

	static mapping = {
		conversionReport type: 'text'
	}
}

@Validateable
class XlsFormUploadCommand {
	MultipartFile uploadFile
	Long xformId

	static constraints = {
		xformId nullable: true
		uploadFile validator: { val, obj ->
			if (val == null || val.empty) {
				return false
			}

			if (!['xls', 'xlsx'].any { val.originalFilename?.toLowerCase()?.endsWith(it) }) {
				return 'xlsFormUpload.uploadFile.type.unsupported.error'
			}
		}
	}
}


