package com.nexusug.xlsform.reporting

/**
 * Details of the various columns to display for a given {@link Report} <br/>
 *
 * Created by ivan on 23/01/2016.
 */
class DisplayColumn {
	String question
	String displayName

	static belongsTo = [report: Report]
	@Override
	public String toString() {
		return "DisplayColumn{" +
				"id=" + id +
				", question='" + question + '\'' +
				", displayName='" + displayName + '\'' +
				", version=" + version +
				'}';
	}
}
