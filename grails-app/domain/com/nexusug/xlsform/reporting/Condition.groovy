package com.nexusug.xlsform.reporting

import com.nexusug.xlsform.util.StringUtils

class Condition {
	String questionKey
	ConditionOperator operator
	String operand
	String formXlsId

	static constraints = {
		operator nullable: false, validator: { val, condition ->
			return val.canValidate(condition.operand) //todo return message specific to the condition operator, for example only numbers allowed
		}
	}

	static belongsTo = [report: Report]

	@Override
	public String toString() {
		return "Condition{" +
				"id=" + id +
				", questionKey='" + questionKey + '\'' +
				", operator=" + operator +
				", operand='" + operand + '\'' +
				", formXlsId='" + formXlsId + '\'' +
				", version=" + version +
				'}';
	}

	def beforeValidate() {
		operand = operand ? StringUtils.sanitizeExtraWhiteSpace(operand) : operand
	}
}
