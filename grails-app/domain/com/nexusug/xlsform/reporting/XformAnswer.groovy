package com.nexusug.xlsform.reporting

import com.nexusug.xlsform.XformQuestion

class XformAnswer {
	String value

	static belongsTo = [question:XformQuestion]
	static constraints = {
		value nullable: true
	}

	@Override
	public String toString() {
		return "XformAnswer{" +
				"id=" + id +
				", value='" + value + '\'' +
				", version=" + version +
				'}';
	}
}
