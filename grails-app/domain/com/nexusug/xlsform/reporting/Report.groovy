package com.nexusug.xlsform.reporting

class Report {
	String name
	String description
	boolean archived

	List<Condition> conditions
	List<ConditionLinker> conditionLinkers
	List<DisplayColumn> displayColumns
	String formLinkerQuestion

	static hasMany = [conditions: Condition, conditionLinkers: ConditionLinker, displayColumns: DisplayColumn]

	static constraints = {
		formLinkerQuestion nullable: true
		description nullable: true
		conditions nullable: false
		conditionLinkers nullable: true, validator: { val, report ->
			int conditionSize = report.conditions ? report.conditions.size() : 0
			int conditionLinkerSize = val ? val.size() : 0
			if (conditionSize > 1 && conditionSize - conditionLinkerSize != 1) {
				return 	['count.invalid']
			}
		}
	}

	static mapping = {
		version false
	}

	@Override
	public String toString() {
		return "Report{" +
				"id=" + id +
				", name='" + name + '\'' +
				", description='" + description + '\'' +
				", conditions=" + conditions +
				", conditionLinkers=" + conditionLinkers +
				", displayColumns=" + displayColumns +
				", formLinkerQuestion='" + formLinkerQuestion + '\'' +
				", version=" + version +
				'}';
	}
}
