package com.nexusug.xlsform

class Xform {
	String formXml
	String name
	String title
	String xlsId
	Date dateCreated
	Date lastUpdated
	boolean deleted

	static hasMany = [submissions:XformSubmission]

	static constraints = {
		xlsId unique: true
	}

	static mapping = {
		formXml type: 'text'
	}
}
