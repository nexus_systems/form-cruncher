package com.nexusug.xlsform

class XformSubmission {

	Date dateCreated
	Date lastUpdated
	boolean archived = false

	Map<String, String> metaData
	List<XformQuestion> questions

	static belongsTo = [xform: Xform]
	static hasMany = [questions: XformQuestion]
	static constraints = {
	}

	@Override
	public String toString() {
		return "XformSubmission{" +
				"id=" + id +
				", dateCreated=" + dateCreated +
				", lastUpdated=" + lastUpdated +
				", metaData=" + metaData +
				", version=" + version +
				", questions=" + questions +
				", xform=" + xform +
				'}';
	}
}
