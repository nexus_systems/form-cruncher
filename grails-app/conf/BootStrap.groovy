import com.nexusug.xlsform.FormStorageService
import grails.util.Environment

class BootStrap {

	FormStorageService formStorageService

	def init = { servletContext ->

		if (Environment.current == Environment.DEVELOPMENT) {
//			createSampleForms()
		}
	}

	private void createSampleForms() {
		String sampleFormsLocation = 'resources/sample_forms'
		URL directoryURL = this.class.getClassLoader().getResource(sampleFormsLocation);
		if (directoryURL != null && directoryURL.getProtocol().equals("file")) {
			String[] forms = new File(directoryURL.toURI()).list()
			String formXML
			forms.each { String formURI ->
				formXML = this.getClass().getResource(sampleFormsLocation + '/' + formURI).text
				formStorageService.createXform(formXML)
			}
		}
	}

	def destroy = {
	}
}
