package com.nexusug.xlsform

import com.nexusug.xlsform.reporting.XformAnswer
import grails.test.spock.IntegrationSpec

import static org.springframework.http.HttpStatus.*

class DataControllerISpec extends IntegrationSpec {

	DataController controller

	def setup() {
		controller = new DataController()
	}

	void 'update returns UNSUPPORTED_MEDIA_TYPE for unsupported content types'() {
		when:
			controller.request.contentType = 'application/x-www-form-urlencoded'
			controller.request.method = 'POST'
			controller.update()
		then:
			controller.response.status == UNSUPPORTED_MEDIA_TYPE.value()
	}

	void 'update correctly updates a submission'() {
		given:
			InputStream jsonPayload = DataServiceISpec.getClassLoader().getResourceAsStream('resources/sample_submission_update.json')
			def form = new Xform(name: 'cascading_select_test', formXml: 'some valid xml', title: 'test form',
					xlsId: 'cascading_select_test_1').save(failOnError: true)
			3.times {
				new XformSubmission(archived: false, xform: form).save(failOnError: true)
			}
			def submissionToUpdate = new XformSubmission(archived: false, xform: form).save(failOnError: true)
			String jsonUpload
			expect:
			XformSubmission.countByArchived(false) == 4
		when:
			jsonUpload = jsonPayload.text.replace('SUBMISSION_ID', ''+submissionToUpdate.id)
			controller.request.contentType = 'application/json'
			controller.request.method = 'POST'
			controller.request.content = jsonUpload.getBytes()
			controller.update()
		then:
			submissionToUpdate.archived
			XformSubmission.countByArchived(true) == 1
			XformSubmission.countByArchived(false) == 4
	}

	void 'delete correctly deletes a submission by archiving it'() {
		given:
			def form = new Xform(name: 'cascading_select_test', formXml: 'some valid xml', title: 'test form',
					xlsId: 'cascading_select_test_1').save(failOnError: true)
			3.times {
				new XformSubmission(archived: false, xform: form).save(failOnError: true)
			}
			def submissionToDelete = new XformSubmission(archived: false, xform: form).save(failOnError: true)
		expect:
			XformSubmission.countByArchived(false) == 4
			XformSubmission.count() == 4
		when:
			controller.params.id = submissionToDelete.id
			controller.delete()
		then:
			submissionToDelete.archived
			XformSubmission.countByArchived(true) == 1
			XformSubmission.countByArchived(false) == 3
	}

	void 'save can save a JSON submission'() {
		given:
			new Xform(name: 'cascading_select_test', formXml: 'some valid xml', title: 'test form',
					xlsId: 'cascading_select_test_1').save(failOnError: true)
		when:
			controller.request.contentType = 'application/json'
			controller.request.method = 'POST'
			controller.request.content = IntegrationTestUtils.readTestResourceAsStream(this.class, 'sample_submission.json').getBytes()
			controller.save()
		then:
			XformSubmission.count() == 1
			XformQuestion.countByRepeatQuestion(false) == 5
			XformQuestion.countByRepeatQuestion(true) == 8
			XformQuestion.countByRepeatQuestionAndRepeatNameAndInRepeatIndex(true, 'group_cascade',0 ) == 3
			XformQuestion.countByRepeatQuestionAndRepeatNameAndInRepeatIndex(true, 'group_cascade',1 ) == 3
			XformQuestion.countByRepeatQuestionAndRepeatNameAndInRepeatIndex(true, 'group_cascade_select', 0) == 2
			XformAnswer.countByQuestion(XformQuestion.findByQuestion('rp_state_select_2')) == 2
			XformAnswer.countByQuestion(XformQuestion.findByQuestion('rp_state_select')) == 2
			with(XformSubmission.list()[0].metaData) {
				phonenumber == '1'
				simserial == '2'
				subscriberid == '3'
				deviceid == '4'
				today == '2016-03-27T03:31:00.000+03'
				end == '2016-03-28T03:31:00.000+03'
				start == '2016-03-27T03:31:00.000+03'
				instanceID == 'uuid:dfee-fef-dsddd-aaa-ered'
			}
	}

	void 'save correctly handles XformNotFoundException'() {
		when:
			controller.request.contentType = 'application/json'
			controller.request.method = 'POST'
			controller.request.content = IntegrationTestUtils.readTestResourceAsStream(this.class, 'sample_submission.json').getBytes()
			controller.save()
		then:
			controller.response.status == BAD_REQUEST.value()
			controller.response.text == 'no form matched name: cascading_select_test_1'
	}

	void 'save correctly handles InvalidSubmission'() {
		when:
			controller.request.contentType = 'application/json'
			controller.request.method = 'POST'
			controller.request.json = [animal:'goat']
			controller.save()
		then:
			controller.response.status == BAD_REQUEST.value()
			controller.response.text == 'Required property \'xlsId\' is missing'
	}

	void 'save can save an XML submission'() {
		given:
			new Xform(name: 'cascading_select_test', formXml: 'some valid xml', title: 'test form',
					xlsId: 'sample').save(failOnError: true)
		when:
			controller.request.method = 'POST'
			controller.request.contentType = 'application/xml'
			controller.request.content = IntegrationTestUtils.readTestResourceAsStream(this.class, 'sample_submission.xml').getBytes()
			controller.save()
		then:
			XformSubmission.count() == 1
			XformQuestion.countByRepeatQuestion(false) == 5
			XformQuestion.countByRepeatQuestion(true) == 18
			XformQuestion.countByRepeatQuestionAndRepeatNameAndInRepeatIndex(true, 'repeat_test', 0) == 3
			XformQuestion.countByRepeatQuestionAndRepeatNameAndInRepeatIndex(true, 'repeat_test', 1) == 3
			XformQuestion.countByRepeatQuestionAndRepeatNameAndInRepeatIndex(true, 'repeat_test', 2) == 3
			XformQuestion.countByRepeatQuestionAndRepeatNameAndInRepeatIndex(true, 'repeat_test', 3) == 0
			XformQuestion.countByRepeatQuestionAndRepeatName(true, 'group_test') == 3
			XformQuestion.countByRepeatQuestionAndRepeatName(true, 'table_list_example') == 4
			XformQuestion.countByRepeatQuestionAndRepeatName(true, 'labeled_select_group') == 2
			XformAnswer.countByQuestion(XformQuestion.findByQuestion('list-nolabel_test')) == 1
			XformAnswer.countByQuestion(XformQuestion.findByQuestion('skipable_question')) == 1
			XformAnswer.findByQuestion(XformQuestion.findByQuestion('list-nolabel_test')).value == 'yes'
			XformAnswer.findByQuestion(XformQuestion.findByQuestion('skipable_question')).value == 'ETD'
			XformAnswer.findByQuestion(XformQuestion.findByQuestion('select_appearance_note')).value == null
			XformAnswer.findByQuestion(XformQuestion.findByQuestion('generated_table_list_label_21')).value == null
			with(XformSubmission.list()[0].metaData) {
				phonenumber == '1'
				simserial == '2'
				subscriberid == '3'
				deviceid == '4'
				today == '2016-03-27T03:31:00.000+03'
				end == '2016-03-28T03:31:00.000+03'
				start == '2016-03-27T03:31:00.000+03'
				instanceID == 'uuid:dfee-fef-dsddd-aaa-ered'
			}
	}
}
