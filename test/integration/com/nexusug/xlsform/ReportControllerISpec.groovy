package com.nexusug.xlsform

import com.nexusug.xlsform.reporting.Condition
import com.nexusug.xlsform.reporting.ConditionLinker
import com.nexusug.xlsform.reporting.ConditionOperator
import com.nexusug.xlsform.reporting.DisplayColumn
import com.nexusug.xlsform.reporting.Report
import grails.test.spock.IntegrationSpec

/**
 * Created by ivan on 07/02/2016.
 */
class ReportControllerISpec extends IntegrationSpec {
	ReportController controller

	def setup() {
		controller = new ReportController()
	}

	/**
	 * Exists to test that the gotcha documented here
	 * <a href="https://github.com/robfletcher/grails-gson#cascading-saves">cascading-saves gotcha</a> is well handled. <br />
	 * Do not change the way the associations are being assigned unless you are sure of what you are doing and are fully
	 * aware of the gotcha being talked about here.
	 */
	void 'can save a report'() {
		given:
			Report report = new Report()
			List<Condition> conditions = []
			List<DisplayColumn> columns = []
			List<ConditionLinker> conditionLinkers = []
			conditions.add(new Condition(questionKey: '_key_', operator: ConditionOperator.TEXT_EQUAL, operand: '_operand_',
					formXlsId: '_xlsId_'))
			conditionLinkers.add(ConditionLinker.AND)
			conditions.add(new Condition(questionKey: '_key2_', operator: ConditionOperator.TEXT_EQUAL, operand: '_operand2_',
					formXlsId: '_xlsId_'))
			columns.add(new DisplayColumn(question: '_question_', displayName: '_displayName_'))
			report.name = '_name_'
			report.description = '_description_'
			report.conditions = conditions
			report.displayColumns = columns
			report.conditionLinkers = conditionLinkers
		when:
			controller.saveOrUpdate(report)
		then:
			Report.count() == 1
			with(Report.list()[0]) {
				conditions.size() == 2
				conditionLinkers.size() == 1
			}
	}

	void 'can delete a report'() {
		given:
			Report report = new Report()
			report.addToConditions(new Condition(questionKey: '_key_', operator: ConditionOperator.TEXT_EQUAL,
					operand: '_operand_', formXlsId: '_xlsId_'))
			report.addToConditionLinkers(ConditionLinker.AND)
			report.addToConditions(new Condition(questionKey: '_key2_', operator: ConditionOperator.TEXT_EQUAL,
					operand: '_operand2_', formXlsId: '_xlsId_'))
			report.addToDisplayColumns(new DisplayColumn(question: '_question_', displayName: '_displayName_'))
			report.name = '_name_'
			report.description = '_description_'
			report.save(failOnError: true)
		expect:
			Report.count() == 1
		when:
			controller.delete(report)
		then:
			report.archived
	}
}
