package com.nexusug.xlsform

import com.nexusug.xlsform.exception.InvalidSubmission
import com.nexusug.xlsform.exception.XformNotFoundException
import com.nexusug.xlsform.exception.XformSubmissionNotFoundException
import com.nexusug.xlsform.reporting.XformAnswer
import grails.test.spock.IntegrationSpec
import groovy.json.JsonSlurper

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
class DataServiceISpec extends IntegrationSpec {

	DataService dataService

	void 'getSubmissions only returns submissions that have not been deleted by the user'() {
		given:
			Xform form = new Xform(formXml: '<test></test>', name: 'test form', title: 'testing', xlsId: 'test_1').save(failOnError: true)
			Xform form2 = new Xform(formXml: '<test></test>', name: 'test form', title: 'testing', xlsId: 'test_2').save(failOnError: true)
			3.times {
				new XformSubmission(archived: false, xform: form).save(failOnError: true)
			}
			2.times {
				new XformSubmission(archived: false, xform: form2).save(failOnError: true)
			}
			2.times {
				new XformSubmission(archived: true, xform: form).save(failOnError: true)
			}
			4.times {
				new XformSubmission(archived: true, xform: form2).save(failOnError: true)
			}
			List<XformSubmission> submissions
		when: 'no pagination properties'
			submissions = dataService.getSubmissions()
		then:
			submissions.every {!it.archived}
			submissions.size() == 5
		when: 'there is pagination'
			submissions = dataService.getSubmissions([max: 1])
		then:
			submissions.every {!it.archived}
			submissions.size() == 1
		when: 'there is pagination'
			submissions = dataService.getSubmissions([max: 10, offset: 3])
		then:
			submissions.every {!it.archived}
			submissions.size() == 2
		when: 'given a specific xlsId'
			submissions = dataService.getSubmissions('test_2')
		then: 'return only the submissions whose forms match the xlsId'
			submissions.size() == 2
			submissions.every {!it.archived}
	}

	void 'updateSubmission correctly updates a submission'() {
		given:
			InputStream jsonPayload = DataServiceISpec.getClassLoader().getResourceAsStream('resources/sample_submission_update.json')
			def form = new Xform(name: 'cascading_select_test', formXml: 'some valid xml', title: 'test form',
					xlsId: 'cascading_select_test_1').save(failOnError: true)
			3.times {
				new XformSubmission(archived: false, xform: form).save(failOnError: true)
			}
			def submissionToUpdate = new XformSubmission(archived: false, xform: form).save(failOnError: true)
			String jsonUpload
		expect:
			XformSubmission.countByArchived(false) == 4
		when:
			jsonUpload = jsonPayload.text.replace('SUBMISSION_ID', ''+submissionToUpdate.id)
		dataService.updateSubmission((Map)new JsonSlurper().parseText(jsonUpload))
		then:
			submissionToUpdate.archived
			XformSubmission.countByArchived(true) == 1
			XformSubmission.countByArchived(false) == 4
	}

	void 'updateSubmission does not allow updating of an archived submission'() {
		given:
			InputStream jsonPayload = DataServiceISpec.getClassLoader().getResourceAsStream('resources/sample_submission_update.json')
			def form = new Xform(name: 'cascading_select_test', formXml: 'some valid xml', title: 'test form',
					xlsId: 'cascading_select_test_1').save(failOnError: true)
			3.times {
				new XformSubmission(archived: false, xform: form).save(failOnError: true)
			}
			def submissionToUpdate = new XformSubmission(archived: true, xform: form).save(failOnError: true)
			String jsonUpload
		when: 'trying to update the same submission'
			jsonUpload = jsonPayload.text.replace('SUBMISSION_ID', ''+submissionToUpdate.id)
			dataService.updateSubmission((Map)new JsonSlurper().parseText(jsonUpload))
		then:
			thrown(XformSubmissionNotFoundException)
	}

	void 'updateSubmission does not allow updating of non existing submissions'() {
		given:
			InputStream jsonPayload = DataServiceISpec.getClassLoader().getResourceAsStream('resources/sample_submission_update.json')
			def form = new Xform(name: 'cascading_select_test', formXml: 'some valid xml', title: 'test form',
					xlsId: 'cascading_select_test_1').save(failOnError: true)
			3.times {
				new XformSubmission(archived: false, xform: form).save(failOnError: true)
			}
			def submissionToUpdate = new XformSubmission(archived: true, xform: form).save(failOnError: true)
			String jsonUpload
		when: 'trying to update the same submission'
			jsonUpload = jsonPayload.text.replace('SUBMISSION_ID', ''+submissionToUpdate.id)
			dataService.updateSubmission((Map)new JsonSlurper().parseText(jsonUpload))
		then:
			thrown(XformSubmissionNotFoundException)
	}

	void 'updateSubmission does not allow updating of submissions when no submission id is provided'() {
		given:
			InputStream jsonPayload = DataServiceISpec.getClassLoader().getResourceAsStream('resources/sample_submission.json')
			def form = new Xform(name: 'cascading_select_test', formXml: 'some valid xml', title: 'test form',
					xlsId: 'cascading_select_test_1').save(failOnError: true)
			3.times {
				new XformSubmission(archived: false, xform: form).save(failOnError: true)
			}
			def submissionToUpdate = new XformSubmission(archived: true, xform: form).save(failOnError: true)
			String jsonUpload
		when: 'trying to update the same submission'
			jsonUpload = jsonPayload.text.replace('SUBMISSION_ID', ''+submissionToUpdate.id)
			dataService.updateSubmission((Map)new JsonSlurper().parseText(jsonUpload))
		then:
			thrown(InvalidSubmission)
	}

	void 'captureXMLSubmission correctly parses and creates submissions from xmls data'() {
		given:
			InputStream xmlPayload = DataServiceISpec.getClassLoader().getResourceAsStream('resources/sample_submission.xml')
			new Xform(name: 'cascading_select_test', formXml: 'some valid xml', title: 'test form',
					xlsId: 'sample').save(failOnError: true)
		when:
			dataService.captureXMLSubmission(xmlPayload.text)
		then:
			XformSubmission.count() == 1
			XformQuestion.countByRepeatQuestion(false) == 5
			XformQuestion.countByRepeatQuestion(true) == 18
			XformQuestion.countByRepeatQuestionAndRepeatName(true, 'repeat_test') == 9
			XformQuestion.countByRepeatQuestionAndRepeatName(true, 'group_test') == 3
			XformQuestion.countByRepeatQuestionAndRepeatName(true, 'table_list_example') == 4
			XformQuestion.countByRepeatQuestionAndRepeatName(true, 'labeled_select_group') == 2
			XformAnswer.countByQuestion(XformQuestion.findByQuestion('list-nolabel_test')) == 1
			XformAnswer.countByQuestion(XformQuestion.findByQuestion('skipable_question')) == 1
			XformAnswer.findByQuestion(XformQuestion.findByQuestion('list-nolabel_test')).value == 'yes'
			XformAnswer.findByQuestion(XformQuestion.findByQuestion('skipable_question')).value == 'ETD'
			XformAnswer.findByQuestion(XformQuestion.findByQuestion('select_appearance_note')).value == null
			XformAnswer.findByQuestion(XformQuestion.findByQuestion('generated_table_list_label_21')).value == null
			with(XformSubmission.list()[0].metaData) {
				phonenumber == '1'
				simserial == '2'
				subscriberid == '3'
				deviceid == '4'
				today == '2016-03-27T03:31:00.000+03'
				end == '2016-03-28T03:31:00.000+03'
				start == '2016-03-27T03:31:00.000+03'
				instanceID == 'uuid:dfee-fef-dsddd-aaa-ered'
			}
	}

	void 'captureXMLSubmission throws InvalidSubmission is thrown when the xml payload does not have a form id specified'() {
		when:
			dataService.captureXMLSubmission('<sample_xlsform><some_text>Andre</some_text></sample_xlsform>')
		then:
			InvalidSubmission exception = thrown()
			exception.message == 'Invalid form id for the submission is missing.'
	}

	void 'captureXMLSubmission throws XformNotFoundException is thrown when no form matches the specified form id'() {
		when:
			dataService.captureXMLSubmission('<sample_xlsform id="fake_id"><some_text>Andre</some_text></sample_xlsform>')
		then:
			XformNotFoundException exception = thrown()
			exception.message == 'no form matched name: fake_id'
	}

	void 'captureJsonSubmission correctly captures xls-form data'() {
		given:
			InputStream xmlPayload = DataServiceISpec.getClassLoader().getResourceAsStream('resources/sample_submission.json')
			String jsonUpload = xmlPayload.text
			new Xform(name: 'cascading_select_test', formXml: 'some valid xml', title: 'test form',
					xlsId: 'cascading_select_test_1').save(failOnError: true)
		when:
			dataService.captureJsonSubmission(jsonUpload)
		then:
			XformSubmission.count() == 1
			XformQuestion.countByRepeatQuestion(false) == 5
			XformQuestion.countByRepeatQuestion(true) == 8
			XformQuestion.countByRepeatQuestionAndRepeatName(true, 'group_cascade') == 6
			XformQuestion.countByRepeatQuestionAndRepeatName(true, 'group_cascade_select') == 2
			XformAnswer.countByQuestion(XformQuestion.findByQuestion('rp_state_select_2')) == 2
			XformAnswer.countByQuestion(XformQuestion.findByQuestion('rp_state_select')) == 2
			with(XformSubmission.list()[0].metaData) {
				phonenumber == '1'
				simserial == '2'
				subscriberid == '3'
				deviceid == '4'
				today == '2016-03-27T03:31:00.000+03'
				end == '2016-03-28T03:31:00.000+03'
				start == '2016-03-27T03:31:00.000+03'
				instanceID == 'uuid:dfee-fef-dsddd-aaa-ered'
			}
	}

	void 'captureJsonSubmission throws InvalidSubmission when json does not have form id specified'() {
		when:
			dataService.captureJsonSubmission('{"property_x":"cascading_select_test_1", "state":"texas"}')
		then:
			thrown(InvalidSubmission)
	}

	void 'captureJsonSubmission throws XformNotFoundException when the specified form id does not match any existing form'() {
		when:
			dataService.captureJsonSubmission('{"xlsId":"cascading_select_test_1", "state":"texas"}')
		then:
			XformNotFoundException exception = thrown()
			exception.message == 'no form matched name: cascading_select_test_1'
	}
}
