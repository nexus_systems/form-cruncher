package com.nexusug.xlsform.reporting

import com.nexusug.xlsform.Xform
import com.nexusug.xlsform.XformQuestion
import com.nexusug.xlsform.XformSubmission
import grails.test.spock.IntegrationSpec

class ReportUtilsISpec extends IntegrationSpec {

    def setup() {
        createSampleSubmissions()
    }

    void 'evaluateSubmission correctly evaluates a submission against a condition'() {
        given:
            Condition condition
            Xform form = new Xform(name: 'createFormSubmissionTextForm', formXml: 'some valid xml', title: 'test form', xlsId:'myTest2')
            XformSubmission submission = new XformSubmission()
            [count : 'king', 'rp_state' : 'texasw', 'rp_cits': '2', 'sub_id': '12334'].each { key, answer ->
                submission.addToQuestions(createXformQuestion(key, answer))
            }
            XformQuestion repeatQuestion = createXformQuestion('repeat', 'yes')
            repeatQuestion.repeatQuestion = true
            repeatQuestion.repeatName = 'myRepeat'

            submission.addToQuestions(repeatQuestion)
            submission.xform = form
            boolean result
        when:
            condition = new Condition(questionKey:'rp_state', operator: ConditionOperator.TEXT_EQUAL, operand: 'TEXAS', formXlsId: 'myTest')
            result = ReportUtils.evaluateSubmission(condition, submission)
        then:
            !result
        when:
            condition = new Condition(questionKey:'rp_state', operator: ConditionOperator.TEXT_EQUAL, operand: 'TEXASW', formXlsId: 'myTest2')
            result = ReportUtils.evaluateSubmission(condition, submission)
        then:
            result
        when:
            condition = new Condition(questionKey:'non-existent', operator: ConditionOperator.TEXT_EQUAL, operand: 'texasw', formXlsId: 'myTest2')
            result = ReportUtils.evaluateSubmission(condition, submission)
        then:
            !result
        when: 'condition is in a repeat instead'
            condition = new Condition(questionKey:'repeat', operator: ConditionOperator.TEXT_EQUAL, operand: 'yes', formXlsId: 'myTest2')
            result = ReportUtils.evaluateSubmission(condition, submission)
        then:
            result
    }

    private static XformQuestion createXformQuestion(String question, String answer) {
        XformQuestion formQuestion = new XformQuestion(question: question)
        formQuestion.addToAnswers( new XformAnswer(value: answer))
        return formQuestion
    }

    /**
     * Helper method that created two forms and adds sample submissions for them.
     */
    private static void createSampleSubmissions() {
        def params = [county : 'king', 'rp_state' : 'texas', 'rp_cities': 'dumont', 'sub_id': '1233']
        Xform form = new Xform(name: 'createFormSubmissionTextForm', formXml: 'some valid xml', title: 'test form', xlsId:'myTest').save(failOnError: true)
        XformSubmission submission = new XformSubmission()
        params.each { key, answer ->
            submission.addToQuestions(createXformQuestion(key, answer))
        }
        submission.xform = form
        submission.save(failOnError: true)

        params = [count : 'king', 'rp_state' : 'texasw', 'rp_cits': '2', 'sub_id': '12334']
        form = new Xform(name: 'createFormSubmissionTextForm', formXml: 'some valid xml', title: 'test form', xlsId:'myTest2').save(failOnError: true)
        submission = new XformSubmission()
        params.each { key, answer ->
            submission.addToQuestions(createXformQuestion(key, answer))
        }

        XformQuestion repeatQuestion = createXformQuestion('repeat', 'yes')
        repeatQuestion.repeatQuestion = true
        repeatQuestion.repeatName = 'myRepeat'

        submission.addToQuestions(repeatQuestion)
        submission.xform = form
        submission.save(failOnError: true)
    }
}
