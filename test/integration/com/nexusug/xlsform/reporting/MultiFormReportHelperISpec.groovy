package com.nexusug.xlsform.reporting

import com.nexusug.xlsform.XformQuestion
import com.nexusug.xlsform.Xform
import com.nexusug.xlsform.XformSubmission
import grails.test.spock.IntegrationSpec
import spock.lang.Unroll

class MultiFormReportHelperISpec extends IntegrationSpec {

    def setup() {
        createSampleSubmissions()
    }

    @Unroll
    void 'extractSubmissionsBasedOnFormLinker correctly extracts related submissions'() {
        given:
            List<XformSubmission> submissions = []
            submissions.add(createSubmission([sub_id:'testing123', rp_state: 'testState'], 'myTest'))
            submissions.add(createSubmission([sub_id:'testing123', rp_cits: '2'], 'myTest2'))
        expect:
            expectedSubmissionsSize == MultiFormReportHelper.extractRelatedSubmissionsBasedOnFormLinker(formLinker, formLinkerValue, submissions).size()
        where:
            formLinker 	| formLinkerValue	| expectedSubmissionsSize
            'sub_id'	| ['testing123']	| 2
            'rp_cits'	| ['2']				| 1
            'rp_state'	| ['testState']		| 1
            'rp_state'	| null				| 0
    }

    void 'evaluateRelatedSubmissionList correctly evaluates submissions'() {
        given:
            List<Condition> conditions
            List<XformSubmission> relatedSubmissions
            List<ConditionLinker> conditionLinkers
            XformSubmission submission
            boolean currentEvaluationStatus
            boolean evaluationResult
        when: 'county == king || rp_cits == 2 && connectivity == excellent'
            relatedSubmissions = []
            currentEvaluationStatus = true
            submission = XformSubmission.findByXform(Xform.findByXlsId('myTest'))
            conditions = []
            conditions.add(new Condition(questionKey:'county', operator: ConditionOperator.TEXT_EQUAL, operand: 'king', formXlsId: 'myTest'))
            conditions.add(new Condition(questionKey:'rp_cits', operator: ConditionOperator.NUMERIC_EQUAL, operand: '2', formXlsId: 'myTest2'))
            conditions.add(new Condition(questionKey:'connectivity', operator: ConditionOperator.TEXT_EQUAL, operand: 'excellent', formXlsId: 'myTest2'))

            conditionLinkers = []
            conditionLinkers.add(ConditionLinker.AND)
            conditionLinkers.add(ConditionLinker.OR)
            conditionLinkers.add(ConditionLinker.AND)
            relatedSubmissions.add(submission)

            evaluationResult = MultiFormReportHelper.evaluateRelatedSubmissionList(relatedSubmissions, currentEvaluationStatus, conditions, conditionLinkers)
        then:
            evaluationResult
        when: 'rp_cits == 2 && connectivity == excellent'
            relatedSubmissions = []
            currentEvaluationStatus = true
            submission = XformSubmission.findByXform(Xform.findByXlsId('myTest2'))
            conditions = []
            conditions.add(new Condition(questionKey:'rp_cits', operator: ConditionOperator.NUMERIC_EQUAL, operand: '2', formXlsId: 'myTest2'))
            conditions.add(new Condition(questionKey:'connectivity', operator: ConditionOperator.TEXT_EQUAL, operand: 'excellent', formXlsId: 'myTest2'))

            conditionLinkers = []
            conditionLinkers.add(ConditionLinker.AND)
            conditionLinkers.add(ConditionLinker.AND)
            relatedSubmissions.add(createSubmission(['connectivity':'excellent', 'sub_id': '12334'], 'myTest2'))
            relatedSubmissions.add(submission)

            evaluationResult = MultiFormReportHelper.evaluateRelatedSubmissionList(relatedSubmissions, currentEvaluationStatus, conditions, conditionLinkers)
        then:
            evaluationResult
        when: 'rp_cits == 2 && connectivity == excellent || rp_cits = 5 && connectivity = good'
            relatedSubmissions = []
            currentEvaluationStatus = true
            submission = XformSubmission.findByXform(Xform.findByXlsId('myTest2'))
            conditions = []
            conditions.add(new Condition(questionKey:'rp_cits', operator: ConditionOperator.NUMERIC_EQUAL, operand: '2', formXlsId: 'myTest2'))
            conditions.add(new Condition(questionKey:'connectivity', operator: ConditionOperator.TEXT_EQUAL, operand: 'excellent', formXlsId: 'myTest2'))
            conditions.add(new Condition(questionKey:'rp_cits', operator: ConditionOperator.NUMERIC_EQUAL, operand: '5', formXlsId: 'myTest2'))
            conditions.add(new Condition(questionKey:'connectivity', operator: ConditionOperator.TEXT_EQUAL, operand: 'good', formXlsId: 'myTest2'))

            conditionLinkers = []
            conditionLinkers.add(ConditionLinker.AND)
            conditionLinkers.add(ConditionLinker.AND)
            conditionLinkers.add(ConditionLinker.OR)
            conditionLinkers.add(ConditionLinker.AND)
            relatedSubmissions.add(createSubmission(['connectivity':'excellent', 'sub_id': '12334'], 'myTest2'))
            relatedSubmissions.add(submission)

            evaluationResult = MultiFormReportHelper.evaluateRelatedSubmissionList(relatedSubmissions, currentEvaluationStatus, conditions, conditionLinkers)
        then:
            evaluationResult
        when: 'rp_cits == 2 && connectivity == good || rp_cits = 2 && connectivity = excellent'
            relatedSubmissions = []
            currentEvaluationStatus = true
            submission = XformSubmission.findByXform(Xform.findByXlsId('myTest2'))
            conditions = []
            conditions.add(new Condition(questionKey:'rp_cits', operator: ConditionOperator.NUMERIC_EQUAL, operand: '2', formXlsId: 'myTest2'))
            conditions.add(new Condition(questionKey:'connectivity', operator: ConditionOperator.TEXT_EQUAL, operand: 'excellent', formXlsId: 'myTest2'))
            conditions.add(new Condition(questionKey:'rp_cits', operator: ConditionOperator.NUMERIC_EQUAL, operand: '5', formXlsId: 'myTest2'))
            conditions.add(new Condition(questionKey:'connectivity', operator: ConditionOperator.TEXT_EQUAL, operand: 'good', formXlsId: 'myTest2'))

            conditionLinkers = []
            conditionLinkers.add(ConditionLinker.AND) //this relates to the first condition that was validated
            conditionLinkers.add(ConditionLinker.AND)
            conditionLinkers.add(ConditionLinker.OR)
            conditionLinkers.add(ConditionLinker.AND)
            relatedSubmissions.add(createSubmission(['connectivity':'excellent', 'sub_id': '12334'], 'myTest2'))
            relatedSubmissions.add(submission)

            evaluationResult = MultiFormReportHelper.evaluateRelatedSubmissionList(relatedSubmissions, currentEvaluationStatus, conditions, conditionLinkers)
        then:
            evaluationResult
        when: 'rp_cits == 2 && connectivity == good || rp_cits = 2 && connectivity = perfect || rp_cits = 2 && connectivity = excellent'
            relatedSubmissions = []
            currentEvaluationStatus = true
            submission = XformSubmission.findByXform(Xform.findByXlsId('myTest2'))
            conditions = []
            conditions.add(new Condition(questionKey:'rp_cits', operator: ConditionOperator.NUMERIC_EQUAL, operand: '2', formXlsId: 'myTest2'))
            conditions.add(new Condition(questionKey:'connectivity', operator: ConditionOperator.TEXT_EQUAL, operand: 'excellent', formXlsId: 'myTest2'))
            conditions.add(new Condition(questionKey:'rp_cits', operator: ConditionOperator.NUMERIC_EQUAL, operand: '5', formXlsId: 'myTest2'))
            conditions.add(new Condition(questionKey:'connectivity', operator: ConditionOperator.TEXT_EQUAL, operand: 'good', formXlsId: 'myTest2'))

            conditionLinkers = []
            conditionLinkers.add(ConditionLinker.AND) //this relates to the first condition that was validated
            conditionLinkers.add(ConditionLinker.AND)
            conditionLinkers.add(ConditionLinker.OR)
            conditionLinkers.add(ConditionLinker.AND)
            relatedSubmissions.add(createSubmission(['connectivity':'excellent', 'sub_id': '12334'], 'myTest2'))
            relatedSubmissions.add(submission)

            evaluationResult = MultiFormReportHelper.evaluateRelatedSubmissionList(relatedSubmissions, currentEvaluationStatus, conditions, conditionLinkers)
        then:
            evaluationResult
        when: 'rp_cits == 2 && connectivity == good || rp_cits == 2 && connectivity == best'
            relatedSubmissions = []
            currentEvaluationStatus = true
            submission = XformSubmission.findByXform(Xform.findByXlsId('myTest2'))
            conditions = []
            conditions.add(new Condition(questionKey:'rp_cits', operator: ConditionOperator.NUMERIC_EQUAL, operand: '2', formXlsId: 'myTest2'))
            conditions.add(new Condition(questionKey:'connectivity', operator: ConditionOperator.TEXT_EQUAL, operand: 'good', formXlsId: 'myTest2'))
            conditions.add(new Condition(questionKey:'rp_cits', operator: ConditionOperator.NUMERIC_EQUAL, operand: '2', formXlsId: 'myTest2'))
            conditions.add(new Condition(questionKey:'connectivity', operator: ConditionOperator.TEXT_EQUAL, operand: 'perfect', formXlsId: 'myTest2'))

            conditionLinkers = []
            conditionLinkers.add(ConditionLinker.AND) //this relates to the first condition that was validated
            conditionLinkers.add(ConditionLinker.AND)
            conditionLinkers.add(ConditionLinker.OR)
            conditionLinkers.add(ConditionLinker.AND)
            relatedSubmissions.add(createSubmission(['connectivity':'excellent', 'sub_id': '12334'], 'myTest2'))
            relatedSubmissions.add(submission)

            evaluationResult = MultiFormReportHelper.evaluateRelatedSubmissionList(relatedSubmissions, currentEvaluationStatus, conditions, conditionLinkers)
        then:
            !evaluationResult
        when: 'connectivity == amazing'
            relatedSubmissions = []
            currentEvaluationStatus = true
            submission = XformSubmission.findByXform(Xform.findByXlsId('myTest2'))
            conditions = []
            conditions.add(new Condition(questionKey:'connectivity', operator: ConditionOperator.TEXT_EQUAL, operand: 'amazing', formXlsId: 'myTest2'))

            conditionLinkers = []
            conditionLinkers.add(ConditionLinker.AND)
            relatedSubmissions.add(submission)
            relatedSubmissions.add(createSubmission(['connectivity':'excellent', 'sub_id': '12334'], 'myTest2'))
            evaluationResult = MultiFormReportHelper.evaluateRelatedSubmissionList(relatedSubmissions, currentEvaluationStatus, conditions, conditionLinkers)
        then:
            !evaluationResult
    }

    void 'generateReportEntries correctly evaluates submissions against a report that is based on multiple forms'() {
        given:
            List<XformSubmission> submissions = []
            submissions.add(XformSubmission.findByXform(Xform.findByXlsId('myTest')))
            submissions.add(XformSubmission.findByXform(Xform.findByXlsId('myTest2')))
            Report report
            List<ReportEntry> reportEntries
        when: 'two conditions refer to the same question key'
            report = new Report()
            report.formLinkerQuestion = 'rp_state'
            report.addToConditions(new Condition(questionKey:'rp_state', operator: ConditionOperator.TEXT_EQUAL, operand: 'texas', formXlsId: 'myTest'))
            report.addToConditions(new Condition(questionKey:'rp_state', operator: ConditionOperator.TEXT_EQUAL, operand: 'texasw', formXlsId: 'myTest2'))
            report.addToConditionLinkers(ConditionLinker.OR)
            reportEntries = MultiFormReportHelper.generateReportEntries(report, submissions)
        then: 'things still work fine'
            reportEntries.size() == 2
            reportEntries.count{ it.submissions.find { it.xform.xlsId == 'myTest2'} } == 1
            reportEntries.count{ it.submissions.find { it.xform.xlsId == 'myTest' }} == 1
        when: 'two conditions refer to different keys'
            report = new Report()
            report.formLinkerQuestion = 'sub_id'
            report.addToConditions(new Condition(questionKey:'county', operator: ConditionOperator.TEXT_EQUAL, operand: 'king', formXlsId: 'myTest'))
            report.addToConditions(new Condition(questionKey:'rp_cits', operator: ConditionOperator.NUMERIC_EQUAL, operand: '2', formXlsId: 'myTest2'))
            report.addToConditionLinkers(ConditionLinker.OR)
            reportEntries = MultiFormReportHelper.generateReportEntries(report, submissions)
        then: 'things still work just fine'
            reportEntries.size() == 2
            reportEntries.count{ it.submissions.find { it.xform.xlsId == 'myTest2' } } == 1
            reportEntries.count{ it.submissions.find { it.xform.xlsId == 'myTest' } } == 1
        when: 'just one condition is supplied'
            report = new Report()
            report.formLinkerQuestion = 'sub_id'
            report.addToConditions(new Condition(questionKey:'county', operator: ConditionOperator.TEXT_EQUAL, operand: 'king', formXlsId: 'myTest'))
            reportEntries = MultiFormReportHelper.generateReportEntries(report, submissions)
        then:
            reportEntries.count{ it.submissions.find { it.xform.xlsId == 'myTest' } } == 1
        when: 'given 3 conditions with two submissions having the same value for the reportLinker'
            report = new Report()
            report.formLinkerQuestion = 'sub_id'
            report.addToConditions(new Condition(questionKey:'county', operator: ConditionOperator.TEXT_EQUAL, operand: 'king', formXlsId: 'myTest'))
            report.addToConditions(new Condition(questionKey:'rp_cits', operator: ConditionOperator.NUMERIC_EQUAL, operand: '2', formXlsId: 'myTest2'))
            report.addToConditions(new Condition(questionKey:'connectivity', operator: ConditionOperator.TEXT_EQUAL, operand: 'excellent', formXlsId: 'myTest2'))
            report.addToConditionLinkers(ConditionLinker.OR)
            report.addToConditionLinkers(ConditionLinker.AND)
            submissions.add(createSubmission(['connectivity':'excellent', 'sub_id': '12334'], 'myTest2'))
            reportEntries = MultiFormReportHelper.generateReportEntries(report, submissions)
        then:
            reportEntries.count{ it.submissions.find { it.xform.xlsId == 'myTest2' } } == 1
            reportEntries.find{ it.submissions.count { it.xform.xlsId == 'myTest2' } }.submissions.size() == 2
            reportEntries.count{ it.submissions.find { it.xform.xlsId == 'myTest' } } == 1
        when: 'given 3 conditions with two submissions having the same value for the reportLinker'
            report = new Report()
            report.formLinkerQuestion = 'sub_id'
            report.addToConditions(new Condition(questionKey:'county', operator: ConditionOperator.TEXT_EQUAL, operand: 'king', formXlsId: 'myTest'))
            report.addToConditions(new Condition(questionKey:'rp_cits', operator: ConditionOperator.NUMERIC_EQUAL, operand: '2', formXlsId: 'myTest2'))
            report.addToConditions(new Condition(questionKey:'connectivity', operator: ConditionOperator.TEXT_EQUAL, operand: 'false', formXlsId: 'myTest2'))
            report.addToConditionLinkers(ConditionLinker.OR)
            report.addToConditionLinkers(ConditionLinker.AND)
            submissions.add(createSubmission(['connectivity':'excellent', 'sub_id': '12334'], 'myTest2'))
            reportEntries = MultiFormReportHelper.generateReportEntries(report, submissions)
        then:
            reportEntries.count{ it.submissions.find { it.xform.xlsId == 'myTest2' } } == 0
            reportEntries.count{ it.submissions.find { it.xform.xlsId == 'myTest' } } == 1
    }


    private static XformQuestion createXformQuestion(String question, String answer) {
        XformQuestion formQuestion = new XformQuestion(question: question)
        formQuestion.addToAnswers( new XformAnswer(value: answer))
        return formQuestion
    }

    /**
     * Helper method for creating a submission for a given existing {@link Xform}
     * @param params
     * @param formXlsId
     * @return
     */
    private static XformSubmission createSubmission(Map<String, String> params, String formXlsId) {
        XformSubmission submission = new XformSubmission()
        params.each { key, answer ->
            submission.addToQuestions(createXformQuestion(key, answer))
        }
        submission.xform = Xform.findByXlsId(formXlsId)
        submission.save(failOnError: true)
        return submission
    }

    private static void createSampleSubmissions() {
        def params = [county : 'king', 'rp_state' : 'texas', 'rp_cities': 'dumont', 'sub_id': '1233']
        Xform form = new Xform(name: 'createFormSubmissionTextForm', formXml: 'some valid xml', title: 'test form', xlsId:'myTest').save(failOnError: true)
        XformSubmission submission = new XformSubmission()
        params.each { key, answer ->
            submission.addToQuestions(createXformQuestion(key, answer))
        }
        submission.xform = form
        submission.save(failOnError: true)

        params = [count : 'king', 'rp_state' : 'texasw', 'rp_cits': '2', 'sub_id': '12334']
        form = new Xform(name: 'createFormSubmissionTextForm', formXml: 'some valid xml', title: 'test form', xlsId:'myTest2').save(failOnError: true)
        submission = new XformSubmission()
        params.each { key, answer ->
            submission.addToQuestions(createXformQuestion(key, answer))
        }
        submission.xform = form
        submission.save(failOnError: true)
    }
}
