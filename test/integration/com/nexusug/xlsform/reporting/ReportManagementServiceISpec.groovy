package com.nexusug.xlsform.reporting

import com.nexusug.xlsform.ReportManagementService
import com.nexusug.xlsform.Xform
import com.nexusug.xlsform.XformQuestion
import com.nexusug.xlsform.XformSubmission
import grails.test.spock.IntegrationSpec

/**
 * Created by ivan on 27/12/2015.
 */
class ReportManagementServiceISpec extends IntegrationSpec {
	ReportManagementService reportManagementService

	void 'generateReport correctly obtains the submissions matching the criteria of a given multi form report'() {
		given:
			createSampleSubmission('myTest', [county: 'king', 'rp_state': 'ohio', 'rp_cits': '1'])
			createSampleSubmission('myTest', [county: 'king', 'rp_state': 'texas', 'rp_cits': '2'])
			createSampleSubmission('myTest', [county: 'king', 'rp_state': 'texas', 'rp_cits': '3'])
			createSampleSubmission('myTest2', [county: 'king', 'governor': 'allan', 'food': 'rice'])
			createSampleSubmission('myTest2', [county: 'phil', 'governor': 'ellen', 'food': 'rice'])
			createSampleSubmission('myTest2', [county: 'chicago', 'governor': 'alice', 'food': 'potatoes'])
			Report report = new Report()
			List<ReportEntry> reportEntries
		when: 'rp_state = ohio || rp_cits <= 2 || food = rice'
			report.conditions = []
			report.addToConditions([questionKey:'rp_state', operator: ConditionOperator.TEXT_EQUAL,
									operand: 'ohio', formXlsId: 'myTest'])
			report.addToConditions([questionKey:'rp_cits', operator: ConditionOperator.LESS_EQUAL,
									operand: '2', formXlsId: 'myTest'])
			report.addToConditions([questionKey:'food', operator: ConditionOperator.TEXT_EQUAL,
									operand: 'rice', formXlsId: 'myTest2'])

			report.addToConditionLinkers(ConditionLinker.OR)
			report.addToConditionLinkers(ConditionLinker.OR)
			report.formLinkerQuestion = 'county'
			reportEntries = reportManagementService.generateReport(report)
		then:
			reportEntries.size() == 3
			reportEntries.count{ it.submissions.size() == 2 && it.submissions*.xform*.xlsId.containsAll(['myTest2', 'myTest']) } == 1
	}

	void 'generateReport correctly mix matches logical condition linkers for single form reports'() {
		given:
			createSampleSubmission('myTest', [county: 'king', 'rp_state': 'ohio', 'rp_cits': '1'])
			createSampleSubmission('myTest', [county: 'king', 'rp_state': 'texas', 'rp_cits': '2'])
			createSampleSubmission('myTest', [county: 'king', 'rp_state': 'texas', 'rp_cits': '3'])
			Report report = new Report()
			List<ReportEntry> reportEntries
		when: 'rp_state = ohio || rp_cits <= 2'
			report.conditions = []
			report.addToConditions([questionKey:'rp_state', operator: ConditionOperator.TEXT_EQUAL,
									operand: 'ohio', formXlsId: 'myTest'])
			report.addToConditions([questionKey:'rp_cits', operator: ConditionOperator.LESS_EQUAL,
									operand: '2', formXlsId: 'myTest'])
			report.addToConditionLinkers(ConditionLinker.OR)
			reportEntries = reportManagementService.generateReport(report)
		then:
			reportEntries.size() == 2
		when: '((county = king && rp_cits < 3) || rp_cits == 3)'
			report.conditions.clear()
			report.conditionLinkers.clear()
			report.addToConditionLinkers(ConditionLinker.AND)
			report.addToConditionLinkers(ConditionLinker.OR)
			report.addToConditions([questionKey:'county', operator: ConditionOperator.TEXT_EQUAL,
									operand: 'king', formXlsId: 'myTest'])
			report.addToConditions([questionKey:'rp_cits', operator: ConditionOperator.LESS_THAN,
									operand: '3', formXlsId: 'myTest'])
			report.addToConditions([questionKey:'rp_cits', operator: ConditionOperator.NUMERIC_EQUAL,
									operand: '3', formXlsId: 'myTest'])
			reportEntries = reportManagementService.generateReport(report)
		then:
			reportEntries.size() == 3
		when: '(((county = king && rp_cits < 3) || rp_cits == 3) && rp_state == ohio)'
			report.conditions.clear()
			report.conditionLinkers.clear()
			report.addToConditionLinkers(ConditionLinker.AND)
			report.addToConditionLinkers(ConditionLinker.OR)
			report.addToConditionLinkers(ConditionLinker.AND)
			report.addToConditions([questionKey:'county', operator: ConditionOperator.TEXT_EQUAL,
									operand: 'king', formXlsId: 'myTest'])
			report.addToConditions([questionKey:'rp_cits', operator: ConditionOperator.LESS_THAN,
									operand: '3', formXlsId: 'myTest'])
			report.addToConditions([questionKey:'rp_cits', operator: ConditionOperator.NUMERIC_EQUAL,
									operand: '3', formXlsId: 'myTest'])
			report.addToConditions([questionKey:'rp_state', operator: ConditionOperator.TEXT_EQUAL,
									operand: 'ohio', formXlsId: 'myTest'])
			reportEntries = reportManagementService.generateReport(report)
		then:
			reportEntries.size() == 1
	}

	void 'generateReport correctly obtains the submissions matching the criteria of a given single form report'() {
		given:
			createSampleSubmission('myTest', [county : 'king', 'rp_state' : 'texas', 'rp_cits': '1'])
			createSampleSubmission('myTest', [county : 'king', 'rp_state' : 'texasw', 'rp_cits': '2'])
			createSampleSubmission('myTest', [county : 'king', 'rp_state' : 'texasw', 'rp_cits': '3'])
			Report report = new Report()
			List<ReportEntry> reportEntries
		when: 'there are no conditions, the entire stored submissions are returned'
			reportEntries = reportManagementService.generateReport(report)
		then:
			reportEntries.size() == 3
		when: 'there is one condition'
			report.conditions = [new Condition(questionKey:'rp_state', operator: ConditionOperator.TEXT_EQUAL, operand: 'texas', formXlsId: 'myTest')]
			reportEntries = reportManagementService.generateReport(report)
		then:
			reportEntries.size() == 1
		when: 'there are two conditions'
			report.conditions.clear()
			report.addToConditions([questionKey:'rp_state', operator: ConditionOperator.TEXT_BEGINS_WITH, operand: 'tExas', formXlsId: 'myTest'])
			report.addToConditions([questionKey:'rp_cits', operator: ConditionOperator.LESS_EQUAL, operand: '2', formXlsId: 'myTest'])
			report.addToConditionLinkers(ConditionLinker.AND)
			reportEntries = reportManagementService.generateReport(report)
		then:
			reportEntries.size() == 2
		when: 'there are more than two conditions'
			report.conditions.clear()
			report.conditionLinkers.clear()
			report.addToConditions([questionKey:'rp_state', operator: ConditionOperator.TEXT_CONTAINS, operand: 'texas', formXlsId: 'myTest'])
			report.addToConditions([questionKey:'rp_cits', operator: ConditionOperator.GREATER_EQUAL, operand: '1', formXlsId: 'myTest'])
			report.addToConditions([questionKey:'county', operator: ConditionOperator.TEXT_EQUAL, operand: 'king', formXlsId: 'myTest'])
			report.addToConditionLinkers(ConditionLinker.AND)
			report.addToConditionLinkers(ConditionLinker.AND)
			reportEntries = reportManagementService.generateReport(report)
		then:
			reportEntries.size() == 3
	}

	/**
	 * Helper method that created two forms and adds sample submissions for them.
	 */
	private static void createSampleSubmission(String formId, Map<String, String> questionAnswerMap) {
		Xform form = Xform.findByXlsId(formId) ?: new Xform(name: 'createFormSubmissionTextForm',
				formXml: 'some valid xml', title: 'test form', xlsId:formId).save(failOnError: true)

		XformSubmission submission = new XformSubmission()
		questionAnswerMap.each { String key, String answer ->
			submission.addToQuestions(createXformQuestion(key, answer))
		}
		submission.xform = form
		submission.save(failOnError: true)
	}

	/**
	 * Helper method for creating an {@link XformQuestion}. The returned {@link XformQuestion} is not persisited to the db already
	 * @param question
	 * @param answer
	 * @return
	 */
	private static XformQuestion createXformQuestion(String question, String answer) {
		XformQuestion formQuestion = new XformQuestion(question: question)
		formQuestion.addToAnswers([value: answer])
		return formQuestion
	}
}
