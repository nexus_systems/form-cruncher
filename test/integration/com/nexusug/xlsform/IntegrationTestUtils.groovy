package com.nexusug.xlsform
/**
 * Created by ivan on 09/04/2016.
 */
class IntegrationTestUtils {

	static InputStream readTestResourceAsStream(Class aClass, String resourceName) {
		return aClass.getClassLoader().getResourceAsStream("resources/$resourceName")
	}
}
