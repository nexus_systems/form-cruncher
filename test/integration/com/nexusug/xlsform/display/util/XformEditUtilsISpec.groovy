package com.nexusug.xlsform.display.util

import com.nexusug.xlsform.IntegrationTestUtils
import com.nexusug.xlsform.XformQuestion
import com.nexusug.xlsform.XformSubmission
import com.nexusug.xlsform.display.model.*
import com.nexusug.xlsform.reporting.XformAnswer
import grails.test.spock.IntegrationSpec

/**
 * Created by ivan on 16/04/2016.
 */
class XformEditUtilsISpec extends IntegrationSpec {

	void "getFormElements correctly populates the values of form elements from previous submissions"() {

		given:
			XformQuestion childName_0 = new XformQuestion(repeatName: 'child_repeat', inRepeatIndex: 0, repeatQuestion: true)
			childName_0.question = 'child_name'
			childName_0.answers = [new XformAnswer(value: 'mike')]

			XformQuestion gender_0 = new XformQuestion(repeatName: 'child_repeat', inRepeatIndex: 0, repeatQuestion: true)
			gender_0.question = 'sex_child'
			gender_0.answers = [new XformAnswer(value: 'male')]

			XformQuestion deliveryDate = new XformQuestion(question: 'delivery_date')
			deliveryDate.answers = [new XformAnswer(value: '2012-03-23')]

			XformQuestion deliverPlace = new XformQuestion(question: 'place_delivery')
			deliverPlace.answers = [new XformAnswer(value: 'home')]

			XformQuestion childName_1 = new XformQuestion(repeatName: 'child_repeat', inRepeatIndex: 1, repeatQuestion: true)
			childName_1.question = 'child_name'
			childName_1.answers = [new XformAnswer(value: 'alice')]

			XformQuestion gender_1 = new XformQuestion(repeatName: 'child_repeat', inRepeatIndex: 1, repeatQuestion: true)
			gender_1.question = 'sex_child'
			gender_1.answers = [new XformAnswer(value: 'female')]

			String form = IntegrationTestUtils.readTestResourceAsStream(this.class, 'delivery_outcome_form.xml').text
			XformSubmission submission = new XformSubmission()
		when:
			submission.questions = [deliveryDate, deliverPlace, childName_0, gender_0, childName_1, gender_1]
			List<XformDisplayable> processedDisplayables = XformReaderEditUtils.getFormElements(form, submission)
		then:
			processedDisplayables.size() == 4
			with(processedDisplayables[0]) {
				name == 'delivery_date'
				value == '2012-03-23'
				label == 'Delivery date?'
				required
			}

			with(processedDisplayables[1]) {
				name == 'place_delivery'
				value == 'home'
				required
			}

			with(processedDisplayables[2]) {
				name == 'child_repeat'
				type == XformMultiDisplayableType.repeat
			}

			with(processedDisplayables[3]) {
				name == 'child_repeat'
				type == XformMultiDisplayableType.repeat
			}

			with(((XformMultiDisplayable)processedDisplayables[2]).displayables[0]) {
				name == 'child_name'
				value == 'mike'
			}

			with(((XformMultiDisplayable)processedDisplayables[2]).displayables[1]) {
				name == 'sex_child'
				value == 'male'
			}

			with(((XformMultiDisplayable)processedDisplayables[3]).displayables[0]) {
				name == 'child_name'
				value == 'alice'
			}

			with(((XformMultiDisplayable)processedDisplayables[3]).displayables[1]) {
				name == 'sex_child'
				type == XformSelectElementType.select1
				value == 'female'
			}
		when: 'editing an older submission that didn\'t have all the current questions'
			submission.questions = [deliveryDate]
			processedDisplayables = XformReaderEditUtils.getFormElements(form, submission)
		then: 'the new questions will be contained in the processed displayables'
			processedDisplayables.size() == 3
			with(processedDisplayables[0]) {
				name == 'delivery_date'
				value == '2012-03-23'
				label == 'Delivery date?'
				required
			}

			with(processedDisplayables[1]) {
				name == 'place_delivery'
				value == null
				required
			}

			with(processedDisplayables[2]) {
				name == 'child_repeat'
				type == XformMultiDisplayableType.repeat
			}
	}

	void "processXformMultiDisplayable correctly populates a multi displayable for editing preparation"() {

		given:
			XformSelectElement select = new XformSelectElement(name: 'some_select', type: XformSelectElementType.select)
			select.hint = 'select hint'

			XformTextBox textBox = new XformTextBox(name: 'some_text', type: XformTextBoxType.string)
			textBox.hint = 'text hint'
			textBox.label = 'text label'

			XformTextBox updatedTextBox = new XformTextBox(name: 'updated_text', type: XformTextBoxType.string)
			updatedTextBox.hint = 'updated_text hint'
			updatedTextBox.label = 'updated_text label'


			XformMultiDisplayable multiDisplayable = new XformMultiDisplayable(type: XformMultiDisplayableType.repeat)
			multiDisplayable.name = 'testing_repeat'
			multiDisplayable.label = 'test_repeat label'
			multiDisplayable.hint = 'test_repeat hint'
			multiDisplayable.appearance = 'compact and sleek'

			XformQuestion selectQuestion_0 = new XformQuestion(repeatName: 'testing_repeat', inRepeatIndex: 0)
			selectQuestion_0.question = 'some_select'
			selectQuestion_0.answers = [new XformAnswer(value: 'cats_0'), new XformAnswer(value: 'dogs_0')]

			XformQuestion textQuestion_0 = new XformQuestion(repeatName: 'testing_repeat', inRepeatIndex: 0)
			textQuestion_0.question = 'some_text'
			textQuestion_0.answers = [new XformAnswer(value: 'very good 0')]

			XformQuestion selectQuestion_1 = new XformQuestion(repeatName: 'testing_repeat', inRepeatIndex: 1)
			selectQuestion_1.question = 'some_select'
			selectQuestion_1.answers = [new XformAnswer(value: 'cats_1'), new XformAnswer(value: 'dogs_1')]

			XformQuestion textQuestion_1 = new XformQuestion(repeatName: 'testing_repeat', inRepeatIndex: 1)
			textQuestion_1.question = 'some_text'
			textQuestion_1.answers = [new XformAnswer(value: 'very good 1')]

			List<XformQuestion> questions

		when: 'two questions match the displayable'
			multiDisplayable.displayables = []
			multiDisplayable.displayables.addAll([select, textBox])
			questions = [selectQuestion_0, textQuestion_0, selectQuestion_1, textQuestion_1]
			List<XformMultiDisplayable> processedDisplayables = XformReaderEditUtils.processXformMultiDisplayable(multiDisplayable, questions)
		then: 'only two multi displayables for the two questions are returned. The original multi-displayable is ignored'
			processedDisplayables.size() == 2
			with(processedDisplayables[0]) {
				type == XformMultiDisplayableType.repeat
				name == 'testing_repeat'
				appearance == 'compact and sleek'
				displayables.size() == 2
			}

			with(processedDisplayables[1]) {
				type == XformMultiDisplayableType.repeat
				name == 'testing_repeat'
				appearance == 'compact and sleek'
				displayables.size() == 2
			}

			with(processedDisplayables[0].displayables[0]) {
				type == XformSelectElementType.select
				name == 'some_select'
				hint == 'select hint'
				value == 'cats_0,dogs_0'
			}

			with(processedDisplayables[0].displayables[1]) {
				type == XformTextBoxType.string
				name == 'some_text'
				hint == 'text hint'
				value == 'very good 0'
			}

			with(processedDisplayables[1].displayables[0]) {
				type == XformSelectElementType.select
				name == 'some_select'
				hint == 'select hint'
				value == 'cats_1,dogs_1'
			}

			with(processedDisplayables[1].displayables[1]) {
				type == XformTextBoxType.string
				name == 'some_text'
				hint == 'text hint'
				value == 'very good 1'
			}

		when: 'no question matches the the multi-displayable\'s name'
			multiDisplayable.displayables = []
			multiDisplayable.displayables.addAll([select, textBox])
			questions = []
			processedDisplayables = XformReaderEditUtils.processXformMultiDisplayable(multiDisplayable, questions)
		then: 'only the originally supplied multi-displayable is returned'
			processedDisplayables.size() == 1

			with(processedDisplayables[0]) {
				type == XformMultiDisplayableType.repeat
				name == 'testing_repeat'
				appearance == 'compact and sleek'
				hint == 'test_repeat hint'
				displayables.size() == 2
			}

			with(processedDisplayables[0].displayables[0]) {
				type == XformSelectElementType.select
				name == 'some_select'
				hint == 'select hint'
				value == null
			}

			with(processedDisplayables[0].displayables[1]) {
				type == XformTextBoxType.string
				name == 'some_text'
				label == 'text label'
				value == null
			}

		when: 'submissions contain questions that no longer exist in the form'
			multiDisplayable.displayables = []
			multiDisplayable.displayables.addAll([select])
			questions = [selectQuestion_0, textQuestion_0, selectQuestion_1, textQuestion_1]
			processedDisplayables = XformReaderEditUtils.processXformMultiDisplayable(multiDisplayable, questions)
		then: 'those questions are ignored'
			processedDisplayables.size() == 2
			with(processedDisplayables[0]) {
				type == XformMultiDisplayableType.repeat
				name == 'testing_repeat'
				appearance == 'compact and sleek'
				displayables.size() == 1
			}

			with(processedDisplayables[1]) {
				type == XformMultiDisplayableType.repeat
				name == 'testing_repeat'
				appearance == 'compact and sleek'
				displayables.size() == 1
			}

			with(processedDisplayables[0].displayables[0]) {
				type == XformSelectElementType.select
				name == 'some_select'
				hint == 'select hint'
				value == 'cats_0,dogs_0'
			}

			with(processedDisplayables[1].displayables[0]) {
				type == XformSelectElementType.select
				name == 'some_select'
				hint == 'select hint'
				value == 'cats_1,dogs_1'
			}

		when: 'there are questions that exist in the multi-displayable but don\'t match any previous submission'
			multiDisplayable.displayables = []
			multiDisplayable.displayables.addAll([select, updatedTextBox])
			questions = [selectQuestion_0, textQuestion_0, selectQuestion_1, textQuestion_1]
			processedDisplayables = XformReaderEditUtils.processXformMultiDisplayable(multiDisplayable, questions)
		then: 'those questions are added to each of the processed displayables'
			processedDisplayables.size() == 2
			with(processedDisplayables[0]) {
				type == XformMultiDisplayableType.repeat
				name == 'testing_repeat'
				appearance == 'compact and sleek'
				displayables.size() == 2
			}

			with(processedDisplayables[1]) {
				type == XformMultiDisplayableType.repeat
				name == 'testing_repeat'
				appearance == 'compact and sleek'
				displayables.size() == 2
			}

			with(processedDisplayables[0].displayables[0]) {
				type == XformSelectElementType.select
				name == 'some_select'
				hint == 'select hint'
				value == 'cats_0,dogs_0'
			}

			with(processedDisplayables[0].displayables[1]) {
				type == XformTextBoxType.string
				name == 'updated_text'
				hint == 'updated_text hint'
				value == null
			}

			with(processedDisplayables[1].displayables[0]) {
				type == XformSelectElementType.select
				name == 'some_select'
				hint == 'select hint'
				value == 'cats_1,dogs_1'
			}

			with(processedDisplayables[1].displayables[1]) {
				type == XformTextBoxType.string
				name == 'updated_text'
				hint == 'updated_text hint'
				value == null
			}
	}
}
