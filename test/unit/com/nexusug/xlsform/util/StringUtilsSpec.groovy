package com.nexusug.xlsform.util

import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by ivan on 11/03/2016.
 */
class StringUtilsSpec extends Specification {

	@Unroll
	void 'sanitizeExtraWhiteSpace removes extra whitespace'() {
		expect:
			result == StringUtils.sanitizeExtraWhiteSpace(target)
		where:
			target 					| result
			'ivan      orone   ' 	| 'ivan orone '
			'ivan      orone' 		| 'ivan orone'
			'ivan      orone\t' 	| 'ivan orone '
			'ivan   \n\n   orone' 	| 'ivan orone'
			'ivan   \n\t   orone' 	| 'ivan orone'
	}
}
