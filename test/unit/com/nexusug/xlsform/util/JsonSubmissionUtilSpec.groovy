package com.nexusug.xlsform.util

import com.nexusug.xlsform.exception.InvalidSubmission
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by ivan on 26/03/2016.
 */
class JsonSubmissionUtilSpec extends Specification {

	void 'verifyPayload correctly validates json payloads'() {
		when:
			boolean result = JsonSubmissionUtil.verifyPayload([xlsId: 'some id'])
		then:
			result == true
		when:
			JsonSubmissionUtil.verifyPayload([cream: 'really cool'])
		then:
			InvalidSubmission exception = thrown()
			exception.message == 'Required property \'xlsId\' is missing'
	}

	@Unroll
	void 'isRepeatSubmission correctly detects a repeated submission'() {
		expect:
			JsonSubmissionUtil.isRepeatSubmission(submission) == expected
		where:
			submission 						| expected
			['cats', 'dogs'] 				| false
			[[name: 'simba', age: '1']] 	| true
			[cat:[]]						| false
			[] 								| false
			'animals' 						| false
	}
}
