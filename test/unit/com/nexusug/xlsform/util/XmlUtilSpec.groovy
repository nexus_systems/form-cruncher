package com.nexusug.xlsform.util

import spock.lang.Specification
import spock.lang.Unroll

import javax.xml.namespace.QName
import groovy.xml.QName as GroovyQName

/**
 * Created by ivan on 27/03/2016.
 */
class XmlUtilSpec extends Specification {

	@Unroll
	void 'getNodeName correctly extracts the node name'() {
		expect:
			XmlUtil.getNodeName(node) == nodeName
		where:
			node 																		| nodeName
			new Node(null, 'random_name', [id:'sample_form'])							| 'random_name'
			new Node(null, new QName("http://localhost.com", 'some_question'))			| 'some_question'
			new Node(null, new GroovyQName("http://localhost.com", 'ran_question')) 	| 'ran_question'
	}
}
