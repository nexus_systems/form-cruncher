package com.nexusug.xlsform.reporting

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by ivan on 11/03/2016.
 */
@TestFor(Condition)
class ConditionSpec extends Specification {

	@Unroll
	void 'conditions are always saved with extra spaces removed'() {

		when:
			Condition condition = new Condition(questionKey: 'key', operator: ConditionOperator.NUMERIC_EQUAL,operand: operand,
					formXlsId: 'formId')
			condition.validate()
		then:
			condition.operand == expectedOperand
		where:
			operand | expectedOperand
			'ivan      orone   ' 	| 'ivan orone'
			'ivan      orone' 		| 'ivan orone'
			'ivan      orone\t' 	| 'ivan orone'
			'ivan   \n\n   orone' 	| 'ivan orone'
			'ivan   \n\t   orone' 	| 'ivan orone'
	}

	@Unroll
	void 'conditions must always have an operand that can be validated by the specified ConditionOperator'() {

		given:
			mockForConstraintsTests(Condition)
			Condition condition
		when:
			condition = new Condition(questionKey:'rp_state', operator: operator, operand: operand, formXlsId: 'myTest', report: Mock(Report))
		then:
			condition.validate() == validationResult
		where:
			operator 					| operand 	| validationResult
			ConditionOperator.LESS_THAN | 'ivan' 	| false
			ConditionOperator.LESS_THAN | '2' 		| true
	}
}
