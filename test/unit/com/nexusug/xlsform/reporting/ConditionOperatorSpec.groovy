package com.nexusug.xlsform.reporting

import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
class ConditionOperatorSpec extends Specification {

	@Unroll
	void "text equal evaluator works as expected"() {
		expect:
			ConditionOperator.TEXT_EQUAL.evaluate(constant, variable) == result
		where:
			constant	| variable	| result
			'ivan'		| 'ivAn'	| true
			'ivn'		| 'ivan'	| false
			'3'			| null		| false
			null		| '2'		| false
			null		| null		| true
	}

	@Unroll
	void "text contains evaluator works as expected"() {
		expect:
			ConditionOperator.TEXT_CONTAINS.evaluate(constant, variable) == result
		where:
			constant	| variable	| result
			'ivan'		| 'ivan'	| true
			'ivan'		| 'ivAnIas'	| false
			'ivAnIas'	| 'ivan'	| true
			'3'			| null		| false
			null		| '2'		| false
			null		| null		| false
	}

	@Unroll
	void "text beings with evaluator works as expected"() {
		expect:
			ConditionOperator.TEXT_BEGINS_WITH.evaluate(constant, variable) == result
		where:
			constant	| variable	| result
			'ivan'		| 'ivan'	| true
			'ivan'		| 'ivAnIas'	| false
			'ivAnIas'	| 'ivan'	| true
			'3'			| null		| false
			null		| '2'		| false
			null		| null		| false
	}

	@Unroll
	void "text ends with evaluator works as expected"() {
		expect:
			ConditionOperator.TEXT_ENDS_WITH.evaluate(constant, variable) == result
		where:
			constant	| variable	| result
			'ivan'		| 'ivan'	| true
			'aN'		| 'ivan'	| false
			'ivan'		| 'aN'		| true
			'3'			| null		| false
			null		| '2'		| false
			null		| null		| false
	}

	@Unroll
	void "numeric equal evaluator works as expected"() {
        expect:
			ConditionOperator.NUMERIC_EQUAL.evaluate(constant, variable) == result
		where:
			constant	| variable	| result
			'1'			| '2'		| false
			'1'			| '1'		| true
			'1'			| '1_cat'	| false
			'1.00'		| '1.00'	| true
			'ivan'		| 'ivan'	| false
			'3'			| '2'		| false
			'3'			| null		| false
			null		| '2'		| false
			null		| null		| false
	}

	@Unroll
	void 'greater than evaluator works as expected'() {
		expect:
			ConditionOperator.GREATER_THAN.evaluate(constant, variable) == result
		where:
			constant	| variable	| result
			'2.2'		| '2.4'		| false
			'2.5'		| '2.4'		| true
			'3'			| '2'		| true
			'3'			| '4'		| false
			'v'			| 'c'		| false
			'3'			| null		| false
			null		| '2'		| false
			null		| null		| false
	}

	@Unroll
	void 'less than evaluator works as expected'() {
		expect:
			ConditionOperator.LESS_THAN.evaluate(constant, variable) == result
		where:
			constant	| variable	| result
			'2.2'		| '2.4'		| true
			'2.5'		| '2.4'		| false
			'3'			| '2'		| false
			'3'			| '4'		| true
			'v'			| 'c'		| false
			'3'			| null		| false
			null		| '2'		| false
			null		| null		| false
	}

	@Unroll
	void 'less equal evaluator works as expected'() {
		expect:
			ConditionOperator.LESS_EQUAL.evaluate(constant, variable) == result
		where:
			constant	| variable	| result
			'2.2'		| '2.4'		| true
			'2.2'		| '2.2'		| true
			'2.5'		| '2.4'		| false
			'3'			| '2'		| false
			'3'			| '4'		| true
			'3'			| '3'		| true
			'v'			| 'c'		| false
			'3'			| null		| false
			null		| '2'		| false
			null		| null		| false
	}

	@Unroll
	void 'greater equal evaluator works as expected'() {
		expect:
			ConditionOperator.GREATER_EQUAL.evaluate(constant, variable) == result
		where:
			constant	| variable	| result
			'2.2'		| '2.4'		| false
			'2.2'		| '2.2'		| true
			'2.5'		| '2.4'		| true
			'3'			| '2'		| true
			'3'			| '4'		| false
			'3'			| '3'		| true
			'v'			| 'c'		| false
			'3'			| null		| false
			null		| '2'		| false
			null		| null		| false
	}

	@Unroll
	void 'canValidate is correctly evaluated by all operators'() {
		expect:
			operator.canValidate(value) == expectedResult
		where:
			operator 							| value 	| expectedResult
			ConditionOperator.TEXT_EQUAL		| 'name' 	| true
			ConditionOperator.TEXT_EQUAL		| '2' 		| true

			ConditionOperator.TEXT_CONTAINS		| 'name' 	| true
			ConditionOperator.TEXT_CONTAINS		| '2' 		| true

			ConditionOperator.TEXT_BEGINS_WITH	| 'name' 	| true
			ConditionOperator.TEXT_BEGINS_WITH	| '2' 		| true

			ConditionOperator.TEXT_ENDS_WITH	| 'name' 	| true
			ConditionOperator.TEXT_ENDS_WITH	| '2' 		| true

			ConditionOperator.NUMERIC_EQUAL		| '2' 		| true
			ConditionOperator.NUMERIC_EQUAL		| 'name' 	| false

			ConditionOperator.GREATER_THAN		| '2' 		| true
			ConditionOperator.GREATER_THAN		| 'name' 	| false

			ConditionOperator.LESS_THAN			| '2' 		| true
			ConditionOperator.LESS_THAN			| 'name' 	| false

			ConditionOperator.LESS_EQUAL		| '2' 		| true
			ConditionOperator.LESS_EQUAL		| 'name' 	| false

			ConditionOperator.GREATER_EQUAL		| '2' 		| true
			ConditionOperator.GREATER_EQUAL		| 'name' 	| false
	}

	@Unroll
	void 'generateHql generates the expected hql for each condition operator'(){
		expect:
			operator.generateHql('name', 'value') == expectedResult
		where:
			operator 							| expectedResult
			ConditionOperator.TEXT_EQUAL		| 'UPPER(name) = UPPER(value) '
			ConditionOperator.TEXT_CONTAINS		| 'UPPER(name) LIKE UPPER(value) '
			ConditionOperator.TEXT_BEGINS_WITH	| 'UPPER(name) LIKE UPPER(value) '
			ConditionOperator.TEXT_ENDS_WITH	| 'UPPER(name) LIKE UPPER(value) '
			ConditionOperator.NUMERIC_EQUAL		| 'regexp(name, \'^([0-9]+)?(.[0-9]+)?\$\') <> 0 AND name = value '
			ConditionOperator.GREATER_THAN		| 'regexp(name, \'^([0-9]+)?(.[0-9]+)?\$\') <> 0 AND name > value '
			ConditionOperator.LESS_THAN			| 'regexp(name, \'^([0-9]+)?(.[0-9]+)?\$\') <> 0 AND name < value '
			ConditionOperator.LESS_EQUAL		| 'regexp(name, \'^([0-9]+)?(.[0-9]+)?\$\') <> 0 AND name <= value '
			ConditionOperator.GREATER_EQUAL		| 'regexp(name, \'^([0-9]+)?(.[0-9]+)?\$\') <> 0 AND name >= value '
	}

	@Unroll
	void 'processOperand generates the expected operand to be used for each condition operator'(){
		expect:
			operator.processOperand(value) == expectedResult
		where:
			operator 							| value		| expectedResult
			ConditionOperator.TEXT_EQUAL		| 'ivan'	| 'ivan'
			ConditionOperator.TEXT_CONTAINS		| 'ivan'	| '%ivan%'
			ConditionOperator.TEXT_BEGINS_WITH	| 'ivan'	| 'ivan%'
			ConditionOperator.TEXT_ENDS_WITH	| 'ivan'	| '%ivan'
			ConditionOperator.NUMERIC_EQUAL		| '1'		| '1'
			ConditionOperator.GREATER_THAN		| '1'		| '1'
			ConditionOperator.LESS_THAN			| '1'		| '1'
			ConditionOperator.LESS_EQUAL		| '1'		| '1'
			ConditionOperator.GREATER_EQUAL		| '1'		| '1'
	}
}
