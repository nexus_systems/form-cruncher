package com.nexusug.xlsform.reporting

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Report)
@Mock([Condition, Report])
class ReportSpec extends Specification {

	def setup() {
	}

	def cleanup() {
	}

	@Unroll
	void "conditionLinkers must always match up the number of supplied conditions"() {
		given:
			mockForConstraintsTests(Report)
			Report report
		when:
			report = new Report(name: 'test', description: 'sample')
			if (conditionSize) {
				conditionSize.times {
					report.addToConditions(new Condition())
				}
			}
			if (conditionLinkersSize) {
				conditionLinkersSize.times {
					report.addToConditionLinkers(ConditionLinker.OR)
				}
			}
			report.validate()
		then:
			report.errors['conditionLinkers'] == errorMessage
		where:
			conditionSize	| conditionLinkersSize	| errorMessage
			1				| 0						| null
			2				| 0						| 'count.invalid'
			2				| 1						| null
			5				| 4						| null
			5				| 3						| 'count.invalid'
	}

	void 'cannot save a report with no conditions and name'() {
		given:
			mockForConstraintsTests(Report)
			Report report
		when:
			report = new Report()
			report.validate()
		then:
			report.errors['name'] != null
			report.errors['conditions'] != null
	}
}
