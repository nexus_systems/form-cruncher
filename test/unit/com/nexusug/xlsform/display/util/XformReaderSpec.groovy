package com.nexusug.xlsform.display.util

import com.nexusug.xlsform.UnitTestUtils
import com.nexusug.xlsform.display.model.*
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by ivan on 30/11/2015.
 */
class XformReaderSpec extends Specification {

	void 'getFormName returns the proper form name'() {
		given:
			String xform = UnitTestUtils.readTestResourceAsStream(this.class, 'xformreader_spec_sample_form.xml').text
		expect:
			XformReader.getFormName(xform) == 'data'
	}

	void 'getMetaData returns the xform meta data'() {
		given:
			String xform = UnitTestUtils.readTestResourceAsStream(this.class, 'xformreader_spec_sample_form.xml').text
		expect:
			XformReader.getMetaData(xform) == [instanceID: null, timeStart: null, timeEnd: null, userID: null, deviceID: null, deprecatedID:'12345' ]
	}

	void 'getFormTitle returns the xform title'() {
		given:
			String xform = UnitTestUtils.readTestResourceAsStream(this.class, 'xformreader_spec_sample_form.xml').text
		expect:
			XformReader.getFormTitle(xform) == 'my form'
	}

	void 'getFormId returns the form id'() {
		given:
			String xform = UnitTestUtils.readTestResourceAsStream(this.class, 'xformreader_spec_sample_form.xml').text
		expect:
			XformReader.getFormId(xform) == 'build_my-form_1448121183'
	}

	void 'getElementConstraints returns a map of all of the constraints of an element'() {
		given:
			String xform = UnitTestUtils.readTestResourceAsStream(this.class, 'xformreader_spec_sample_form.xml').text
			Map result
		when:
			result = XformReader.getElementConstraints(xform, '/data/name')
		then:
			result == [nodeset:'/data/name', type:'string', requiredMsg: 'really needed', required: 'true()']
		when:
			result = XformReader.getElementConstraints(xform, '/data/meta/instanceID')
		then:
			result == [nodeset:'/data/meta/instanceID', type:'string', readonly: 'true()', calculate:"concat('uuid:', uuid())"]
		when:
			result = XformReader.getElementConstraints(xform, '/data/start')
		then:
			result == [nodeset:'/data/start', preload:'timestamp', type:'dateTime', preloadParams:'start']
	}

	void 'getFormElements returns a list of all the form elements'() {
		given:
			String xform = UnitTestUtils.readTestResourceAsStream(this.class, 'xformreader_spec_form_elements_test.xml').text
		when:
			def displayables = XformReader.getFormElements(xform)
		then:
			displayables[0].label == "Please enter birth information for each child born."
			displayables[0].hint == null
			displayables[0].readOnly == false
			displayables[0].required == false
			displayables[0].type == XformMultiDisplayableType.repeat
			displayables[0].displayables[0].label == 'Name of child?'
			displayables[0].displayables[0].name == 'child_name'
			displayables[0].displayables[1].label == 'Sex of child?'
			displayables[0].displayables[1].type == XformSelectElementType.select1
			displayables[0].displayables[1].items == [new XformSelectItem(label: 'Male', value: 'male'),
										 new XformSelectItem(label: 'Female', value: 'female')]
			displayables[0].displayables[2].name == 'birthweight'
			displayables[0].displayables[3].name == 'immunizations_atbirth'
			displayables[1].name == 'preference_group'
			displayables[2].name == 'respondent'
	}

	void 'getElementProperties returns a map of all properties of a form element'() {
		given:
			String xform = UnitTestUtils.readTestResourceAsStream(this.class, 'xformreader_spec_form_elements_test.xml').text
		expect:
			XformReader.getTextBoxTypeElementProperties(xform, '/data/name') == [label:null, hint:'can be as small as 0.3', appearance: "multiline"]
			XformReader.getTextBoxTypeElementProperties(xform, '/data/web_browsers') == [label:'What web browsers do you use?',
																							 hint:'Select all that apply', appearance: null]
	}

	void 'getSelectTypeElementItems returns a list of all items of a form select element'() {
		given:
			String xform = UnitTestUtils.readTestResourceAsStream(this.class, 'xformreader_spec_form_elements_test.xml').text
		expect:
			XformReader.getSelectTypeElementItems(xform, '/data/web_browsers').equals(
					[
							new XformSelectItem(label: 'Mozilla Firefox', value: 'firefox'),
							new XformSelectItem(label: 'Google Chrome', value: 'chrome'),
							new XformSelectItem(label: 'Internet Explorer', value: 'ie'),
							new XformSelectItem(label: 'Safari', value: 'safari')
					]
			)
	}

	void 'isConfigurationElement returns true for configuration elements and false for those that are not'() {
		given:
			String xform = UnitTestUtils.readTestResourceAsStream(this.class, 'xformreader_spec_sample_form.xml').text
		expect:
			!XformReader.isConfigurationElement(xform, '/data/name')
			XformReader.isConfigurationElement(xform, '/data/meta/instanceID')
	}

	@Unroll
	void 'extractItextReference extracts the itext if available otherwise returns the original value'() {
		expect:
			result == XformReader.extractItextReference(itext)
		where:
			itext 								| result
			'jr:itext(itextId)' 				| 'itextId'
			null								| null
			'jr:itext(\'/data/name:label\')' 	| '/data/name:label'
			'this is just a plain value' 		| 'this is just a plain value'
	}

	@Unroll
	void 'extractItemsetInstanceRef extracts the correct instance reference from an nodeset reference of an itemset'() {
		expect:
			result == XformReader.extractItemsetInstanceRef(nodesetRef)
		where:
			nodesetRef 																| result
			'instance(\'counties\')/root/item[state= /new_cascading_select/state ]' | 'counties'
			null																	| null
			'this is just a plain value' 											| 'this is just a plain value'
	}

	void 'extractItemsetInstanceFilters correctly extracts the instance filters associated with nodeset reference of an itemset'() {
		when:
			def nodesetRef = 'instance(\'counties\')/root/item[state= /new_cascading_select/state ]'
			def result = XformReader.extractItemsetInstanceFilters(nodesetRef)
		then:
			result == [new XformSelectItemsetInstanceFilter(itemPropertyRef:'/root/item/state', instanceValueRef:'/new_cascading_select/state')]
		when:
			nodesetRef = 'instance(\'cities\')/root/item[state= /new_cascading_select/state  and county= /new_cascading_select/county ]'
			result = XformReader.extractItemsetInstanceFilters(nodesetRef)
		then:
			result == [new XformSelectItemsetInstanceFilter(itemPropertyRef:'/root/item/state', instanceValueRef:'/new_cascading_select/state'),
					   new XformSelectItemsetInstanceFilter(itemPropertyRef:'/root/item/county', instanceValueRef:'/new_cascading_select/county')]
		when:
			nodesetRef = 'this is just a plain value'
			result = XformReader.extractItemsetInstanceFilters(nodesetRef)
		then:
			result == []
		when:
			nodesetRef = null
			result = XformReader.extractItemsetInstanceFilters(nodesetRef)
		then:
			result == []
	}

	void 'getSelectTypeElementItemset correctly returns the itemset details of a given nodeset'() {
		given:
			String xform = UnitTestUtils.readTestResourceAsStream(this.class, 'xformreader_spec_form_elements_test.xml').text
			XformSelectItemset result
		when:
			result = XformReader.getSelectTypeElementItemset(xform, '/data/country')
		then:
			with(result) {
				labelRef == "com.nexus.xlsfom.dynamic-itext:itextId"
				valueRef == "name"
				instanceRef == "counties"
			}
		when: 'there is no itemset'
			result = XformReader.getSelectTypeElementItemset(xform, '/data/web_browsers')
		then: 'return null!'
			result == null
	}

	void 'createXformSelectElement correctly creates and returns a XformSelectElement'() {
		given:
		String xform = UnitTestUtils.readTestResourceAsStream(this.class, 'xformreader_spec_select_elements.xml').text
			XformSelectElement result
		when:
			Map<String, String> constraints = new HashMap<>()
			constraints.readOnly = 'false()'
			constraints.required = 'true()'
			constraints.type = 'select1'
			constraints.requiredMsg = 'we need it'
			String name = 'country'
			String value = null
			String nodeset = '/data/country'
			result = XformReader.createXformSelectElement(constraints, name, value, xform, nodeset)
		then:
			with(result) {
				itemset.labelRef == "com.nexus.xlsfom.dynamic-itext:itextId"
				itemset.valueRef == "name"
				itemset.instanceRef == "counties"
				itemset.instanceItemFilters == [new XformSelectItemsetInstanceFilter(itemPropertyRef:'/root/item/state',
						instanceValueRef:'/new_cascading_select/state')]
				items == []
				label == 'county'
				(!readOnly)
				required
				requiredMsg == 'we need it'
				appearance == 'minimal'
				type == XformSelectElementType.select1
			}
		when:
			constraints = new HashMap<>()
			constraints.readOnly = 'false()'
			constraints.required = 'true()'
			constraints.type = 'select1'
			name = 'state'
			value = null
			nodeset = '/data/state'
			result = XformReader.createXformSelectElement(constraints, name, value, xform, nodeset)
		then:
			with(result) {
				itemset == null
				label == 'state'
				(!readOnly)
				required
				type == XformSelectElementType.select1
				items == [new XformSelectItem(label: 'Texas', value: 'texas'), new XformSelectItem(label: 'Washington', value: 'washington')]
			}
	}

	void 'XformTextBox correctly creates and returns a XformTextBox'() {
		given:
			String xform = UnitTestUtils.readTestResourceAsStream(this.class, 'xformreader_spec_form_elements_test.xml').text
			XformTextBox result
		when:
			Map<String, String> constraints = new HashMap<>()
			constraints.readOnly = 'false()'
			constraints.required = 'true()'
			constraints.type = 'string'
			constraints.requiredMsg = 'provide this please'
			String name = 'country'
			String value = null
			String nodeset = '/data/name'
			result = XformReader.createXformTextBox(constraints, name, value, xform, nodeset)
		then:
			with(result) {
				label == null
				hint == 'can be as small as 0.3'
				(!readOnly)
				required
				requiredMsg == 'provide this please'
				type == XformTextBoxType.string
				appearance == 'multiline'
			}
	}

	void 'serializeFormModelToJSON correctly serializes the xform xml content to equivalent JSON'() {
		given:
			String xform = UnitTestUtils.readTestResourceAsStream(this.class, 'xformreader_spec_serialize_form_model_to_json.xml').text
			String expectJsonModel = UnitTestUtils.readTestResourceAsStream(this.class, 'xformreader_spec_serialize_form_model_to_json.json').text
		when:
			def jsonModel = XformReader.serializeFormModelToJSON(xform, 2)
		then:
			jsonModel == expectJsonModel
	}

	void 'XformMultiDisplayable correctly creates and returns a XformMultiDisplayable'() {
		given:
			String xform = UnitTestUtils.readTestResourceAsStream(this.class, 'xformreader_spec_form_elements_test.xml').text
			XformMultiDisplayable result
		when:
			String nodeset = '/repeat_and_group_test/preference_group'
			result = XformReader.createXformMultiDisplayable(xform, nodeset)
		then:
			with(result) {
				label == "Let's us know more about you"
				hint == null
				(!readOnly)
				(!required)
				type == XformMultiDisplayableType.group
			}

			with(result.displayables[0]) {
				label == 'Do you like fish'
				value == 'yes'
			}

			with(result.displayables[1]) {
				label == 'What is your gender'
				type == XformSelectElementType.select1
				items == [new XformSelectItem(label: 'Male', value: 'male'),
												 new XformSelectItem(label: 'Female', value: 'female')]
			}
		when:
			nodeset = '/repeat_and_group_test/child_repeat'
			result = XformReader.createXformMultiDisplayable(xform, nodeset)
		then:
			with(result) {
				label == "Please enter birth information for each child born."
				hint == null
				(!readOnly)
				(!required)
				type == XformMultiDisplayableType.repeat
			}

			with(result.displayables[0]) {
				name == 'child_name'
				label == 'Name of child?'
				value == 'ivan'
				(!readOnly)
				(!hidden)
			}

			with(result.displayables[1]) {
				name == 'sex_child'
				label == 'Sex of child?'
				(!readOnly)
				(!hidden)
				type == XformSelectElementType.select1
				items == [ new XformSelectItem(label: 'Male', value: 'male'),
						   new XformSelectItem(label: 'Female', value: 'female') ]
			}
	}

	@Unroll
	void 'isXformGroupElement correctly detects xform groups'() {
		given:
			String xform = UnitTestUtils.readTestResourceAsStream(this.class, 'xformreader_spec_form_elements_test.xml').text
		expect:
			result == XformReader.isXformGroupElement(xform, nodeset)
		where:
			nodeset 			 						| result
			'/repeat_and_group_test/child_repeat' 		| true
			'/repeat_and_group_test/preference_group'	| true
			'/data/name'		 						| false
			'/data/country'		 						| false
	}
}
