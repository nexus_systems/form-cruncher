package com.nexusug.xlsform

import grails.test.mixin.TestFor
import spock.lang.Specification

import static org.springframework.http.HttpStatus.METHOD_NOT_ALLOWED
import static org.springframework.http.HttpStatus.UNSUPPORTED_MEDIA_TYPE

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(DataController)
class DataControllerSpec extends Specification {

	void 'save only supports posts'() {
		when:
			request.method = method
			controller.save()
		then:
			response.status == resposeStatus
		where:
			method | resposeStatus
			'PUT'  | METHOD_NOT_ALLOWED.value()
			'GET'  | METHOD_NOT_ALLOWED.value()
	}
}
